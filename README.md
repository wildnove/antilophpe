# README #

Antilophpe is a PHP+MySQL Code generator and a base web application (more or less a framework).
Starting from some XML-like tags into MySQL COMMENT field Antilophpe can generate PHP files.
These files can be used into the Antilophpe web application (admin).

# HOW TO INSTALL #

* In a LAMP Server just copy the two directories (admin and generator).
* Run sql.
* Try the generator.
* try the Antilophpe web application (admin).
* Enjoy all!

# Regole JSON #
Regole:
- name è il nome "codificato" dell'oggetto va scritto CamelCase (con il primo carattere maiuscolo). Max 30 caratteri
- label è il nome leggibile umanamente. Max 30 caratteri
- describedby serve per indicare un campo che descrive l'oggetto (ad esempio per una mostra potrebbe essere il titolo)
- orderby serve per indicare l'ordinamento di default della tabella di visualizzazione degli oggetti (DESC indica ordinamento decrescente)
- fields contiene i campi nell'ordine di visualizzazione in tabella, form, pdf ed excel. Il nome "codificato" va scritto camelCase (con il primo carattere minuscolo). Max 30 caratteri
- type è il tipo di dato che influirà anche sull'interfaccia di compilazione, può essere: "string", "text", "date", "object", "select", "integer", "flag", "decimal"
- maxlen numero massimo di caratteri per quel campo
- typeoptions definisce opzioni specifiche per il tipo indicato. values è la lista dei valori selezionabili e closed (default true) indica se è possibile aggiungere altri valori
- default è il valore da presentare precompilato all'utente per quel campo nella form
- grouplabel definisce una label di gruppo, i campi nello stesso gruppo devono essere successivi e avere la stessa grouplabel
- mandatory serve a definire se un campo è obbligatorio nella form, default è a true. Indicare solo se false.
- index serve a definire il tipo di indice per quel campo nel db, può essere: "index", "unique", "none". Rispettivamente indicizzato, indicizzato con chiave unica, non indicizzato. Default è "index".
- intable, inexcel, inpdf servono ad indicare se quel campo va estratto rispettivamente nella tabella, nell'excel e nel pdf, default true. Indicare solo se false.

Esempio
{
  "name": "MostraDiQuadri",
  "label": "Mostra di quadri",
  "describedby": "titolo",
  "orderby": "titolo DESC",
  "fields": {
    "titolo": {
      "label": "Titolo",
      "type": "string",
      "maxlen": 20,
      "index": "unique"
    },
    "museoOspitante": {
      "label": "Museo ospitante",
      "type": "object",
      "typeoptions": {
        "name": "Museo",
        "field": "nome"
      }
    },
    "dataApertura": {
      "label": "Data apertura",
      "type": "date"
    },
    "categoria": {
      "label": "Categoria",
      "type": "select",
      "typeoptions": {
        "values": [
          "acquerello",
          "olio",
          "tempera"
        ],
        "closed": false
      },
      "default": "acquerello"
    },
    "numeroIngressiDisponibili": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Numero ingressi disponibili",
      "type": "integer",
      "maxlen": 4,
      "mandatory": false,
      "index": "none"
    },
    "prezzoBigliettoEuro": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Prezzo del biglietto in euro",
      "type": "decimal",
      "mandatory": false,
      "intable": false
    },
    "accessoDisabili": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Accesso disabili",
      "type": "flag"
    },
    "note": {
      "label": "Note",
      "type": "text",
      "mandatory": false,
      "intable": false,
      "inexcel": false,
      "inpdf": false
    }
  }
}


Se volete editarlo facilmente copiatelo e incollatelo qui (a sinistra)
http://www.jsoneditoronline.org/
