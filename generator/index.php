<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$antilophpeAdminPath = '../admin';

require_once($antilophpeAdminPath . "/config/config.inc.php");
require_once($antilophpeAdminPath . "/core/DatabaseHelper.class.php");
require_once($antilophpeAdminPath . "/core/ModuleHelper.class.php");
require_once("ObjectGenerator.class.php");

$DatabaseHelper = new DatabaseHelper($dbParams);

function writeContent($filename, $content)
{
    $handle = fopen($filename, 'w+');
    fwrite($handle, $content);
    fclose($handle);
    @chmod($filename, 0777);
}

echo '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>antilophpe3 - JSON driven generator</title>
    <style>
    </style>
</head>
<body>
<div style="margin:auto;width:900px;">
    <h1 style="color:green;">antilophpe 3</h1>
    <h2>JSON driven generator</h2>';

$driver  = $DatabaseHelper->getDriver();
if (isset($_REQUEST['jsonDescription']) && $_REQUEST['password'] == $dbParams['password']) {
    if ($_REQUEST['operation'] == 'JSON UPD') {
      if($driver == 'mysql')
        $stmts = ObjectGenerator::generateJsonUpdMysqlStmts($DatabaseHelper, $_REQUEST['jsonDescription']);
      if($driver == 'oci')
        $stmts = ObjectGenerator::generateJsonUpdOracleStmts($DatabaseHelper, $_REQUEST['jsonDescription']);      
    }
    else {        
        if($driver == 'mysql')
          $stmts = ObjectGenerator::generateMysqlStmts($DatabaseHelper, $_REQUEST['jsonDescription']);
        if($driver == 'oci')
          $stmts = ObjectGenerator::generateOracleStmts($DatabaseHelper, $_REQUEST['jsonDescription']);
    }
    if ($stmts) {
        echo '
    <fieldset>
        <legend>Statement MySQL generati da eseguire</legend>	  
        <textarea style="height:400px;width:100%;" name="jsonDescription">' . htmlspecialchars($stmts) . '</textarea> 
    </fieldset>';
    } else {
        echo '
    <div>Attenzione errore nel generare gli statement dal json!</div>';
    }

} else if (isset($_REQUEST['Module__id']) && $_REQUEST['password'] == $dbParams['password']) {

    if ($_REQUEST['Module__id'] == 1)
        $generationIds = array(3, 4, 5, 6, 7, 8, 9);
    else
        $generationIds = array($_REQUEST['Module__id']);

    foreach ($generationIds as $Module__id) {
        $sql = "SELECT `jsonDescription`
             FROM `" . TBPX . "Module` 
             WHERE `id` = '" . $Module__id . "'";
        $stmt = $DatabaseHelper->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $dateTimeNow = date('Y-m-d_H-i-s');
        $moduleDescription = json_decode($row['jsonDescription'], true);
        $moduleDescription = ModuleHelper::addDefaultFieldsToDescription($moduleDescription);
        if (!$moduleDescription) {
            echo '
    <div>Errore! Dati non validi, i campi default esistono gi&agrave; nel json descrittivo.</div>';
            break;
        }
        if (!is_dir($antilophpeAdminPath . '/trash'))
            @mkdir($antilophpeAdminPath . '/trash', 0777);

        $content = ObjectGenerator::generateClassFileContent($Module__id, $moduleDescription);
        if ($_REQUEST['generateInTrash'] != 1) {
            $filename = $antilophpeAdminPath . '/models/' . $moduleDescription['name'] . '.class.php';
            if (file_exists($filename)) {
                $filetrash = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '_' . $dateTimeNow . '.class.php';
                copy($filename, $filetrash);
            }
        } else
            $filename = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '.class.php';
        writeContent($filename, $content);

        $content = ObjectGenerator::generateGuiFileContent($Module__id, $moduleDescription);
        if ($_REQUEST['generateInTrash'] != 1) {
            $filename = $antilophpeAdminPath . '/gui/' . $moduleDescription['name'] . '.php';
            if (file_exists($filename)) {
                $filetrash = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '_' . $dateTimeNow . '.php';
                copy($filename, $filetrash);
            }
        } else
            $filename = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '.php';
        writeContent($filename, $content);

        $content = ObjectGenerator::generateOperationsFileContent($Module__id, $moduleDescription);
        if ($_REQUEST['generateInTrash'] != 1) {
            $filename = $antilophpeAdminPath . '/gui/' . $moduleDescription['name'] . '_operations.php';
            if (file_exists($filename)) {
                $filetrash = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '_' . $dateTimeNow . '_operations.php';
                copy($filename, $filetrash);
            }
        } else
            $filename = $antilophpeAdminPath . '/trash/' . $moduleDescription['name'] . '_operations.php';
        writeContent($filename, $content);

        echo '
    <div><h2 style="color:green;">Module "' . $moduleDescription['name'] . '" GENERATED!</h2></div>';
    }
} else {
    echo '    
    <form action="index.php" method="post">
        <fieldset>
            <legend>Genera sql da json</legend>           
            <div>
               <label>DB Password * </label>
                <input type="password" name="password" value="' . $dbParams['password'] . '"/>
            </div>           
            <br/>   
            <div>
                <label for="jsonDescription">Genera statement a partire dal JSON descrittivo</label>	    
                <textarea style="height:200px;width:100%;" id="jsonDescription" name="jsonDescription"></textarea>
            </div>
            <div style="text-align:right;"><input name="operation" type="submit" value="JSON UPD"/>&nbsp;<input name="operation" type="submit" value="GENERA"/></div>	    
        </fieldset>
    </form>
    <br/>
    <form action="index.php" method="post">
        <fieldset>
            <legend>Genera file modulo da db</legend>
            <div>
               <label>DB Password * </label>
                <input type="password" name="password" value="' . $dbParams['password'] . '"/>
            </div>          
            <br/>  
            <div>            
                <legend for="Module__id">Genera file modulo (da ' . TBPX . 'Module.jsonDescription)</legend>	 
                <select id="Module__id" name="Module__id">
                    <option value="">Scegli...</option>
                    <option value="1">All base</option>';
    $sql = "SELECT `id`, `name` 
              FROM `" . TBPX . "Module`
              WHERE `jsonDescription` IS NOT NULL 
              ORDER BY `id`";
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo '
                  <option ' . ($row['id'] == $_REQUEST['Module__id'] ? 'selected="selected"' : '') . ' value="' . $row['id'] . '">' . $row['name'] . '</option>';
    }
    echo '           
                </select>
                &nbsp;&nbsp;
                <input type="checkbox" checked="checked" name="generateInTrash" id="generateInTrash" value="1"/><label for="generateInTrash">Genera in trash</label>
                <br/>
            </div>
            <div style="text-align:right;"><input type="submit" value="GENERA"/></div>	    
        </fieldset>
    </form>';

    echo '
    <br/>
    <div style="padding:10px;background-color:#FFFADC;font-size:12px;font-family:sans-serif;border: 1px solid orange;overflow:auto;">
      <h2 style="color:#800000;">REGOLE JSON</h2>    
        <pre>
- name è il nome "codificato" dell\'oggetto va scritto CamelCase (con il primo carattere maiuscolo). Max 30 caratteri
- label è il nome leggibile umanamente. Max 30 caratteri
- describedby serve per indicare un campo che descrive l\'oggetto (ad esempio per una mostra potrebbe essere il titolo)
- orderby serve per indicare l\'ordinamento di default della tabella di visualizzazione degli oggetti (DESC indica ordinamento decrescente)
- fields contiene i campi nell\'ordine di visualizzazione in tabella, form, pdf ed excel. Il nome "codificato" va scritto camelCase (con il primo carattere minuscolo). Max 30 caratteri
- type è il tipo di dato che influirà anche sull\'interfaccia di compilazione, può essere: "string", "text", "date", "object", "select", "integer", "flag", "decimal", "datetime"
- maxlen numero massimo di caratteri per quel campo
- typeoptions definisce opzioni specifiche per il tipo indicato. values è la lista dei valori selezionabili e closed (default true) indica se è possibile aggiungere altri valori
- default è il valore da presentare precompilato all\'utente per quel campo nella form
- grouplabel definisce una label di gruppo, i campi nello stesso gruppo devono essere successivi e avere la stessa grouplabel
- mandatory serve a definire se un campo è obbligatorio nella form, default è a true. Indicare solo se false.
- index serve a definire il tipo di indice per quel campo nel db, può essere: "index", "unique", "none". Rispettivamente indicizzato, indicizzato con chiave unica, non indicizzato. Default è "index".
- intable, inexcel, inpdf servono ad indicare se quel campo va estratto rispettivamente nella tabella, nell\'excel e nel pdf, default true. Indicare solo se false.

Esempio
{
  "name": "MostraDiQuadri",
  "label": "Mostra di quadri",
  "describedby": "titolo",
  "orderby": ["titolo DESC"],
  "fields": {
    "titolo": {
      "label": "Titolo",
      "type": "string",
      "maxlen": 20,
      "index": "unique"
    },
    "museoOspitante": {
      "label": "Museo ospitante",
      "type": "object",
      "typeoptions": {
        "name": "Museo",
        "field": "nome"
      }
    },
    "dataApertura": {
      "label": "Data apertura",
      "type": "date"
    },
    "categoria": {
      "label": "Categoria",
      "type": "select",
      "typeoptions": {
        "values": [
          "acquerello",
          "olio",
          "tempera"
        ],
        "closed": false
      },
      "default": "acquerello"
    },
    "numeroIngressiDisponibili": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Numero ingressi disponibili",
      "type": "integer",
      "maxlen": 4,
      "mandatory": false,
      "index": "none"
    },
    "prezzoBigliettoEuro": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Prezzo del biglietto in euro",
      "type": "decimal",
      "mandatory": false,
      "intable": false
    },
    "accessoDisabili": {
      "grouplabel": "Informazioni e prezzo",
      "label": "Accesso disabili",
      "type": "flag"
    },
    "note": {
      "label": "Note",
      "type": "text",
      "mandatory": false,
      "intable": false,
      "inexcel": false,
      "inpdf": false
    }
  }
}


Se volete editarlo facilmente copiatelo e incollatelo qui (a sinistra)
http://www.jsoneditoronline.org/
        </pre>
    </div>';

    echo '
    <div style="margin-top:10px;padding:10px;background-color:#FFFADC;font-size:12px;font-family:sans-serif;border: 1px solid orange;">
      <h3 style="color:#800000;">IMMAGINI DI SISTEMA</h3>
      Attualmente ci sono le seguenti immagini presenti in questa installazione di atilope (modificarle per il cliente):';

    if ($handle = opendir($antilophpeAdminPath . '/img/')) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && (strstr($file, 'png') != '' || strstr($file, 'jpg') != '')) {
                echo '<div style="text-align:center;margin:20px;border:1px solid gray;padding:5px;"><img src="' . $antilophpeAdminPath . '/img/' . $file . '" alt="' . $antilophpeAdminPath . '/img/' . $file . '"/><br/><br/>' . $antilophpeAdminPath . '/img/' . $file . '</div>';
            }
        }
        closedir($handle);
    }

    echo '
    </div>';
}


echo '
</div>
</body>
</html>';
?>