<?php

class ObjectGenerator
{
    static function generateMysqlStmts($DatabaseHelper, $jsonDescription)
    {
        $moduleDescription = json_decode($jsonDescription, true);

        $returnStmt = "";
        $returnStmt .= "
CREATE TABLE `" . TBPX . $moduleDescription['name'] . "` (
`id` INT(8) UNSIGNED NOT NULL AUTO_INCREMENT,";
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;

            if ($field['type'] == 'string') {
                $type = "VARCHAR(" . ($field['maxlen'] ? $field['maxlen'] : 255) . ") NULL";
            } else if ($field['type'] == 'object') {
                $type = "INT(8) UNSIGNED NULL";
            } else if ($field['type'] == 'select') {
                $type = "VARCHAR(" . ($field['maxlen'] ? $field['maxlen'] : 255) . ") NULL";
            } else if ($field['type'] == 'integer') {
                $type = "INT(" . ($field['maxlen'] ? $field['maxlen'] : 8) . ") NULL";
            } else if ($field['type'] == 'date') {
                $type = "DATE NULL";
            } else if ($field['type'] == 'datetime') {
                $type = "DATETIME NULL";
            } else if ($field['type'] == 'decimal') {
                $type = "DECIMAL(" . ($field['maxlen'] ? $field['maxlen'] : 8) . ",2) NULL";
            } else if ($field['type'] == 'flag') {
                $type = "TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'";
            } else if ($field['type'] == 'text' || $field['type'] == 'crud') {
                $type = "TEXT NULL";
            }

            $returnStmt .= "
`" . $field['name'] . "` " . $type . ",";
        }

        $returnStmt .= "
`coverFile` INT(8) UNSIGNED NULL,
`lastDateTime` DATETIME NOT NULL,
`user` INT(8) UNSIGNED " . ($moduleDescription['name'] != 'Log' ? 'NOT ' : '') . "NULL,
`active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
`trashed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',";
        $returnStmt .= "
PRIMARY KEY(`id`), ";
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['type'] == 'text' || $field['type'] == 'crud' || $field['index'] == 'none')
                continue;
            else if ($field['index'] == 'unique')
                $returnStmt .= "
UNIQUE KEY (`" . $field['name'] . "`),";
            else
                $returnStmt .= "
KEY (`" . $field['name'] . "`),";
        }
        $returnStmt .= "
KEY (`coverFile`),
KEY (`lastDateTime`),
KEY (`user`),
KEY (`active`),
KEY (`trashed`)
) ENGINE = MyISAM DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci AUTO_INCREMENT = 1;";

        if (!array_key_exists("id", $moduleDescription)) {
            $sql = "SELECT MAX(`id`)+1 AS `nextId`
                 FROM `" . TBPX . "Module`";
            $stmt = $DatabaseHelper->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $insert['id'] = $row['nextId'];
        } else
            $insert['id'] = $moduleDescription['id'];

        $insert['name'] = $moduleDescription['name'];
        $insert["label"] = $moduleDescription['label'];
        $insert["url"] = $moduleDescription["url"] ? $moduleDescription["url"] : $insert['name'] . '.php';
        $insert["urlTarget"] = $moduleDescription["urltarget"] ? $moduleDescription["urltarget"] : '_WorkFrame';
        $insert["parentModule"] = $moduleDescription["parentmodule"] ? $moduleDescription["parentmodule"] : 1;
        $insert["siblingOrder"] = $moduleDescription["siblingorder"] ? $moduleDescription["siblingorder"] : 0;
        $insert["icon"] = $moduleDescription["icon"] ? $moduleDescription["icon"] : "actions/cancel.png";
        $insert["jsonDescription"] = json_encode(json_decode($jsonDescription));

        $lastDateTime = date('Y-m-d H:i:s');

        $returnStmt .= "

INSERT INTO `" . TBPX . "Module` VALUES(" . $insert['id'] . ", " . $DatabaseHelper->pdo->quote($insert['name']) . ", " . $DatabaseHelper->pdo->quote($insert['label']) . ", " . $DatabaseHelper->pdo->quote($insert['url']) . ", " . $DatabaseHelper->pdo->quote($insert['urlTarget']) . ", " . $insert['parentModule'] . ", " . $insert['siblingOrder'] . ", " . $DatabaseHelper->pdo->quote($insert['icon']) . ", " . $DatabaseHelper->pdo->quote($insert['jsonDescription']) . ", NULL, '" . $lastDateTime . "', 1, 1, 0);
";

        $actions = array('VIEW', 'INSERT', 'UPDATE', 'ACTIVATE', 'TRASH', 'FILESVIEW', 'FILESMANAGE', 'DUPLICATE', 'EXCEL', 'PDF');

        foreach ($actions as $action) {

            $returnStmt .= "
INSERT INTO `" . TBPX . "Action` VALUES(NULL, " . $insert['id'] . ", '" . $action . "', NULL, '" . $lastDateTime . "', 1, 1, 0);";
        }
        return $DatabaseHelper->dbescape($returnStmt);
    }



    static function generateOracleStmts($DatabaseHelper, $jsonDescription)
    {
        $moduleDescription = json_decode($jsonDescription, true);
        $returnStmt = "";
        $returnStmt .= "
CREATE TABLE `" . TBPX . $moduleDescription['name'] . "` (
`id` NUMBER(8) NOT NULL,";
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;

            if ($field['type'] == 'string') {
                $type = "VARCHAR(" . (isset($field['maxlen']) ? $field['maxlen'] : 255) . ") NULL";
            } else if ($field['type'] == 'object') {
                $type = "NUMBER(10) NULL";
            } else if ($field['type'] == 'select') {
                $type = "VARCHAR(" . (isset($field['maxlen']) ? $field['maxlen'] : 255) . ") NULL";
            } else if ($field['type'] == 'integer') {
                $type = "NUMBER(" . (isset($field['maxlen']) ? $field['maxlen'] : 8) . ") NULL";
            } else if ($field['type'] == 'date') {
                $type = "DATE NULL";
            } else if ($field['type'] == 'datetime') {
                $type = "DATETIME NULL";
            } else if ($field['type'] == 'decimal') {
                $type = "NUMBER(" . (isset($field['maxlen']) ? $field['maxlen'] : 8) . ",2) NULL";
            } else if ($field['type'] == 'flag') {
                $type = "NUMBER(1) DEFAULT '0' NOT NULL";
            } else if ($field['type'] == 'text' || $field['type'] == 'crud') {
                $type = "VARCHAR(4000) NULL";
            }

            $returnStmt .= "
`" . $field['name'] . "` " . $type . ",";
        }

        $returnStmt .= "
`coverFile` NUMBER(8) NULL,
`lastDateTime` TIMESTAMP(0) NOT NULL,
`user` NUMBER(8) " . ($moduleDescription['name'] != 'Log' ? 'NOT ' : '') . "NULL,
`active` NUMBER(1) DEFAULT '1' NOT NULL,
`trashed` NUMBER(1) DEFAULT '0' NOT NULL,";
        $returnStmt .= "
PRIMARY KEY(`id`),";
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['index'] = isset($field['index']) ? $field['index'] : '';
            $field['name'] = $name;
            if ($field['type'] == 'text' || $field['type'] == 'crud' || $field['index'] == 'none')
                continue;
            else if ($field['index'] == 'unique')
                $returnStmt .= "
UNIQUE (`" . $field['name'] . "`),";
        }

        $returnStmt = rtrim($returnStmt, ",").");";

        // Indexing
        $idx_counter = 0;
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $idx_counter++;
            $field['index'] = isset($field['index']) ? $field['index'] : '';
            $field['name'] = $name;
            if ($field['index'] != 'unique' &&
                $field['type'] != 'text' &&
                $field['type'] != 'crud' &&
                $field['index'] != 'none')
                $returnStmt .= "
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . $idx_counter . "` ON `" . TBPX . $moduleDescription['name'] . "`(`" . $field['name'] . "`);";
        }

        $returnStmt .= "
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . ($idx_counter+1) . "` ON `" . TBPX . $moduleDescription['name'] . "`(`coverFile`);
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . ($idx_counter+2) . "` ON `" . TBPX . $moduleDescription['name'] . "`(`lastDateTime`);
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . ($idx_counter+3) . "` ON `" . TBPX . $moduleDescription['name'] . "`(`user`);
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . ($idx_counter+4) . "` ON `" . TBPX . $moduleDescription['name'] . "`(`active`);
CREATE INDEX `" . TBPX . $moduleDescription['name'] . "_idx_" . ($idx_counter+5) . "` ON `" . TBPX . $moduleDescription['name'] . "`(`trashed`);


-- Generate ID using sequence and trigger
CREATE SEQUENCE `" . TBPX . $moduleDescription['name'] . "_seq` START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER `" . TBPX . $moduleDescription['name'] . "_seq_tr`
 BEFORE INSERT ON `" . TBPX . $moduleDescription['name'] . "` FOR EACH ROW
 WHEN (NEW.`id` IS NULL)
BEGIN
 SELECT `" . TBPX . $moduleDescription['name'] . "_seq`.NEXTVAL INTO :NEW.`id` FROM DUAL;
END;
/";

        if (!array_key_exists("id", $moduleDescription)) {
            $sql = "SELECT `" . TBPX . "Module_seq`.NEXTVAL AS `nextId` 
                FROM DUAL";
            $stmt = $DatabaseHelper->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $insert['id'] = $row['nextId'];
        } else
            $insert['id'] = $moduleDescription['id'];

        $insert['name'] = $moduleDescription['name'];
        $insert["label"] = $moduleDescription['label'];
        $insert["url"] = isset($moduleDescription["url"]) ? $moduleDescription["url"] : $insert['name'] . '.php';
        $insert["urlTarget"] = isset($moduleDescription["urltarget"]) ? $moduleDescription["urltarget"] : '_WorkFrame';
        $insert["parentModule"] = isset($moduleDescription["parentmodule"]) ? $moduleDescription["parentmodule"] : 1;
        $insert["siblingOrder"] = isset($moduleDescription["siblingorder"]) ? $moduleDescription["siblingorder"] : 0;
        $insert["icon"] = isset($moduleDescription["icon"]) ? $moduleDescription["icon"] : "actions/cancel.png";
        $insert["jsonDescription"] = json_encode(json_decode($jsonDescription));

        $lastDateTime = date('Y-m-d H:i:s');

        $returnStmt .= "

INSERT INTO `" . TBPX . "Module` VALUES(" . $insert['id'] . ", " . $DatabaseHelper->pdo->quote($insert['name']) . ", " . $DatabaseHelper->pdo->quote($insert['label']) . ", " . $DatabaseHelper->pdo->quote($insert['url']) . ", " . $DatabaseHelper->pdo->quote($insert['urlTarget']) . ", " . $insert['parentModule'] . ", " . $insert['siblingOrder'] . ", " . $DatabaseHelper->pdo->quote($insert['icon']) . ", " . $DatabaseHelper->pdo->quote($insert['jsonDescription']) . ", NULL, TO_TIMESTAMP('" . $lastDateTime . "', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
";

        $actions = array('VIEW', 'INSERT', 'UPDATE', 'ACTIVATE', 'TRASH', 'FILESVIEW', 'FILESMANAGE', 'DUPLICATE', 'EXCEL', 'PDF');

        foreach ($actions as $action) {

            $returnStmt .= "
INSERT INTO `" . TBPX . "Action` VALUES(NULL, " . $insert['id'] . ", '" . $action . "', NULL, TO_TIMESTAMP('" . $lastDateTime . "', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);";
        }
        return $DatabaseHelper->dbescape($returnStmt);
    }


    static function generateJsonUpdMysqlStmts($DatabaseHelper, $jsonDescription)
    {
        $moduleDescription = json_decode($jsonDescription, true);
        $insert['id'] = $moduleDescription['id'];
        $insert["jsonDescription"] = json_encode(json_decode($jsonDescription));
        $lastDateTime = date('Y-m-d H:i:s');
        $returnStmt = "UPDATE `" . TBPX . "Module` SET `jsonDescription` = " . $DatabaseHelper->pdo->quote($insert['jsonDescription']) . ", `lastDateTime`= '" . $lastDateTime . "' WHERE `id` = " . $insert['id'] . ";";
        $returnStmt = str_replace("\'", "''", $returnStmt);
        return $DatabaseHelper->dbescape($returnStmt);
    }

    static function generateJsonUpdOracleStmts($DatabaseHelper, $jsonDescription)
    {
        $moduleDescription = json_decode($jsonDescription, true);
        $insert['id'] = $moduleDescription['id'];
        $insert["jsonDescription"] = json_encode(json_decode($jsonDescription));
        $lastDateTime = date('Y-m-d H:i:s');
        $returnStmt = "UPDATE `" . TBPX . "Module` SET `jsonDescription` = " . $DatabaseHelper->pdo->quote($insert['jsonDescription']) . ", `lastDateTime`= TO_TIMESTAMP('" . $lastDateTime . "', 'YYYY-MM-DD HH24:MI:SS') WHERE `id` = " . $insert['id'] . ";";
        $returnStmt = str_replace("\'", "''", $returnStmt);
        return $DatabaseHelper->dbescape($returnStmt);
    }   


    static function generateClassFileContent($Module__id, $moduleDescription)
    {
        $returnContent = '';
        $returnContent = '<?php

class ' . $moduleDescription['name'] . '
{
    private $DatabaseHelper;';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $returnContent .= '
    private $' . $field['name'] . ';';

            if ($field['type'] == 'object') {
                $returnContent .= '
    private $' . $field['name'] . '_' . $field['typeoptions']['field'] . ';';
            }
        }

        $returnContent .= '
';

        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $returnContent .= '
    function set' . ucfirst($field['name']) . '($' . $field['name'] . ')
    {
        $this->' . $field['name'] . ' = $' . $field['name'] . ';
    }

    function get' . ucfirst($field['name']) . '()
    {
        return $this->' . $field['name'] . ';
    }
';
            if ($field['type'] == 'object') {
                $returnContent .= '
    function set' . ucfirst($field['name'] . '_' . $field['typeoptions']['field']) . '($' . $field['name'] . '_' . $field['typeoptions']['field'] . ')
    {
        $this->' . $field['name'] . '_' . $field['typeoptions']['field'] . ' = $' . $field['name'] . '_' . $field['typeoptions']['field'] . ';
    }

    function get' . ucfirst($field['name'] . '_' . $field['typeoptions']['field']) . '()
    {
        return $this->' . $field['name'] . '_' . $field['typeoptions']['field'] . ';
    }
';

            }
        }

        $returnContent .= '
    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "' . $moduleDescription['name'] . '`.*';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['type'] == 'object') {
                $returnContent .= ',
                     `" . TBPX . "' . $field['typeoptions']['name'] . '_' . $field['name'] . '`.`' . $field['typeoptions']['field'] . '` AS `' . $field['name'] . '_' . $field['typeoptions']['field'] . '`';
            }
        }
        $returnContent .= '";
        return $stmt . "
                     FROM `" . TBPX . "' . $moduleDescription['name'] . '`';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['type'] == 'object') {
                $returnContent .= '
                     LEFT JOIN `" . TBPX . "' . $field['typeoptions']['name'] . '` `" . TBPX . "' . $field['typeoptions']['name'] . '_' . $field['name'] . '` ON `" . TBPX . "' . $field['typeoptions']['name'] . '_' . $field['name'] . '`.`id` = `" . TBPX . "' . $moduleDescription['name'] . '`.`' . $field['name'] . '`';
            }
        }
        $returnContent .= '";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "' . $moduleDescription['name'] . '`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "' . $moduleDescription['name'] . '`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

}

?>';
        return $returnContent;

    }

    static function generateGuiFileContent($Module__id, $moduleDescription)
    {
        $returnContent = '';
        $returnContent .= '<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/' . $moduleDescription['name'] . '.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = ' . $Module__id . ';
$Object = new ' . $moduleDescription['name'] . '($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION[\'Mask\'][$moduleId])) {
    echo \'
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\\\'_LoginOperations.php?operation=logout&message=intrusion\\\'</script>
      </body>
    </html>\';
    exit;
}

$Design = new Design();' 
. ( array_search('crud', array_column($moduleDescription['fields'], 'type')) ? "\n".'$Design->addJs(\'../js/jquery.textareacrud-configurable.js?ver3\');' : '' ) .'
$Design->privateHtmlOpen();

$' . $moduleDescription['name'] . ' = new ' . $moduleDescription['name'] . '($DatabaseHelper);

if ($_REQUEST[\'operation\'] == \'filterOrder\') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST[\'operation\']==\'order\'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST[\'operation\'] == \'emptyFilterOrder\' || $_REQUEST[\'operation\'] == \'viewTrashed\' || $_REQUEST[\'operation\'] == \'viewNotTrashed\'){
    unset($_SESSION[\'Filter' . $moduleDescription['name'] . '\']);
    unset($_SESSION[\'Order' . $moduleDescription['name'] . '\']);
    unset($_SESSION[\'Navbar' . $moduleDescription['name'] . '\']);
}
// Default order
if (!$_SESSION[\'Order' . $moduleDescription['name'] . '\']) {';
        for ($i = 1; $i <= 3; $i++) {
            $orderTableName = '';
            $orderFieldName = '';
            $orderDirection = '';
            if (isset($moduleDescription['orderby'][$i - 1])) {
                $value = $moduleDescription['orderby'][$i - 1];
                if (strpos($value, ' ') !== false)
                    list($field, $orderDirection) = explode(" ", $value);
                else
                    $field = $value;
                if ($moduleDescription['fields'][$field]['type'] == 'object') {
                    $orderTableName = $moduleDescription['fields'][$field]['typeoptions']['name'] . '_' . $field;
                    $orderFieldName = $moduleDescription['fields'][$field]['typeoptions']['field'];
                } else {
                    $orderTableName = $moduleDescription['name'];
                    $orderFieldName = $field;
                }
            }
            $returnContent .= '
    $_SESSION[\'Order' . $moduleDescription['name'] . '\'][\'field' . $i . '\'] = ' . ($orderTableName && $orderFieldName ? 'TBPX . \'' . $orderTableName . '.' . $orderFieldName . '\'' : '\'\'') . ';
    $_SESSION[\'Order' . $moduleDescription['name'] . '\'][\'field' . $i . '_direction\'] = \'' . $orderDirection . '\';';
        }
        $returnContent .= '
}
if ($_REQUEST[\'bloccoAttuale\'] != \'\')
    $_SESSION[\'Navbar' . $moduleDescription['name'] . '\'][\'bloccoAttuale\'] = $_REQUEST[\'bloccoAttuale\'];
if ($_REQUEST[\'numeroPagina\'] != \'\')
    $_SESSION[\'Navbar' . $moduleDescription['name'] . '\'][\'numeroPagina\'] = $_REQUEST[\'numeroPagina\'];

if ($_REQUEST[\'operation\'] == \'viewTrashed\')
    $_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\'] = 1;
if ($_REQUEST[\'operation\'] == \'viewNotTrashed\')
    $_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\'] = 0;

if ($_REQUEST[\'operation\'] == \'remoteSearchCall\') {
    $_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\'] = 0;
    $_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'foreignFieldsList\'] = $_REQUEST[\'foreignFieldsList\'];
    $_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'objectName\'] = $_REQUEST[\'objectName\'];
    $_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'fieldName\'] = $_REQUEST[\'fieldName\'];
}

if ($_REQUEST[\'operation\'] == \'insert\') {
    $' . $moduleDescription['name'] . '->setId(\'\');
}
if ($_REQUEST[\'operation\'] == \'updated\') {
    $' . $moduleDescription['name'] . '_array = $' . $moduleDescription['name'] . '->getAsArrayById($_REQUEST[\'' . $moduleDescription['name'] . '__id\']);
    $array_foreignFieldsList = explode(",", $_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'foreignFieldsList\']);
    $foreignSequence_updated = \'\';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$' . $moduleDescription['name'] . '_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, \' \');
}
if ($_REQUEST[\'operation\'] == \'modify\') {
    $' . $moduleDescription['name'] . '->loadById($_REQUEST[\'' . $moduleDescription['name'] . '__id\']);
}
if ($_REQUEST[\'operation\'] == \'manageFiles\') {
    $' . $moduleDescription['name'] . '->loadById($_REQUEST[\'' . $moduleDescription['name'] . '__id\']);
}
if ($_REQUEST[\'operation\'] == \'duplicateManual\') {
    $' . $moduleDescription['name'] . '->loadById($_REQUEST[\'' . $moduleDescription['name'] . '__id\']);
    $' . $moduleDescription['name'] . '->setId(\'\');
    $_REQUEST[\'' . $moduleDescription['name'] . '__id\'] = \'\';
}

if (($_SESSION[\'Mask\'][$moduleId][\'INSERT\'] || $_SESSION[\'Mask\'][$moduleId][\'UPDATE\'] || $_SESSION[\'Mask\'][$moduleId][\'DUPLICATE\'])
    && ($_REQUEST[\'operation\'] == \'insert\' || $_REQUEST[\'operation\'] == \'modify\' || $_REQUEST[\'operation\'] == \'duplicateManual\')
) {
    echo \'

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            \'.$ModuleHelper->getModuleLogoAndLabel().\'<span class="vertical_middle"> - \'.($_REQUEST[\'operation\'] == \'modify\' ? \'Modifica (Id: \'.$' . $moduleDescription['name'] . '->getId().\')\' : \'Aggiungi\'.($_REQUEST[\'operation\'] == \'duplicateManual\' ? \' (da duplicazione)\' : \'\')).\'</span>
        </div>
        <div class="float_right padding2">
            <a href="' . $moduleDescription['name'] . '.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="' . $moduleDescription['name'] . '_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>\';';
        $returnContent .= '
    if ($_REQUEST[\'operation\'] == \'modify\') {
        echo \'
            <input type="hidden" value="\' . $' . $moduleDescription['name'] . '->getId() . \'" name="' . $moduleDescription['name'] . '__id" id="' . $moduleDescription['name'] . '__id"/>\';
    }';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $field['inform'] = isset($field['inform']) ? $field['inform'] : true;
            $field['mandatory'] = isset($field['mandatory']) ? $field['mandatory'] : true;
            $field['grouplabel'] = isset($field['grouplabel']) ? $field['grouplabel'] : '';
            $field['maxlen'] = isset($field['maxlen']) ? $field['maxlen'] : '';
            $field['help'] = isset($field['help']) ? $field['help'] : '';
            $field['help'] = isset($field['help']) ? $field['help'] : '';

            $field['typeoptions']['closed'] = isset($field['typeoptions']['closed']) ? $field['typeoptions']['closed'] : true;

            if ($field['name'] == 'coverFile' ||
                $field['name'] == 'lastDateTime' ||
                $field['name'] == 'id' ||
                $field['name'] == 'user' ||
                $field['name'] == 'active' ||
                $field['name'] == 'trashed' ||
                $field['inform'] === false
            )
                continue;

            if ($field['grouplabel'] != $prevGroupLabel) {
                if ($prevGroupLabel) {
                    $returnContent .= '
    echo \'
            </fieldset>\';';
                }
                if ($field['grouplabel']) {
                    $returnContent .= '
    echo \'
            <fieldset>
                <legend>' . str_replace("'", "\'", $field['grouplabel']) . '</legend>\';';
                }
                $prevGroupLabel = $field['grouplabel'];
            }

            $mandatory = $field['mandatory'] === false ? '' : ' class="mandatory"';
            $inputSize = $field['maxlen'] + 10;

            $helpLabel = $field['help'] ? ' <span class="helplabel">' . str_replace("'", "\'", $field['help']) . '</span>' : '';

            if ($field['type'] == 'object') {
                $returnContent .= '
    if (!$' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '())
        $' . $moduleDescription['name'] . '->set' . ucfirst($field['name']) . '(\'\');
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input data-foreignobject="' . $field['typeoptions']['name'] . '" data-foreignfield="' . $field['typeoptions']['field'] . '" data-field="' . $field['name'] . '" class="autocomplete-foreign input_text" type="text" value="' . '\' . $' . $moduleDescription['name'] . '->get' . ucfirst($field['name'] . '_' . $field['typeoptions']['field']) . '().\'' . '" name="Foreign' . $moduleDescription['name'] . '__' . $field['name'] . '" id="Foreign' . $moduleDescription['name'] . '__' . $field['name'] . '" size="50"/>
                &nbsp;&nbsp;
                <input class="input_text readonly" readonly="readonly" type="text" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" value="\'.$' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '().\'" size="5"/>
                <a href="#" title="Cerca." onclick="window.open(\\\'' . $field['typeoptions']['name'] . '.php?operation=remoteSearchCall&amp;objectName=' . $moduleDescription['name'] . '&amp;fieldName=' . $field['name'] . '&amp;foreignFieldsList=' . $field['typeoptions']['field'] . '\\\',\\\'Window' . $field['typeoptions']['name'] . '\'.rand(0,1000).\'\\\',\\\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\\\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
                  <span>Cerca</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="
                  $(\\\'#Foreign' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val(\\\'\\\');
                  $(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val(\\\'\\\');
                  return false;"  class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>\';';
            } else if ($field['type'] == 'flag') {
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input class="input_checkbox" \' . ($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '() == 1 ? \'checked="checked"\' : \'\') . \' type="checkbox" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" value="1"/>
                <span class="label_checkbox">&nbsp;SI</span>
            </div>\';';
            } else if ($field['type'] == 'integer') {
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input class="input_text" type="text"  value="\'.htmlspecialchars($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '()).\'" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" maxlength="' . $field['maxlen'] . '" size="' . $inputSize . '"/>
            </div>\';';
            } else if ($field['type'] == 'decimal') {
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input class="input_text" type="text"  value="\'.htmlspecialchars(str_replace(".", ",", $' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '())).\'" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" maxlength="' . $field['maxlen'] . '" size="' . $inputSize . '"/>
            </div>\';';
            } else if ($field['type'] == 'date') {
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>\';
    if ($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '() != \'\')
        $' . $moduleDescription['name'] . '->set' . ucfirst($field['name']) . '(date("d/m/Y", strtotime($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '())));
    echo \'
                <input class="input_text date-pick" value="\'.$' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '().\'" type="text" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" maxlength="' . $field['maxlen'] . '" size="' . $inputSize . '"/>
            </div>\';';
            } else if ($field['type'] == 'datetime') {
                continue;
            } else if ($field['type'] == 'select' && $field['typeoptions']['closed'] === false) { // select open
                if ($field['maxlen'])
                    $inputSize = $field['maxlen'] > 50 ? 50 : $field['maxlen'] + 10;
                else
                    $inputSize = 20;
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input data-field="' . $field['name'] . '" class="autocomplete-select input_text" type="text" value="\'.htmlspecialchars($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '()).\'" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" maxlength="' . $field['maxlen'] . '" size="' . $inputSize . '"/>
                <a href="#" title="Visualizza suggerimenti di compilazione." onclick="$(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').focus();$(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').autocomplete(\\\'search\\\', \\\'@suggest@\\\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/apps/kdict.png" alt="SUGG" width="16" height="16" />
                  <span>Dizionario</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="$(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val(\\\'\\\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>\';';

            } else if ($field['type'] == 'select') { // select closed
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <select name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '">
                  <option value="">Seleziona...</option>\';
                  foreach($moduleDescription[\'fields\'][\'' . $field['name'] . '\'][\'typeoptions\'][\'values\'] as $option){
                    $selected = $' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '() == $option ? \'selected="selected"\' : \'\';
                    echo \'
                  <option \'.$selected.\' value="\'.htmlspecialchars($option).\'">\' . htmlspecialchars($option) . "</option>\n";
                  }
                echo \'
                </select>
            </div>\';';
            } else if ($field['type'] == 'text' || $field['type'] == 'crud') {
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <textarea name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" rows="3" cols="70">\'.htmlspecialchars($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '()).\'</textarea>
            </div>\';';
            } else { // string
                $inputSize = $field['maxlen'] > 50 ? 50 : $field['maxlen'] + 10;
                $returnContent .= '
    echo \'
            <div' . $mandatory . '>
                <label for="' . $moduleDescription['name'] . '__' . $field['name'] . '">' . str_replace("'", "\'", $field['label']) . $helpLabel . '</label><br/>
                <input data-field="' . $field['name'] . '" class="autocomplete input_text" type="text" value="\'.htmlspecialchars($' . $moduleDescription['name'] . '->get' . ucfirst($field['name']) . '()).\'" name="' . $moduleDescription['name'] . '__' . $field['name'] . '" id="' . $moduleDescription['name'] . '__' . $field['name'] . '" maxlength="' . $field['maxlen'] . '" size="' . $inputSize . '"/>
            </div>\';';
            }
        }

        if ($prevGroupLabel) {
            $returnContent .= '
    echo \'
                </fieldset>\';';
        }

        $returnContent .= '
    echo \'
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>\';
}
else{
      echo \'
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="' . $moduleDescription['name'] . '.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\\\'#FilterBox\\\').dialog(\\\'close\\\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>\';
        $prevGroupLabel = \'\';
        foreach ($moduleDescription[\'fields\'] AS $name => $field) {
            if ($field[\'grouplabel\'] != $prevGroupLabel && $field[\'grouplabel\']) {
                echo \'
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;\' . $field[\'grouplabel\'] . \'</label></td>
       </tr>\';
                $prevGroupLabel = $field[\'grouplabel\'];
            }
            if ($field[\'grouplabel\'])
                $field[\'label\'] = \'&nbsp;&nbsp;&nbsp;\'.$field[\'label\'];
            $field[\'name\'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo \'
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina ' . str_replace("'", "\'", $moduleDescription['label']) . '
        </div>\';
        $fieldsArray= array(';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['name'] == 'trashed')
                continue;
            else if ($field['type'] == 'object') {
                $returnContent .= '
                        \'' . str_replace("'", "\'", $field['label']) . '\' => TBPX . \'' . $field['typeoptions']['name'] . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '\',';
            } else {
                $returnContent .= '
                        \'' . str_replace("'", "\'", $field['label']) . '\' => TBPX . \'' . $moduleDescription['name'] . '.' . $field['name'] . '\',';
            }
        }
        $returnContent .= '
                );
        echo \'
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>\';
        for($i=1; $i<=3; $i++){
            echo \'
              <tr>
                <td><label>\'.$i.\'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="Order' . $moduleDescription['name'] . '__field\'.$i.\'" name="Order' . $moduleDescription['name'] . '__field\'.$i.\'">
                    <option value="">Scegli...</option>\';
            foreach($fieldsArray as $key => $value){
              echo \'
                    <option \'.($value == $_SESSION[\'Order' . $moduleDescription['name'] . '\'][\'field\'.$i] ? \'selected="selected"\' : \'\').\' value="\'.$value.\'">\'.$key.\'</option>\';
            }
            echo \'
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" \'.($_SESSION[\'Order' . $moduleDescription['name'] . '\'][\'field\'.$i.\'_direction\']==\'DESC\' ? \'checked="checked"\' : \'\').\' value="DESC" id="Order' . $moduleDescription['name'] . '__field\'.$i.\'_direction" name="Order' . $moduleDescription['name'] . '__field\'.$i.\'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>\';
        }
        echo \'
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\\\'' . $moduleDescription['name'] . '.php?operation=emptyFilterOrder\\\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>\';

    if (($_SESSION[\'Mask\'][$moduleId][\'FILESVIEW\'] || $_SESSION[\'Mask\'][$moduleId][\'FILESMANAGE\'])
        && ($_REQUEST[\'operation\'] == \'manageFiles\')
    ) {
        $ModuleHelper->echoFileBox($' . $moduleDescription['name'] . ', $_REQUEST[\'File__id\']);
    }

    echo \'
        <div id="box_operation_buttons" class="align_center">\';

    if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1 && ($_SESSION[\'Mask\'][$moduleId][\'INSERT\'] || $_SESSION[\'Mask\'][$moduleId][\'DUPLICATE\']))
      echo \'
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>\';

    echo \'
        <a href="#" onclick="$(\\\'#FilterBox\\\').dialog(\\\'open\\\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>\';

    if (isset($_SESSION[\'Filter' . $moduleDescription['name'] . '\']) || isset($_SESSION[\'Order' . $moduleDescription['name'] . '\']))
      echo \'
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\\\'' . $moduleDescription['name'] . '.php?operation=emptyFilterOrder\\\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>\';

    if ($_SESSION[\'Mask\'][$moduleId][\'EXCEL\'])
      echo \'
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\\\'' . $moduleDescription['name'] . '_operations.php?operation=excel\\\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>\';

    if ($_SESSION[\'Mask\'][$moduleId][\'PDF\'])
      echo \'
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\\\'' . $moduleDescription['name'] . '_operations.php?operation=pdf\\\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>\';

    if ($_SESSION[\'Mask\'][$moduleId][\'TRASH\']){
      if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1){
        $trashCount = $' . $moduleDescription['name'] . '->getTrashCount();
        $trashImg = $trashCount > 0 ? \'../img/icons/nuvola/16x16/filesystems/trashcan_full.png\' : \'../img/icons/nuvola/16x16/filesystems/trashcan_empty.png\';
        echo \'
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\\\'' . $moduleDescription['name'] . '.php?operation=viewTrashed\\\';return false;
          ">
            <img src="\'.$trashImg.\'" alt="cestino" width="16" height="16" />
            <span>Cestino\'.($trashCount > 0 ? \' (\'.$trashCount.\')\' : \'\').\'</span>
          </a>\';
      }
      else
        echo \'
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\\\'' . $moduleDescription['name'] . '.php?operation=viewNotTrashed\\\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>\';
    }
    echo \'
        </div>\';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $' . $moduleDescription['name'] . '->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION[\'Navbar' . $moduleDescription['name'] . '\'][\'bloccoAttuale\'], $_SESSION[\'Navbar' . $moduleDescription['name'] . '\'][\'numeroPagina\'], $_REQUEST[\'getRequest\']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();

    $getRequest .= "&";
    echo $navBar[\'css\'];
    echo $navBar[\'barra\'];

    echo \'
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="\'.intval($totRecords[0]).\'"/>
    <div class="align_left table_box">
      <table border="1" id="Table' . $moduleDescription['name'] . '">
        <caption class="align_center font12 bold gray padding2 vertical_middle">\'.$ModuleHelper->getModuleLogoAndLabel().\'</caption>
        <thead>
          <tr class="Table' . $moduleDescription['name'] . '_header">\';
    if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1)
        echo \'
            <th width="1%" class="selectorArrow">&nbsp;</th>\';
    echo \'
            <th width="1%" class="no_wrap"><a href="?operation=order&amp;Order' . $moduleDescription['name'] . '__field1=\' . TBPX . \'' . $moduleDescription['name'] . '.' . 'id' . '" title="Ordina per ' . str_replace("'", "\'", 'Id') . '.">' . str_replace("'", "\'", 'Id') . '\' . ($orderFields[TBPX . \'' . $moduleDescription['name'] . '.' . 'id' . '\'] ? \'&nbsp;\'.($orderFields[TBPX . \'' . $moduleDescription['name'] . '.' . 'id' . '\'] == \'DESC\' ? \'&#9662;\' : \'&#9652;\') : \'\') . \'</a></th>\';
    echo \'';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $field['intable'] = isset($field['intable']) ? $field['intable'] : true;

            $orderArrow = '\' . ($orderFields[TBPX . \'User.id\'] ? ($orderFields[TBPX . \'User.id\'] == \'DESC\' ? \' &#9662;\' : \' &#9652;\') : \'\') . \' ';

            if ($field['name'] == 'coverFile' ||
                $field['name'] == 'id' ||
                $field['name'] == 'user' ||
                $field['name'] == 'active' ||
                $field['name'] == 'trashed' ||
                $field['type'] == 'crud' ||
                $field['intable'] === false
            ) {
                continue;
            } else if ($field['type'] == 'object') {
                $returnContent .= '
            <th class="no_wrap"><a href="?operation=order&amp;Order' . $moduleDescription['name'] . '__field1=\' . TBPX . \'' . $field['typeoptions']['name'] . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '" title="Ordina per ' . str_replace("'", "\'", $field['label']) . '.">' . str_replace("'", "\'", $field['label']) . '\' . ($orderFields[TBPX . \'' . $field['typeoptions']['name'] . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '\'] ? \'&nbsp;\'.($orderFields[TBPX . \'' . $field['typeoptions']['name'] . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '\'] == \'DESC\' ? \'&#9662;\' : \'&#9652;\') : \'\') . \'</a></th>';
            } else {
                $returnContent .= '
            <th class="no_wrap"><a href="?operation=order&amp;Order' . $moduleDescription['name'] . '__field1=\' . TBPX . \'' . $moduleDescription['name'] . '.' . $field['name'] . '" title="Ordina per ' . str_replace("'", "\'", $field['label']) . '.">' . str_replace("'", "\'", $field['label']) . '\' . ($orderFields[TBPX . \'' . $moduleDescription['name'] . '.' . $field['name'] . '\'] ? \'&nbsp;\'.($orderFields[TBPX . \'' . $moduleDescription['name'] . '.' . $field['name'] . '\'] == \'DESC\' ? \'&#9662;\' : \'&#9652;\') : \'\') . \'</a></th>';
            }
        }
        $returnContent .= '
            <th width="1%"><a href="?operation=order&amp;Order' . $moduleDescription['name'] . '__field1=\' . TBPX . \'' . $moduleDescription['name'] . '.active" title="Ordina per stato attivo/non attivo.">AT\' . ($orderFields[TBPX . \'' . $moduleDescription['name'] . '.active\'] ? \'&nbsp;\'.($orderFields[TBPX . \'' . $moduleDescription['name'] . '.active\'] == \'DESC\' ? \'&#9662;\' : \'&#9652;\') : \'\') . \'</a></th>\';
    if ($_SESSION[\'Mask\'][$moduleId][\'FILESVIEW\'] || $_SESSION[\'Mask\'][$moduleId][\'FILESMANAGE\'])
      echo \'
            <th width="1%">&nbsp;</th>\';
    if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1){
      if ($_SESSION[\'Mask\'][$moduleId][\'UPDATE\'])
        echo \'
            <th width="1%">&nbsp;</th>\';
      if ($_SESSION[\'Mask\'][$moduleId][\'TRASH\'])
        echo \'
            <th width="1%">&nbsp;</th>\';
    }
    else{
      if ($_SESSION[\'Mask\'][$moduleId][\'TRASH\'])
        echo \'
            <th width="1%">&nbsp;</th>\';
    }
    echo \'
          </tr>
        </thead>
        <tbody>\';

    $sql = $' . $moduleDescription['name'] . '->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $count = 0;
    while($' . $moduleDescription['name'] . '_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'foreignFieldsList\']);
      $foreignSequence=\'\';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$' . $moduleDescription['name'] . '_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,\' \');

      if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($' . $moduleDescription['name'] . '_array["id"]==$_REQUEST["' . $moduleDescription['name'] . '__id"])
        $colorazione_riga="selected_table_row";

      echo \'
            <tr\';
      if ($_SESSION[\'Mask\'][$moduleId][\'UPDATE\'])
        echo \' ondblclick="location.href=\\\'?operation=modify&amp;' . $moduleDescription['name'] . '__id=\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'\\\';"\';
      echo \' class="\'.$colorazione_riga.\' contextMenuTableRecord Table' . $moduleDescription['name'] . '_row" id="record\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'">\';

      if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1)
        echo \'
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\\\'Foreign\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'objectName\'].\'__\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'fieldName\'].\'\\\').value=unescape(\\\'\'.str_replace(\'+\',\' \',urlencode(utf8_decode($foreignSequence))).\'\\\');
                      window.opener.document.getElementById(\\\'\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'objectName\'].\'__\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'fieldName\'].\'\\\').value=\\\'\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'\\\';
                      window.close();
                    }
                    else
                      swal(\\\'Nessuna operazione di selezione richiesta.\\\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>\';
      echo \'              
            <td>\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'</td>\';            
      echo \'';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $field['intable'] = isset($field['intable']) ? $field['intable'] : true;

            if ($field['name'] == 'coverFile' ||
                $field['name'] == 'id' ||
                $field['name'] == 'user' ||
                $field['name'] == 'active' ||
                $field['name'] == 'trashed' ||
                $field['type'] == 'crud' ||
                (is_bool($field['intable']) && $field['intable'] === false)
            ) {
                continue;
            } else if ($field['name'] == 'lastDateTime') {
                $returnContent .= '\';
        $' . $moduleDescription['name'] . '_array[\'lastDateTime\']=date(\'d/m/Y H:i:s\', strtotime($' . $moduleDescription['name'] . '_array[\'lastDateTime\']));
      echo \'
            <td class="no_wrap"><span class="font8">\'.$' . $moduleDescription['name'] . '_array[\'lastDateTime\'].\'</span><br/><span class="font9">Utente: \'.$' . $moduleDescription['name'] . '_array[\'user_userName\'].\'</span></td>';
            } else if ($field['type'] == 'flag') {
                $returnContent .= '
            <td>\'.($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']==1 ? \'SI\' : \'NO\').\'</td>';
            } else if ($field['type'] == 'integer') {
                $returnContent .= '
            <td>\'.$' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'].\'</td>';
            } else if ($field['type'] == 'decimal') {
                $returnContent .= '
            <td>\'.number_format($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'],2,\',\',\'.\').\'</td>';
            } else if ($field['type'] == 'date') {
                $returnContent .= '\';
        if (trim($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'])!=\'\')
          $' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']=date("d/m/Y", strtotime($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']));
        echo \'
            <td>\'.$' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'].\'</td>';
            } else if ($field['type'] == 'datetime') {
                $returnContent .= '\';
        if (trim($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'])!=\'\')
          $' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']=date("d/m/Y H:i:s", strtotime($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']));
        echo \'
            <td>\'.$' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'].\'</td>';
            } else if ($field['type'] == 'text') {
                $returnContent .= '
            <td>\';
      $' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'] = htmlspecialchars(trim($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']));
      if ($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\']!=\'\'){
        $maxlen = 35;
        if (strlen($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'])>$maxlen){
            echo \'
              <div class="tooltip-ajax help" data-field="' . $field['name'] . '" data-id="\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'" data-title="' . str_replace("'", "\'", $field['label']) . '">
                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                <span>\'.mb_strimwidth($' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'], 0, $maxlen, "...").\'</span>
              </div>\';
        } else
            echo $' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'];
      }
      else
        echo \'&nbsp;\';
      echo \'
            </td>';
            } else if ($field['type'] == 'object') {
                $returnContent .= '
            <td>\'.$' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '_' . $field['typeoptions']['field'] . '\'].\'</td>';
            } else {
                $returnContent .= '
            <td>\'.$' . $moduleDescription['name'] . '_array[\'' . $field['name'] . '\'].\'</td>';
            }
        }
        $returnContent .= ' \';
         echo \'
            <td class="vertical_middle">\';
      if ($_SESSION[\'Mask\'][$moduleId][\'ACTIVATE\']){
        if($' . $moduleDescription['name'] . '_array[\'active\']==1)
          echo \'
              <a href="#" title="disattiva" onclick="location.href=\\\'' . $moduleDescription['name'] . '_operations.php?operation=changeField&' . $moduleDescription['name'] . '__id=\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'&' . $moduleDescription['name'] . '__active=0\\\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>\';
        else
          echo \'
              <a href="#" title="attiva" onclick="location.href=\\\'' . $moduleDescription['name'] . '_operations.php?operation=changeField&' . $moduleDescription['name'] . '__id=\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'&' . $moduleDescription['name'] . '__active=1\\\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>\';
      }
      else{
        if($' . $moduleDescription['name'] . '_array[\'active\']==1)
          echo \'<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>\';
        else
          echo \'<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>\';
      }
        echo \'
            </td>\';

      if ($_SESSION[\'Mask\'][$moduleId][\'FILESVIEW\'] || $_SESSION[\'Mask\'][$moduleId][\'FILESMANAGE\']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($' . $moduleDescription['name'] . '_array[\'id\']);
        if ($filesCount > 0){
            $icon=\'../img/icons/nuvola/16x16/filesystems/folder_green.png\';
            $dataTitle=\'File allegati\';
            $dataBody=\'Ci sono \'.$filesCount.\' file allegati.\';
            if ($filesCount == 1){
                $dataTitle=\'File allegato\';
                $dataBody=\'C\\\'&egrave; un file allegato.\';
            }
        }
        else{
          $icon=\'../img/icons/nuvola/16x16/filesystems/folder_grey_open.png\';
          $dataTitle=\'Nessun allegato\';
          $dataBody=\'Non ci sono file allegati. Click per aggiungerne.\';
        }
        echo \'
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="\'.$dataTitle.\'" data-body="\'.$dataBody.\'" onclick="location.href=\\\'?operation=manageFiles&' . $moduleDescription['name'] . '__id=\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'\\\';return false;">
                  <img src="\'.$icon.\'" alt="files" width="16" height="16"/>
                </a>\';
        if ($' . $moduleDescription['name'] . '_array[\'coverFile\']) {
            echo \'
                <a href="_Download.php?File__id=\' . $' . $moduleDescription['name'] . '_array[\'coverFile\'] . \'" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="\' . $' . $moduleDescription['name'] . '_array[\'coverFile\'] . \'" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>\';
        }
        echo \'
              </td>\';
      }

      if ($_SESSION[\'ViewTrashed' . $moduleDescription['name'] . '\']!=1){
        if ($_SESSION[\'Mask\'][$moduleId][\'UPDATE\'])
          echo \'
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\\\'?operation=modify&amp;' . $moduleDescription['name'] . '__id=\'.$' . $moduleDescription['name'] . '_array[\'id\'].\'\\\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>\';

        if ($_SESSION[\'Mask\'][$moduleId][\'TRASH\'])
          echo \'
              <td class="vertical_middle">\' . $ModuleHelper->renderTrashAction($' . $moduleDescription['name'] . '_array[\'id\']) . \'</td>\';
      }
      else{
        if ($_SESSION[\'Mask\'][$moduleId][\'TRASH\'])
          echo \'
              <td class="vertical_middle">\' . $ModuleHelper->renderRestoreAction($' . $moduleDescription['name'] . '_array[\'id\']) . \'</td>\';
      }
      echo \'
            </tr>\';
    }
    echo \'
          </tbody>
        </table>
      </div>\';
    echo $navBar[\'barra\'];
    echo \'<br/>\';      
}
echo \'
<script>
function submitObjectForm(){
    $(window).unbind(\\\'beforeunload\\\');
    
    if (false){}';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $field['mandatory'] = isset($field['mandatory']) ? $field['mandatory'] : true;

            if ($field['name'] == 'id' ||
                $field['name'] == 'coverFile' ||
                $field['name'] == 'lastDateTime' ||
                $field['name'] == 'user' ||
                $field['name'] == 'active' ||
                $field['name'] == 'trashed' ||
                $field['mandatory'] === false
            ) {
                continue;
            } else if ($field['type'] == 'crud') {
                $returnContent .= '
    else if ($(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val()==\\\'\\\' || $(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val()==\\\'[]\\\'){
        $("[for=\\\'' . $moduleDescription['name'] . '__' . $field['name'] . '\\\']").scrollViewAndFocus();
        swal(\\\'Attenzione! Compilare il campo: ' . str_replace("'", "\\\\\'", $field['label']) . '.\\\');
    }';
            } else {
                $returnContent .= '
    else if ($(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').val()==\\\'\\\'){
        $(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').scrollViewAndFocus();
        swal(\\\'Attenzione! Compilare il campo: ' . str_replace("'", "\\\\\'", $field['label']) . '.\\\');
    }';
            }
        }
        $returnContent .= '
    else
        $(\\\'#objectForm\\\').submit();
    return false;
}\';
    $ModuleHelper->echoDefaultJsFunctions();
echo \'
$(document).ready(function(){
    if (window.opener){
        $(\\\'.selectorArrow\\\').show();        
    }

    if (window.opener && \\\'\'.$_REQUEST[\'operation\'].\'\\\' == \\\'updated\\\'){
      window.opener.document.getElementById(\\\'Foreign\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'objectName\'].\'__\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'fieldName\'].\'\\\').value=unescape(\\\'\'.str_replace(\'+\',\' \',urlencode(utf8_decode($foreignSequence_updated))).\'\\\');
      window.opener.document.getElementById(\\\'\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'objectName\'].\'__\'.$_SESSION[\'RemoteSearch' . $moduleDescription['name'] . '\'][\'fieldName\'].\'\\\').value=\\\'\'.$_REQUEST[\'' . $moduleDescription['name'] . '__id\'].\'\\\';
      window.close();
      return false;
    }\';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST[\'operation\']==\'manageFiles\'){
    echo \'
    $(\\\'#FileBox\\\').dialog(\\\'open\\\');\';
}

if ($_REQUEST[\'operation\']==\'remoteSearchCall\'){
    echo \'
    $(\\\'#FilterBox\\\').dialog(\\\'open\\\');\';
}

if ($_REQUEST[\'mex\']!=\'\')
    echo \'swal(\\\'\'.$_REQUEST[\'mex\'].\'\\\')\';
';

        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['type'] == 'crud') {
                $returnContent .= '
echo \'
$(\\\'#' . $moduleDescription['name'] . '__' . $field['name'] . '\\\').textareacrud_configurable(\\\'' . $moduleDescription['name'] . '\\\', \\\'\'.json_encode($moduleDescription[\'fields\'][\'' . $field['name'] . '\'], JSON_HEX_APOS).\'\\\', true);\';';
            }
        }


        $returnContent .= '           
echo \'
});
</script>\';


$Design->privateHtmlClose();

?>';
        return $returnContent;
    }

    static function generateOperationsFileContent($Module__id, $moduleDescription)
    {
        $returnContent = '<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/' . $moduleDescription['name'] . '.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = ' . $Module__id . ';
$objectName = \'' . $moduleDescription['name'] . '\';
$Object = new ' . $moduleDescription['name'] . '($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);

try {
    if (!isset($_SESSION[\'Mask\'][$moduleId]))
        throw new Exception(\'authorizationDenied\');

    switch ($_REQUEST[\'operation\']) {

        case "autocomplete":
            echo $ModuleHelper->autocomplete($_REQUEST[\'type\'], $_REQUEST[\'field\'], $_REQUEST[\'term\']);
            break;


        case "echoField":
            echo $ModuleHelper->getField($_REQUEST[\'type\'], $_REQUEST[\'field\'], $_REQUEST[\'id\']);
            break;


        case "echoImage":
            echo $ModuleHelper->getImageHtml($_REQUEST[\'type\'], $_REQUEST[\'field\'], $_REQUEST[\'id\'], $_REQUEST[\'secretKey\']);
            break;


        case "save":';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            $field['inform'] = isset($field['inform']) ? $field['inform'] : true;

            if ($field['name'] == 'active' ||
                $field['name'] == 'trashed' ||
                $field['name'] == 'lastDateTime' ||
                $field['name'] == 'user' ||
                $field['name'] == 'coverFile' ||
                (is_bool($field['inform']) && $field['inform'] === false)
            ) {
                continue;
            } else if ($field['type'] == 'decimal') {
                $returnContent .= '
            if ($_REQUEST[$objectName . \'__' . $field['name'] . '\'] != \'\')
                $_REQUEST[$objectName . \'__' . $field['name'] . '\'] = str_replace(\',\', \'.\', $_REQUEST[$objectName.\'__' . $field['name'] . '\']);';
            } else if ($field['type'] == 'date') {
                $returnContent .= '
            $_REQUEST[$objectName . \'__' . $field['name'] . '\'] = $_REQUEST[$objectName . \'__' . $field['name'] . '\'] ? date("Y-m-d", strtotime(str_replace(\'/\', \'-\', $_REQUEST[$objectName.\'__' . $field['name'] . '\']))) : null;';
            } else if ($field['type'] == 'flag') {
                $returnContent .= '
            if ($_REQUEST[$objectName . \'__' . $field['name'] . '\'] != \'1\')
                $_REQUEST[$objectName . \'__' . $field['name'] . '\'] = 0;';
            } else if ($field['type'] == 'object') {
                $returnContent .= '
            if ($_REQUEST[$objectName . \'__' . $field['name'] . '\'] == \'\')
                $_REQUEST[$objectName . \'__' . $field['name'] . '\'] = null;';
            }
        }
        $returnContent .= '

            if ($_REQUEST[$objectName . \'__id\'] == \'\') {
                if (!$_SESSION[\'Mask\'][$moduleId][\'INSERT\'] && !$_SESSION[\'Mask\'][$moduleId][\'DUPLICATE\'])
                    throw new Exception(\'authorizationDenied\');
                $id = $ModuleHelper->insert($_REQUEST);
            } else {
                if (!$_SESSION[\'Mask\'][$moduleId][\'UPDATE\'])
                    throw new Exception(\'authorizationDenied\');
                $id = $ModuleHelper->update($_REQUEST);
            }
            header("Location: " . $objectName . ".php?operation=updated&" . $objectName . "__id=" . $id);
            break;


        case "changeField":
            if (isset($_REQUEST[$objectName . \'__trashed\'])) {
                if (!$_SESSION[\'Mask\'][$moduleId][\'TRASH\'])
                    throw new Exception(\'authorizationDenied\');
                $arrayObject = array(
                    $objectName . \'__id\' => $_REQUEST[$objectName . \'__id\'],
                    $objectName . \'__trashed\' => $_REQUEST[$objectName . \'__trashed\'],
                );
            } else if (isset($_REQUEST[$objectName . \'__active\'])) {
                if (!$_SESSION[\'Mask\'][$moduleId][\'ACTIVATE\'])
                    throw new Exception(\'authorizationDenied\');
                $arrayObject = array(
                    $objectName . \'__id\' => $_REQUEST[$objectName . \'__id\'],
                    $objectName . \'__active\' => $_REQUEST[$objectName . \'__active\'],
                );
            } else {
                throw new Exception(\'invalidOperation\');
            }
            $id = $ModuleHelper->update($arrayObject);
            header("Location: " . $objectName . ".php?operation=updated&" . $objectName . "__id=" . $id);
            break;


        case "excel":
            if (!$_SESSION[\'Mask\'][$moduleId][\'EXCEL\'])
                throw new Exception(\'authorizationDenied\');
            $ModuleHelper->outputExcel();
            break;


        case "pdf":
            if (!$_SESSION[\'Mask\'][$moduleId][\'PDF\'])
                throw new Exception(\'authorizationDenied\');
            $ModuleHelper->outputPdf();
            break;


        case "saveFile":
            if (!$_SESSION[\'Mask\'][$moduleId][\'FILESMANAGE\'])
                throw new Exception(\'authorizationDenied\');
            if ($_FILES[\'File__file\'][\'size\'] < UPLOADED_LIMIT_FILESIZE)
                $ModuleHelper->saveFile($_REQUEST, $_FILES);
            else
                $mex = "Attenzione impossibile salvare, il file supera la dimensione massima consentita.";
            header("Location: " . $objectName . ".php?operation=manageFiles&" . $objectName . "__id=" . $_REQUEST[\'File__objectId\'] . ($mex ? \'&mex=\' . $mex : \'\'));
            break;

        
        case "deleteFile":
            if (!$_SESSION[\'Mask\'][$moduleId][\'FILESMANAGE\'])
                throw new Exception(\'authorizationDenied\');
            $ModuleHelper->deleteFile($_REQUEST[\'File__id\'], $_REQUEST[\'isCoverFile\']);
            header("Location: " . $objectName . ".php?operation=manageFiles&" . $objectName . "__id=" . $_REQUEST[\'File__objectId\']);
            break;


        default:
            throw new Exception(\'authorizationDenied\');
    }
} catch (Exception $e) {
    echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"it-IT\">
<head><title>Error!</title></head>
<body>
    <script>top.location.href=\'_LoginOperations.php?operation=logout&message=" . $e->getMessage() . "\'</script>
</body>
</html>";
} 
exit;

?>';
        return $returnContent;
    }

}

?>
