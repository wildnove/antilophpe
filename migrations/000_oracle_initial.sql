CREATE TABLE "ant_Action" (
"id" NUMBER(10) NOT NULL,
"module" NUMBER(10)  NULL,
"action" VARCHAR2(50) NULL,
"coverFile" NUMBER(10)  NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10)  NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0  NOT NULL,
PRIMARY KEY("id"),
UNIQUE ("module","action")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_Action_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_Action_seq_tr"
 BEFORE INSERT ON "ant_Action" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_Action_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_Action_seq_tr";
-- DROP SEQUENCE "ant_Action_seq";
-- DROP TABLE "ant_Action" cascade constraints PURGE;

CREATE TABLE "ant_Module" (
"id" NUMBER(10) NOT NULL,
"name" VARCHAR2(50) NULL,
"label" VARCHAR2(50) NULL,
"url" VARCHAR2(4000) NULL,
"urlTarget" VARCHAR2(255) NULL,
"parentModule" NUMBER(10) NULL,
"siblingOrder" NUMBER(10) NULL,
"icon" VARCHAR2(255) NULL,
"jsonDescription" VARCHAR2(4000) NULL,
"coverFile" NUMBER(10) NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10) NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id"),
UNIQUE ("name")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_Module_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_Module_seq_tr"
 BEFORE INSERT ON "ant_Module" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_Module_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_Module_seq_tr";
-- DROP SEQUENCE "ant_Module_seq";
-- DROP TABLE "ant_Module" cascade constraints PURGE;

INSERT INTO "ant_Module" VALUES(1, '', '', '_WorkFrame.php', '_WorkFrame', '0', '0', '', '', NULL, TO_TIMESTAMP('2016-12-22 17:57:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Module" VALUES(2, 'System', 'Sistema', '', '', '1', '2', 'apps/package_system.png', '', NULL, TO_TIMESTAMP('2016-12-22 17:57:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Module" VALUES(3, 'Module', 'Moduli', 'Module.php', '_WorkFrame', 2, 2, 'apps/kalzium.png', '{"id":"3","name":"Module","label":"Moduli","icon":"apps\/kalzium.png","parentmodule":2,"siblingorder":2,"orderby":["parentModule","siblingOrder"],"fields":{"name":{"label":"Nome","type":"string","maxlen":50,"index":"unique","mandatory":false},"label":{"label":"Label","type":"string","maxlen":50,"index":"none"},"url":{"label":"URL","type":"text","mandatory":false},"urlTarget":{"label":"URL target","type":"string","maxlen":255,"index":"none"},"parentModule":{"label":"Modulo padre","type":"object","mandatory":false,"typeoptions":{"name":"Module","field":"name"}},"siblingOrder":{"label":"Ordine fratelli","type":"integer","maxlen":4,"mandatory":false,"default":0},"icon":{"label":"Icona","type":"string","maxlen":255,"index":"none"},"jsonDescription":{"label":"JSON Descrittivo","type":"text","mandatory":false,"index":"none"}}}', NULL, TO_TIMESTAMP('2017-01-19 11:34:17', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 3, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 3, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);

INSERT INTO "ant_Module" VALUES(4, 'Action', 'Azioni', 'Action.php', '_WorkFrame', 2, 4, 'filesystems/exec.png', '{"id":"4","name":"Action","label":"Azioni","icon":"filesystems\/exec.png","parentmodule":2,"siblingorder":4,"orderby":["module","id"],"fields":{"module":{"label":"Modulo","type":"object","typeoptions":{"name":"Module","field":"name"}},"action":{"label":"Azione","type":"string","maxlen":50}}}', NULL, TO_TIMESTAMP('2017-01-19 11:15:30', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 4, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 4, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);

CREATE TABLE "ant_Mask" (
"id" NUMBER(10)  NOT NULL,
"mask" VARCHAR2(50) NULL,
"coverFile" NUMBER(10)  NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10)  NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id"),
UNIQUE ("mask")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_Mask_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_Mask_seq_tr"
 BEFORE INSERT ON "ant_Mask" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_Mask_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_Mask_seq_tr";
-- DROP SEQUENCE "ant_Mask_seq";
-- DROP TABLE "ant_Mask" cascade constraints PURGE;

INSERT INTO "ant_Module" VALUES(5, 'Mask', 'Maschere', 'Mask.php', '_WorkFrame', 2, 6, 'apps/kdf.png', '{"id":"5","name":"Mask","label":"Maschere","icon":"apps\/kdf.png","parentmodule":2,"siblingorder":6,"orderby":["mask"],"fields":{"mask":{"label":"Maschera","type":"string","index":"unique","maxlen":50}}}', NULL, TO_TIMESTAMP('2017-01-19 11:41:58', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 5, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 5, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);

CREATE TABLE "ant_Authorization" (
"id" NUMBER(10) NOT NULL,
"mask" NUMBER(10) NULL,
"action" NUMBER(10)  NULL,
"coverFile" NUMBER(10)  NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10) NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id"),
UNIQUE ("mask","action")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_Authorization_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_Authorization_seq_tr"
 BEFORE INSERT ON "ant_Authorization" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_Authorization_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_Authorization_seq_tr";
-- DROP SEQUENCE "ant_Authorization_seq";
-- DROP TABLE "ant_Authorization" cascade constraints PURGE;

INSERT INTO "ant_Module" VALUES(6, 'Authorization', 'Autorizzazioni', 'Authorization.php', '_WorkFrame', 2, 8, 'apps/kdisknav.png', '{"id":"6","name":"Authorization","label":"Autorizzazioni","icon":"apps\\/kdisknav.png","parentmodule":2,"siblingorder":8,"orderby":["mask","action"],"fields":{"mask":{"label":"Maschera","type":"object","typeoptions":{"name":"Mask","field":"mask"}},"action":{"label":"Azione","type":"object","typeoptions":{"name":"Action","field":"action"}}}}', NULL, TO_TIMESTAMP('2017-01-19 11:42:48', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 6, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 6, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);

CREATE TABLE "ant_User" (
"id" NUMBER(10) NOT NULL,
"denomination" VARCHAR2(255) NULL,
"userName" VARCHAR2(50) NULL,
"userPassword" VARCHAR2(255) NULL,
"mask" NUMBER(10) NULL,
"email" VARCHAR2(255) NULL,
"passwordLastUpdate" TIMESTAMP(0) NULL,
"passwordExpirationDays" NUMBER(10) NULL,
"coverFile" NUMBER(10) NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10) NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id"),
UNIQUE ("userName")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_User_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_User_seq_tr"
 BEFORE INSERT ON "ant_User" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_User_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_User_seq_tr";
-- DROP SEQUENCE "ant_User_seq";
-- DROP TABLE "ant_User" cascade constraints PURGE;

INSERT INTO "ant_Module" VALUES(7, 'User', 'Utenti', 'User.php', '_WorkFrame', 2, 10, 'apps/kgpg.png', '{"id":"7","name":"User","label":"Utenti","icon":"apps\\/kgpg.png","parentmodule":2,"siblingorder":10,"orderby":["denomination"],"fields":{"denomination":{"label":"Denominazione","type":"string","maxlen":255},"userName":{"label":"Utente","type":"string","maxlen":50,"index":"unique"},"userPassword":{"label":"Password","type":"string","maxlen":255,"index":"none","intable":false,"inexcel":false,"inpdf":false,"infilter":false},"mask":{"label":"Maschera","type":"object","mandatory":false,"typeoptions":{"name":"Mask","field":"mask"}},"email":{"label":"Email","type":"string","maxlen":255},"passwordLastUpdate":{"label":"Agg. password","type":"datetime","inform":false},"passwordExpirationDays":{"label":"GG scad. password","type":"integer","maxlen":4,"intable":false,"infilter":false}}}', NULL, TO_TIMESTAMP('2017-01-19 11:45:50', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 7, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 7, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);

INSERT INTO "ant_User" VALUES (1, ' Admin', 'admin', 'd0b2551ed62cabf9968ef4fa9478e1b8eaa87f71b5501b697267d8a2edd360a2', NULL, 'antilophpe@ncfsistemi.com', TO_TIMESTAMP('2016-12-22 18:48:29', 'YYYY-MM-DD HH24:MI:SS'), 9999, NULL, TO_TIMESTAMP('2016-12-22 18:48:29', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
-- default admin password is "antilophpe"

CREATE TABLE "ant_File" (
"id" NUMBER(10) NOT NULL,
"objectName" VARCHAR2(50) NULL,
"objectId" NUMBER(10) NULL,
"title" VARCHAR2(255) NULL,
"description" VARCHAR2(4000) NULL,
"originalFilename" VARCHAR2(255) NULL,
"mimetype" VARCHAR2(50) NULL,
"extension" VARCHAR2(30) NULL,
"sizeKb" NUMBER(8,2) NULL,
"widthPx" NUMBER(10) NULL,
"heightPx" NUMBER(10) NULL,
"hasThumbnail" NUMBER(3) DEFAULT 0 NOT NULL,
"hasResampled" NUMBER(3) DEFAULT 0 NOT NULL,
"coverFile" NUMBER(10) NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10) NOT NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_File_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_File_seq_tr"
 BEFORE INSERT ON "ant_File" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_File_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_File_seq_tr";
-- DROP SEQUENCE "ant_File_seq";
-- DROP TABLE "ant_File" cascade constraints PURGE;

INSERT INTO "ant_Module" VALUES(8, 'File', 'File allegati', 'File.php', '_WorkFrame', 2, 12, 'filesystems/folder_green.png', '{"id":"8","name":"File","label":"File allegati","icon":"filesystems\\/folder_green.png","parentmodule":2,"siblingorder":12,"orderby":["objectName","objectId"],"fields":{"objectName":{"label":"Oggetto - name","type":"string","maxlen":50},"objectId":{"label":"Oggetto - id","type":"integer","maxlen":8},"title":{"label":"Titolo","type":"string","maxlen":255},"description":{"label":"Descrizione","type":"text","mandatory":false},"originalFilename":{"label":"Filename originale","type":"string","maxlen":255},"mimetype":{"label":"Mimetype","type":"string","maxlen":50},"extension":{"label":"Estensione","type":"string","maxlen":30},"sizeKb":{"label":"Dimensione kB","type":"decimal","maxlen":8},"widthPx":{"grouplabel":"Dati immagine","label":"Larghezza px","type":"integer","maxlen":8,"mandatory":false},"heightPx":{"grouplabel":"Dati immagine","label":"Altezza px","type":"integer","maxlen":8,"mandatory":false},"hasThumbnail":{"grouplabel":"Dati immagine","label":"Esiste thumbnail","type":"flag","mandatory":false},"hasResampled":{"grouplabel":"Dati immagine","label":"Esiste resampled","type":"flag","mandatory":false}}}', NULL, TO_TIMESTAMP('2017-01-19 11:48:58', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 8, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 8, 'DELETE', NULL, TO_TIMESTAMP('2016-12-31 15:24:04', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

CREATE TABLE "ant_Log" (
"id" NUMBER(10) NOT NULL,
"type" VARCHAR2(100) NULL,
"log" VARCHAR2(4000) NULL,
"ip" VARCHAR2(26) NULL,
"browser" VARCHAR2(255) NULL,
"coverFile" NUMBER(10) NULL,
"lastDateTime" TIMESTAMP(0) NOT NULL,
"user" NUMBER(10) NULL,
"active" NUMBER(3) DEFAULT 1 NOT NULL,
"trashed" NUMBER(3) DEFAULT 0 NOT NULL,
PRIMARY KEY("id")
);

-- Generate ID using sequence and trigger
CREATE SEQUENCE "ant_Log_seq" START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER "ant_Log_seq_tr"
 BEFORE INSERT ON "ant_Log" FOR EACH ROW
 WHEN (NEW."id" IS NULL)
BEGIN
 SELECT "ant_Log_seq".NEXTVAL INTO :NEW."id" FROM DUAL;
END;
/
-- DROP TRIGGER "ant_Log_seq_tr";
-- DROP SEQUENCE "ant_Log_seq";
-- DROP TABLE "ant_Log" cascade constraints PURGE;


INSERT INTO "ant_Module" VALUES(9, 'Log', 'Log', 'Log.php', '_WorkFrame', 2, 14, 'actions/view_text.png', '{"id":"9","name":"Log","label":"Log","icon":"actions\\/view_text.png","parentmodule":2,"siblingorder":14,"orderby":["lastDateTime DESC"],"fields":{"type":{"label":"Tipo","type":"string","maxlen":100},"log":{"label":"Log","type":"text"},"ip":{"label":"IP","type":"string","maxlen":26},"browser":{"label":"Browser","type":"string","maxlen":255}}}', NULL, TO_TIMESTAMP('2017-01-19 11:51:02', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);

INSERT INTO "ant_Action" VALUES(NULL, 9, 'VIEW', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 1, 0);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'INSERT', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'UPDATE', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'ACTIVATE', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'TRASH', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'FILESVIEW', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'FILESMANAGE', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'DUPLICATE', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'EXCEL', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1);
INSERT INTO "ant_Action" VALUES(NULL, 9, 'PDF', NULL, TO_TIMESTAMP('2016-12-31 15:45:21', 'YYYY-MM-DD HH24:MI:SS'), 1, 0, 1); 

-- Sequence manual update no incrment
-- Module
ALTER SEQUENCE "ant_Module_seq" INCREMENT BY 10;
select "ant_Module_seq".nextval from dual;
ALTER SEQUENCE "ant_Module_seq" INCREMENT BY 1;

-- User
select "ant_User_seq".nextval from dual;

COMMIT;