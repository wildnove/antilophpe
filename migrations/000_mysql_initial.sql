-- TBPX = ant_

CREATE TABLE `ant_Action`
(
    `id`           INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `module`       INT(8) UNSIGNED     NULL,
    `action`       VARCHAR(50)         NULL,
    `coverFile`    INT(8) UNSIGNED     NULL,
    `lastDateTime` DATETIME            NOT NULL,
    `user`         INT(8) UNSIGNED     NOT NULL,
    `active`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY (`module`, `action`),
    KEY (`module`),
    KEY (`action`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

CREATE TABLE `ant_Module`
(
    `id`              INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `name`            VARCHAR(50)         NULL,
    `label`           VARCHAR(50)         NULL,
    `url`             TEXT                NULL,
    `urlTarget`       VARCHAR(255)        NULL,
    `parentModule`    INT(8) UNSIGNED     NULL,
    `siblingOrder`    INT(4)              NULL,
    `icon`            VARCHAR(255)        NULL,
    `jsonDescription` TEXT                NULL,
    `coverFile`       INT(8) UNSIGNED     NULL,
    `lastDateTime`    DATETIME            NOT NULL,
    `user`            INT(8) UNSIGNED     NOT NULL,
    `active`          TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY (`name`),
    KEY (`parentModule`),
    KEY (`siblingOrder`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (1, '', '', '_WorkFrame.php', '_WorkFrame', '0', '0', '', '', NULL, '2016-12-22 17:57:21', 1, 1, 0);
INSERT INTO `ant_Module`
VALUES (2, 'System', 'Sistema', '', '', '1', '2', 'apps/package_system.png', '', NULL, '2016-12-22 17:57:21', 1, 1, 0);

INSERT INTO `ant_Module`
VALUES (3, 'Module', 'Moduli', 'Module.php', '_WorkFrame', 2, 2, 'apps/kalzium.png',
        '{\"id\":\"3\",\"name\":\"Module\",\"label\":\"Moduli\",\"icon\":\"apps\\/kalzium.png\",\"parentmodule\":2,\"siblingorder\":2,\"orderby\":[\"parentModule\",\"siblingOrder\"],\"fields\":{\"name\":{\"label\":\"Nome\",\"type\":\"string\",\"maxlen\":50,\"index\":\"unique\",\"mandatory\":false},\"label\":{\"label\":\"Label\",\"type\":\"string\",\"maxlen\":50,\"index\":\"none\"},\"url\":{\"label\":\"URL\",\"type\":\"text\",\"mandatory\":false},\"urlTarget\":{\"label\":\"URL target\",\"type\":\"string\",\"maxlen\":255,\"index\":\"none\"},\"parentModule\":{\"label\":\"Modulo padre\",\"type\":\"object\",\"mandatory\":false,\"typeoptions\":{\"name\":\"Module\",\"field\":\"name\"}},\"siblingOrder\":{\"label\":\"Ordine fratelli\",\"type\":\"integer\",\"maxlen\":4,\"mandatory\":false,\"default\":0},\"icon\":{\"label\":\"Icona\",\"type\":\"string\",\"maxlen\":255,\"index\":\"none\"},\"jsonDescription\":{\"label\":\"JSON Descrittivo\",\"type\":\"text\",\"mandatory\":false,\"index\":\"none\"}}}',
        NULL, '2017-01-19 11:34:17', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 3, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 3, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);

INSERT INTO `ant_Module`
VALUES (4, 'Action', 'Azioni', 'Action.php', '_WorkFrame', 2, 4, 'filesystems/exec.png',
        '{\"id\":\"4\",\"name\":\"Action\",\"label\":\"Azioni\",\"icon\":\"filesystems\\/exec.png\",\"parentmodule\":2,\"siblingorder\":4,\"orderby\":[\"module\",\"id\"],\"fields\":{\"module\":{\"label\":\"Modulo\",\"type\":\"object\",\"typeoptions\":{\"name\":\"Module\",\"field\":\"name\"}},\"action\":{\"label\":\"Azione\",\"type\":\"string\",\"maxlen\":50}}}',
        NULL, '2017-01-19 11:15:30', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 4, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 4, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);

CREATE TABLE `ant_Mask`
(
    `id`           INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `mask`         VARCHAR(50)         NULL,
    `coverFile`    INT(8) UNSIGNED     NULL,
    `lastDateTime` DATETIME            NOT NULL,
    `user`         INT(8) UNSIGNED     NOT NULL,
    `active`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY (`mask`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (5, 'Mask', 'Maschere', 'Mask.php', '_WorkFrame', 2, 6, 'apps/kdf.png',
        '{\"id\":\"5\",\"name\":\"Mask\",\"label\":\"Maschere\",\"icon\":\"apps\\/kdf.png\",\"parentmodule\":2,\"siblingorder\":6,\"orderby\":[\"mask\"],\"fields\":{\"mask\":{\"label\":\"Maschera\",\"type\":\"string\",\"index\":\"unique\",\"maxlen\":50}}}',
        NULL, '2017-01-19 11:41:58', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 5, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 5, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);

CREATE TABLE `ant_Authorization`
(
    `id`           INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `mask`         INT(8) UNSIGNED     NULL,
    `action`       INT(8) UNSIGNED     NULL,
    `coverFile`    INT(8) UNSIGNED     NULL,
    `lastDateTime` DATETIME            NOT NULL,
    `user`         INT(8) UNSIGNED     NOT NULL,
    `active`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY (`mask`, `action`),
    KEY (`mask`),
    KEY (`action`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (6, 'Authorization', 'Autorizzazioni', 'Authorization.php', '_WorkFrame', 2, 8, 'apps/kdisknav.png',
        '{\"id\":\"6\",\"name\":\"Authorization\",\"label\":\"Autorizzazioni\",\"icon\":\"apps\\/kdisknav.png\",\"parentmodule\":2,\"siblingorder\":8,\"orderby\":[\"mask\",\"action\"],\"fields\":{\"mask\":{\"label\":\"Maschera\",\"type\":\"object\",\"typeoptions\":{\"name\":\"Mask\",\"field\":\"mask\"}},\"action\":{\"label\":\"Azione\",\"type\":\"object\",\"typeoptions\":{\"name\":\"Action\",\"field\":\"action\"}}}}',
        NULL, '2017-01-19 11:42:48', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 6, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 6, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);

CREATE TABLE `ant_User`
(
    `id`                     INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `denomination`           VARCHAR(255)        NULL,
    `userName`               VARCHAR(50)         NULL,
    `userPassword`           VARCHAR(255)        NULL,
    `mask`                   INT(8) UNSIGNED     NULL,
    `email`                  VARCHAR(255)        NULL,
    `passwordLastUpdate`     DATETIME            NULL,
    `passwordExpirationDays` INT(4)              NULL,
    `coverFile`              INT(8) UNSIGNED     NULL,
    `lastDateTime`           DATETIME            NOT NULL,
    `user`                   INT(8) UNSIGNED     NOT NULL,
    `active`                 TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`                TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY (`denomination`),
    UNIQUE KEY (`userName`),
    KEY (`mask`),
    KEY (`email`),
    KEY (`passwordLastUpdate`),
    KEY (`passwordExpirationDays`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (7, 'User', 'Utenti', 'User.php', '_WorkFrame', 2, 10, 'apps/kgpg.png',
        '{\"id\":\"7\",\"name\":\"User\",\"label\":\"Utenti\",\"icon\":\"apps\\/kgpg.png\",\"parentmodule\":2,\"siblingorder\":10,\"orderby\":[\"denomination\"],\"fields\":{\"denomination\":{\"label\":\"Denominazione\",\"type\":\"string\",\"maxlen\":255},\"userName\":{\"label\":\"Utente\",\"type\":\"string\",\"maxlen\":50,\"index\":\"unique\"},\"userPassword\":{\"label\":\"Password\",\"type\":\"string\",\"maxlen\":255,\"index\":\"none\",\"intable\":false,\"inexcel\":false,\"inpdf\":false,\"infilter\":false},\"mask\":{\"label\":\"Maschera\",\"type\":\"object\",\"mandatory\":false,\"typeoptions\":{\"name\":\"Mask\",\"field\":\"mask\"}},\"email\":{\"label\":\"Email\",\"type\":\"string\",\"maxlen\":255},\"passwordLastUpdate\":{\"label\":\"Agg. password\",\"type\":\"datetime\",\"inform\":false},\"passwordExpirationDays\":{\"label\":\"GG scad. password\",\"type\":\"integer\",\"maxlen\":4,\"intable\":false,\"infilter\":false}}}',
        NULL, '2017-01-19 11:45:50', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 7, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 7, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);

INSERT INTO `ant_User`
VALUES (1, ' Admin', 'admin', 'd0b2551ed62cabf9968ef4fa9478e1b8eaa87f71b5501b697267d8a2edd360a2', NULL,
        'antilophpe@ncfsistemi.com', '2016-12-22 18:48:29', 9999, NULL, '2016-12-22 18:48:29', 1, 1, 0);
-- default admin password is "antilophpe"

CREATE TABLE IF NOT EXISTS `ant_File`
(
    `id`               int(8) unsigned     NOT NULL AUTO_INCREMENT,
    `objectName`       varchar(50) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `objectId`         int(8)                               DEFAULT NULL,
    `secretKey`        varchar(30) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `title`            varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `description`      text COLLATE utf8_unicode_ci,
    `originalFilename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `mimetype`         varchar(50) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `extension`        varchar(30) COLLATE utf8_unicode_ci  DEFAULT NULL,
    `sizeKb`           decimal(8, 2)                        DEFAULT NULL,
    `widthPx`          int(8)                               DEFAULT NULL,
    `heightPx`         int(8)                               DEFAULT NULL,
    `hasThumbnail`     tinyint(1) unsigned NOT NULL         DEFAULT '0',
    `hasResampled`     tinyint(1) unsigned NOT NULL         DEFAULT '0',
    `coverFile`        int(8) unsigned                      DEFAULT NULL,
    `lastDateTime`     datetime            NOT NULL,
    `user`             int(8) unsigned     NOT NULL,
    `active`           tinyint(1) unsigned NOT NULL         DEFAULT '1',
    `trashed`          tinyint(1) unsigned NOT NULL         DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `objectName` (`objectName`),
    KEY `objectId` (`objectId`),
    KEY `title` (`title`),
    KEY `originalFilename` (`originalFilename`),
    KEY `mimetype` (`mimetype`),
    KEY `extension` (`extension`),
    KEY `sizeKb` (`sizeKb`),
    KEY `widthPx` (`widthPx`),
    KEY `heightPx` (`heightPx`),
    KEY `hasThumbnail` (`hasThumbnail`),
    KEY `hasResampled` (`hasResampled`),
    KEY `coverFile` (`coverFile`),
    KEY `lastDateTime` (`lastDateTime`),
    KEY `user` (`user`),
    KEY `active` (`active`),
    KEY `trashed` (`trashed`),
    KEY `secretKey` (`secretKey`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (8, 'File', 'File allegati', 'File.php', '_WorkFrame', 2, 12, 'filesystems/folder_green.png',
        '{"id":"8","name":"File","label":"File allegati","icon":"filesystems\\/folder_green.png","parentmodule":2,"siblingorder":12,"orderby":["lastDateTime DESC"],"fields":{"objectName":{"label":"Oggetto - name","type":"string","maxlen":50},"objectId":{"label":"Oggetto - id","type":"integer","maxlen":8},"secretKey":{"label":"Secret key","type":"string","maxlen":30},"title":{"label":"Titolo","type":"string","maxlen":255},"description":{"label":"Descrizione","type":"text","mandatory":false},"originalFilename":{"label":"Filename originale","type":"string","maxlen":255},"mimetype":{"label":"Mimetype","type":"string","maxlen":50},"extension":{"label":"Estensione","type":"string","maxlen":30},"sizeKb":{"label":"Dimensione kB","type":"decimal","maxlen":8},"widthPx":{"grouplabel":"Dati immagine","label":"Larghezza px","type":"integer","maxlen":8,"mandatory":false},"heightPx":{"grouplabel":"Dati immagine","label":"Altezza px","type":"integer","maxlen":8,"mandatory":false},"hasThumbnail":{"grouplabel":"Dati immagine","label":"Esiste thumbnail","type":"flag","mandatory":false},"hasResampled":{"grouplabel":"Dati immagine","label":"Esiste resampled","type":"flag","mandatory":false}}}',
        NULL, '2017-01-19 11:48:58', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 8, 'VIEW', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'INSERT', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'UPDATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'ACTIVATE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'TRASH', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'FILESVIEW', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'FILESMANAGE', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'DUPLICATE', NULL, '2016-12-31 15:24:04', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'EXCEL', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'PDF', NULL, '2016-12-31 15:24:04', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 8, 'DELETE', NULL, '2016-12-31 15:24:04', 1, 1, 0);

CREATE TABLE `ant_Log`
(
    `id`           INT(8) UNSIGNED     NOT NULL AUTO_INCREMENT,
    `type`         VARCHAR(100)        NULL,
    `log`          TEXT                NULL,
    `ip`           VARCHAR(26)         NULL,
    `browser`      VARCHAR(255)        NULL,
    `coverFile`    INT(8) UNSIGNED     NULL,
    `lastDateTime` DATETIME            NOT NULL,
    `user`         INT(8) UNSIGNED     NULL,
    `active`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `trashed`      TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY (`type`),
    KEY (`ip`),
    KEY (`browser`),
    KEY (`coverFile`),
    KEY (`lastDateTime`),
    KEY (`user`),
    KEY (`active`),
    KEY (`trashed`)
) ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci
  AUTO_INCREMENT = 1;

INSERT INTO `ant_Module`
VALUES (9, 'Log', 'Log', 'Log.php', '_WorkFrame', 2, 14, 'actions/view_text.png',
        '{\"id\":\"9\",\"name\":\"Log\",\"label\":\"Log\",\"icon\":\"actions\\/view_text.png\",\"parentmodule\":2,\"siblingorder\":14,\"orderby\":[\"lastDateTime DESC\"],\"fields\":{\"type\":{\"label\":\"Tipo\",\"type\":\"string\",\"maxlen\":100},\"log\":{\"label\":\"Log\",\"type\":\"text\"},\"ip\":{\"label\":\"IP\",\"type\":\"string\",\"maxlen\":26},\"browser\":{\"label\":\"Browser\",\"type\":\"string\",\"maxlen\":255}}}',
        NULL, '2017-01-19 11:51:02', 1, 1, 0);

INSERT INTO `ant_Action`
VALUES (NULL, 9, 'VIEW', NULL, '2016-12-31 15:45:21', 1, 1, 0);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'INSERT', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'UPDATE', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'ACTIVATE', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'TRASH', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'FILESVIEW', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'FILESMANAGE', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'DUPLICATE', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'EXCEL', NULL, '2016-12-31 15:45:21', 1, 0, 1);
INSERT INTO `ant_Action`
VALUES (NULL, 9, 'PDF', NULL, '2016-12-31 15:45:21', 1, 0, 1);