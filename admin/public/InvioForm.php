<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invio Form</title>
    <link href="../lib/bootstrap/bootstrap.css" rel="stylesheet">
    <style>
    </style>

    <script src="../lib/bootstrap/jquery.min.js"></script>
    <script src="../lib/bootstrap/bootstrap.js"></script>

    <script>
        $(document).ready(function (e) {
            $("#fupForm").on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: 'InvioForm_operations.php',
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $('.submitBtn').attr("disabled", "disabled");
                        $('#fupForm').css("opacity", ".5");
                    },
                    success: function (response) {

                        $('.statusMsg').html('');
                        if (response.status == 1) {
                            $('#fupForm')[0].reset();
                            $('.statusMsg').html('<p class="alert alert-success">' + response.message + '</p>');
                        } else {
                            $('.statusMsg').html('<p class="alert alert-danger">' + response.message + '</p>');
                        }
                        // $('#fupForm').css("opacity", "");
                        // $(".submitBtn").removeAttr("disabled");
                    }
                });
            });

            $("#File__file").change(function () {
                var file = this.files[0];
                var imagefile = file.type;
                var match = ['image/jpeg', 'image/jpg'];
                if (!((imagefile == match[0]) || (imagefile == match[1]))) {
                    alert('Attenzione è possibile caricare solo file di tipo jpg o jpeg.');
                    $("#File__file").val('');
                    return false;
                }
            });
        });
    </script>

</head>
<body>
<h2><span style="color: #007558;" class="ugb-highlight">INVIACI LA TUA OPERA</span></h2>
<form id="fupForm" enctype="multipart/form-data">
    <input type="hidden" name="operation" value="save"/>
    <div class="form-group">
        <label for="InvioForm__name">Nome *</label>
        <input type="text" class="form-control" id="InvioForm__nome" name="InvioForm__nome" placeholder="Nome"
               required/>
    </div>
    <div class="form-group">
        <label for="InvioForm__cognome">Cognome *</label>
        <input type="text" class="form-control" id="InvioForm__cognome" name="InvioForm__cognome" placeholder="Cognome"
               required/>
    </div>
    <div class="form-group">
        <label for="InvioForm__email">Email *</label>
        <input type="email" class="form-control" id="InvioForm__email" name="InvioForm__email" placeholder="Email"
               required/>
    </div>
    <div class="form-group">
        <label for="InvioForm__telefono">Telefono</label>
        <input type="text" class="form-control" id="InvioForm__telefono" name="InvioForm__telefono"
               placeholder="Telefono"/>
    </div>
    <div class="form-group">
        <label for="InvioForm__dati[tecnica]">Tecnica *</label>
        <input type="text" class="form-control" id="InvioForm__dati[tecnica]" name="InvioForm__dati[tecnica]"
               placeholder="Tecnica (es: acquerello su carta, matita su cartoncino, ecc.)" required/>
    </div>
    <div class="form-group">
        <label for="InvioForm__dati[misure]">Misure *</label>
        <input type="text" class="form-control" id="InvioForm__dati[misure]" name="InvioForm__dati[misure]"
               placeholder="Misure (es: 50x50)" required/>
    </div>
    <div class="form-group">
        <label for="InvioForm__dati[titolo]">Titolo *</label>
        <input type="text" class="form-control" id="InvioForm__dati[titolo]" name="InvioForm__dati[titolo]"
               placeholder="Titolo" required/>
    </div>
    <div class="form-group">
        <label for="File__file">Immagine .jpg (dimensione massima 5MB)</label>
        <input type="file" class="form-control" style="height: auto;" id="File__file" name="File__file" required/>
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" required/> Autorizzo il trattamento dati personali e concedo il consenso per il trattamento dei miei dati personali per le finalità e con le modalità riportate nell'informativa seguente:
            <a style="color: hsl( 165, 100%, 33% );" href="https://www.iubenda.com/privacy-policy/91138832/legal" title="Privacy Policy" target="_blank">Privacy Policy</a>
        </label>
    </div>


    <div class="text-center">
        <input type="submit" name="submit" class="btn btn-success submitBtn" value="INVIA"/>
    </div>
</form>
<div style="margin-top:10px;" class="statusMsg"></div>
</body>

</html>