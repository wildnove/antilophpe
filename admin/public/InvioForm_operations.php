<?php
require_once("../config/config.inc.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/InvioForm.class.php");

session_start();
$_SESSION['User']['id'] = 1;
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 16;
$objectName = 'InvioForm';
$Object = new InvioForm($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);

try {
    switch ($_REQUEST['operation']) {

        case "save":
            if ($_FILES['File__file']['size'] > 5*1048576) {
                $response['status'] = 0;
                $response['message'] = 'Attenzione l\'immagine inviata file supera la dimensione massima consentita (5MB), salvataggio non eseguito.';
                echo json_encode($response);
                break;
            }

            $_REQUEST['InvioForm__dataInvio'] = date("Y-m-d H:i:s");
            $titolo = $_REQUEST['InvioForm__dati']['titolo'];
            $descrizione = 'Tecnica: '.$_REQUEST['InvioForm__dati']['tecnica'].' Misure: '.$_REQUEST['InvioForm__dati']['misure'];
            $_REQUEST['InvioForm__dati'] = json_encode($_REQUEST['InvioForm__dati']);

            $id = $ModuleHelper->insert($_REQUEST);

            $insertFile['File__objectName'] = 'InvioForm';
            $insertFile['File__objectId'] = $id;
            $insertFile['File__title'] = $titolo;
            $insertFile['File__description'] = $descrizione;
            $insertFile['File__objectId'] = $id;
            $insertFile['setObjectCoverFile'] = 1;
            $ModuleHelper->saveFile($insertFile, $_FILES);
//
            $response['status'] = 1;
            $response['message'] = 'Grazie, abbiamo ricevuto i tuoi dati!';

            echo json_encode($response);
            break;

        default:
            throw new Exception('authorizationDenied');
    }
} catch (Exception $e) {
}
exit;
