#!/usr/bin/php -q 
<?php
// exit(0);
function elapsedTime($secs) {
    $bit = array('y' => $secs / 31556926 % 12, 'w' => $secs / 604800 % 52, 'd' => $secs / 86400 % 7, 'h' => $secs / 3600 % 24, 'm' => $secs / 60 % 60, 's' => $secs % 60);

    foreach ($bit as $k => $v)
        if ($v > 0)
            $ret[] = $v . $k;

    return join(' ', $ret);
}

$startTime = time();
print date('Y-m-d H:i:s') . ' -> start' . "\n";

require __DIR__ . '/../config/config.inc.php';
require __DIR__ . '/../include/Database/DatabaseConnection.class.php';
require __DIR__ . '/../include/Object/NotificaEmail.class.php';

$debug = false;
if ($debug){
    $db_param['db_type'] = 'oracle';
    $db_param['db_host'] = 'ubuntuserver';
    $db_param['db_name'] = '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = ' . $db_param['db_host'] . ')(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME = XE) ))';
    $db_param['db_charset'] = 'we8iso8859p1';
}

$DatabaseConnection = new DatabaseConnection($db_param);
$resourceLinkID = $DatabaseConnection -> getResourceLinkId();

NotificaEmail::sendDevelopersMessage("TestCronJob eseguito (".date('Y-m-d H:i:s').")");

$endTime = time();
$elapsedTime = ($endTime - $startTime) != 0 ? elapsedTime($endTime - $startTime) : '0 secs';
print date('Y-m-d H:i:s', time()) . ' -> end, execution time ' . $elapsedTime . "\n";

?>