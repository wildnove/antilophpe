<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Mask.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 5;
$Object = new Mask($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->privateHtmlOpen();

$Mask = new Mask($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterMask']);
    unset($_SESSION['OrderMask']);
    unset($_SESSION['NavbarMask']);
}
// Default order
if (!$_SESSION['OrderMask']) {
    $_SESSION['OrderMask']['field1'] = TBPX . 'Mask.mask';
    $_SESSION['OrderMask']['field1_direction'] = '';
    $_SESSION['OrderMask']['field2'] = '';
    $_SESSION['OrderMask']['field2_direction'] = '';
    $_SESSION['OrderMask']['field3'] = '';
    $_SESSION['OrderMask']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarMask']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarMask']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedMask'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedMask'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedMask'] = 0;
    $_SESSION['RemoteSearchMask']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchMask']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchMask']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $Mask->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $Mask_array = $Mask->getAsArrayById($_REQUEST['Mask__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchMask']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$Mask_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $Mask->loadById($_REQUEST['Mask__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $Mask->loadById($_REQUEST['Mask__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $sourceMaskId = $_REQUEST['Mask__id'];
    $Mask->loadById($sourceMaskId);
    $Mask->setId('');
    $_REQUEST['Mask__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {

    $formOperation = $_REQUEST['operation'] == 'duplicateManual' ? 'duplicate' : 'save';

    echo '
<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$Mask->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="Mask.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="Mask_operations.php" method="post">
            <input type="hidden" name="operation" value="'.$formOperation.'"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
            <input type="hidden" value="' . $Mask->getId() . '" name="Mask__id" id="Mask__id"/>';
    }
    if ($_REQUEST['operation'] == 'duplicateManual') {
        echo '
            <input type="hidden" value="' . $sourceMaskId . '" name="sourceMaskId" id="sourceMaskId"/>';
    }
    echo '
            <div class="mandatory">
                <label for="Mask__mask">Maschera</label><br/>
                <input data-field="mask" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Mask->getMask()).'" name="Mask__mask" id="Mask__mask" maxlength="150" size="60"/>
            </div>';
    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="Mask.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $field['name'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina Maschere
        </div>';
        $fieldsArray= array(
                        'Maschera' => TBPX . 'Mask.mask',
                        'Id' => TBPX . 'Mask.id',
                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'Mask.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
                        'Attivo' => TBPX . 'Mask.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderMask__field'.$i.'" name="OrderMask__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderMask']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderMask']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderMask__field'.$i.'_direction" name="OrderMask__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Mask.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
        && ($_REQUEST['operation'] == 'manageFiles')
    ) {
        $ModuleHelper->echoFileBox($Mask, $_REQUEST['File__id']);
    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedMask']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterMask']) || isset($_SESSION['OrderMask']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Mask.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'Mask_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['PDF'])
      echo '
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\'Mask_operations.php?operation=pdf\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['TRASH']){
      if ($_SESSION['ViewTrashedMask']!=1){
        $trashCount = $Mask->getTrashCount();
        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
        echo '
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\'Mask.php?operation=viewTrashed\';return false;
          ">
            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
          </a>';
      }
      else
        echo '
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\'Mask.php?operation=viewNotTrashed\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>';
    }
    echo '
        </div>';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $Mask->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarMask']['bloccoAttuale'], $_SESSION['NavbarMask']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();

    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableMask">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableMask_header">';
    if ($_SESSION['ViewTrashedMask']!=1)
      echo '
            <th  width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap"><a href="?operation=order&amp;OrderMask__field1=' . TBPX . 'Mask.mask" title="Ordina per Maschera.">Maschera' . ($orderFields[TBPX . 'Mask.mask'] ? '&nbsp;'.($orderFields[TBPX . 'Mask.mask'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderMask__field1=' . TBPX . 'Mask.lastDateTime" title="Ordina per Data mod.">Data mod' . ($orderFields[TBPX . 'Mask.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'Mask.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][6]) // Authorization = 6
        echo '
            <th  width="1%" class="no_wrap">Contatori</th>';
    echo '
            <th width="1%"><a href="?operation=order&amp;OrderMask__field1=' . TBPX . 'Mask.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'Mask.active'] ? '&nbsp;'.($orderFields[TBPX . 'Mask.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
      echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['ViewTrashedMask']!=1){
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['DUPLICATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    else{
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $Mask->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while($Mask_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchMask']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$Mask_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedMask']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($Mask_array["id"]==$_REQUEST["Mask__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;Mask__id='.$Mask_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableMask_row" id="record'.$Mask_array['id'].'">';

      if ($_SESSION['ViewTrashedMask']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchMask']['objectName'].'__'.$_SESSION['RemoteSearchMask']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchMask']['objectName'].'__'.$_SESSION['RemoteSearchMask']['fieldName'].'\').value=\''.$Mask_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';
      echo '
            <td>'.$Mask_array['mask'].'</td>';
        $Mask_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($Mask_array['lastDateTime']));
      echo '
            <td class="no_wrap"><span class="font8">'.$Mask_array['lastDateTime'].'<br/>ID: '.$Mask_array['id'].' User: '.$Mask_array['user_userName'].'</span></td> ';
        if ($_SESSION['Mask'][6]){ // Authorization = 6
        echo '
        <td class="align_center">';
            $counterAuthorizations = $Mask->countAuthorizations($Mask_array['id']);
            if ($counterAuthorizations>0){
                    echo '
        <a title="Visualizza '.$counterAuthorizations.' autorizzazioni"  class="button"
        onclick="location.href=\'Authorization.php?operation=filterOrder&amp;FilterAuthorization__mask=\'+unescape(\''.str_replace('+',' ',urlencode(utf8_decode($Mask_array['mask']))).'\')+\'&amp;FilterAuthorization__mask_ALO=LIKE\';
              tree = parent.document.getElementById(\'_MenuFrame\').contentWindow.tree;
              idToPosition = parent.document.getElementById(\'_MenuFrame\').contentWindow.idToPosition;
              tree.s(idToPosition[\'6\']);">
          <img src="../img/icons/nuvola/16x16/apps/kdisknav.png" alt="ogg" width="16" height="16" />
          <span><b>'.$counterAuthorizations.'</b> autorizzazioni</span>
        </a>';
                }
        echo '    
            </td>';
        }
      echo '
            <td class="vertical_middle">';
      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
        if($Mask_array['active']==1)
          echo '
              <a href="#" title="disattiva" onclick="location.href=\'Mask_operations.php?operation=changeField&Mask__id='.$Mask_array['id'].'&Mask__active=0\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>';
        else
          echo '
              <a href="#" title="attiva" onclick="location.href=\'Mask_operations.php?operation=changeField&Mask__id='.$Mask_array['id'].'&Mask__active=1\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>';
      }
      else{
        if($Mask_array['active']==1)
          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
        else
          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
      }
        echo '
            </td>';

      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($Mask_array['id']);
        if ($filesCount > 0){
            $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
            $dataTitle='File allegati';
            $dataBody='Ci sono '.$filesCount.' file allegati.';
            if ($filesCount == 1){
                $dataTitle='File allegato';
                $dataBody='C\'&egrave; un file allegato.';
            }
        }
        else{
          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
          $dataTitle='Nessun allegato';
          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
        }
        echo '
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&Mask__id='.$Mask_array['id'].'\';return false;">
                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
                </a>';
        if ($Mask_array['coverFile']) {
            echo '
                <a href="_Download.php?File__id=' . $Mask_array['coverFile'] . '" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $Mask_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>';
        }
        echo '
              </td>';
      }

      if ($_SESSION['ViewTrashedMask']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;Mask__id='.$Mask_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

          if ($_SESSION['Mask'][$moduleId]['DUPLICATE'])
              echo '
                <td class="vertical_middle">
                  <a href="#" title="duplica" onclick="location.href=\'?operation=duplicateManual&amp;Mask__id='.$Mask_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/view_bottom.png" alt="duplica" width="16" height="16"/>
                  </a>
                </td>';


        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($Mask_array['id']) . '</td>';
      }
      else{
        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($Mask_array['id']) . '</td>';
      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#Mask__mask\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Maschera.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }
    
    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchMask']['objectName'].'__'.$_SESSION['RemoteSearchMask']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchMask']['objectName'].'__'.$_SESSION['RemoteSearchMask']['fieldName'].'\').value=\''.$_REQUEST['Mask__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}

if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';
echo '
});
</script>';


$Design->privateHtmlClose();

?>