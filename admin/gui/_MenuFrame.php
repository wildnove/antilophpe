<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);
$Design = new Design();
$Design->setBodyTag('
  <body id="menu_body" style="background-image:none;background-color:#eeeeee;">');
$Design->setDatabaseHelper($DatabaseHelper);
$Design->addCss('../lib/dtree/dtree.css');
$Design->addJs('../lib/dtree/dtree.js');
$Design->privateHtmlOpen();
$Design->menu($_SESSION['Mask']);
$Design->privateHtmlClose($frameset = 0, $noswitch = 1, $sublevels = '');
?>