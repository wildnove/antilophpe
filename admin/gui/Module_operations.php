<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Module.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 3;
$objectName = 'Module';
$Object = new Module($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);

try {
    if (!isset($_SESSION['Mask'][$moduleId]))
        throw new Exception('authorizationDenied');

    switch ($_REQUEST['operation']) {

        case "autocomplete":
            echo $ModuleHelper->autocomplete($_REQUEST['type'], $_REQUEST['field'], $_REQUEST['term']);
            break;


        case "echoField":
            echo $ModuleHelper->getField($_REQUEST['type'], $_REQUEST['field'], $_REQUEST['id']);
            break;


        case "echoImage":
            echo $ModuleHelper->getImageHtml($_REQUEST['type'], $_REQUEST['field'], $_REQUEST['id']);
            break;


        case "save":
            if ($_REQUEST[$objectName . '__parentModule'] == '')
                $_REQUEST[$objectName . '__parentModule'] = null;

            if ($_REQUEST[$objectName . '__id'] == '') {
                if (!$_SESSION['Mask'][$moduleId]['INSERT'] && !$_SESSION['Mask'][$moduleId]['DUPLICATE'])
                    throw new Exception('authorizationDenied');
                $id = $ModuleHelper->insert($_REQUEST);
            } else {
                if (!$_SESSION['Mask'][$moduleId]['UPDATE'])
                    throw new Exception('authorizationDenied');
                $id = $ModuleHelper->update($_REQUEST);
            }
            header("Location: " . $objectName . ".php?operation=updated&" . $objectName . "__id=" . $id);
            break;


        case "changeField":
            if (isset($_REQUEST[$objectName . '__trashed'])) {
                if (!$_SESSION['Mask'][$moduleId]['TRASH'])
                    throw new Exception('authorizationDenied');
                $arrayObject = array(
                    $objectName . '__id' => $_REQUEST[$objectName . '__id'],
                    $objectName . '__trashed' => $_REQUEST[$objectName . '__trashed'],
                );
            } else if (isset($_REQUEST[$objectName . '__active'])) {
                if (!$_SESSION['Mask'][$moduleId]['ACTIVATE'])
                    throw new Exception('authorizationDenied');
                $arrayObject = array(
                    $objectName . '__id' => $_REQUEST[$objectName . '__id'],
                    $objectName . '__active' => $_REQUEST[$objectName . '__active'],
                );
            } else {
                throw new Exception('invalidOperation');
            }
            $id = $ModuleHelper->update($arrayObject);
            header("Location: " . $objectName . ".php?operation=updated&" . $objectName . "__id=" . $id);
            break;


        case "excel":
            if (!$_SESSION['Mask'][$moduleId]['EXCEL'])
                throw new Exception('authorizationDenied');
            $ModuleHelper->outputExcel();
            break;


        case "pdf":
            if (!$_SESSION['Mask'][$moduleId]['PDF'])
                throw new Exception('authorizationDenied');
            $ModuleHelper->outputPdf();
            break;


        case "saveFile":
            if (!$_SESSION['Mask'][$moduleId]['FILESMANAGE'])
                throw new Exception('authorizationDenied');
            if ($_FILES['File__file']['size'] < UPLOADED_LIMIT_FILESIZE)
                $ModuleHelper->saveFile($_REQUEST, $_FILES);
            else
                $mex = "Attenzione impossibile salvare, il file supera la dimensione massima consentita.";
            header("Location: " . $objectName . ".php?operation=manageFiles&" . $objectName . "__id=" . $_REQUEST['File__objectId'] . ($mex ? '&mex=' . $mex : ''));
            break;


        case "deleteFile":
            if (!$_SESSION['Mask'][$moduleId]['FILESMANAGE'])
                throw new Exception('authorizationDenied');
            $ModuleHelper->deleteFile($_REQUEST['File__id'], $_REQUEST['isCoverFile']);
            header("Location: " . $objectName . ".php?operation=manageFiles&" . $objectName . "__id=" . $_REQUEST['File__objectId']);
            break;


        default:
            throw new Exception('authorizationDenied');
    }
} catch (Exception $e) {
    echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"it-IT\">
<head><title>Error!</title></head>
<body>
    <script>top.location.href='_LoginOperations.php?operation=logout&message=" . $e->getMessage() . "'</script>
</body>
</html>";
}
exit;

?>