<?php
require_once("../config/config.inc.php");
require_once("../core/Design.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../models/File.class.php");

// fix antilophpe
session_start();
$_SESSION = array();
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
session_destroy();

$Design = new Design();
$Design->setBodyTag('
  <body style="background-image:none;">');
$Design->privateHtmlOpen();
switch ($_REQUEST['operation']) {

    case "recuperaPassword":
        echo '
<div  class="display_none" id="RecuperaPasswordBox">
  <div class="thickboxCaption">
    <div class="float_left">
      Recupera password dimenticata
    </div>
      <div class="float_right padding2">
        <a href="#" title="Chiudi la finestra." onclick="$(\'#RecuperaPasswordBox\').dialog(\'close\');return false;" class="button">
          <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
          <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
        </a>
      </div>    
    <div class="clearer">&nbsp;</div>
  </div>
  <form  action="_LoginOperations.php" method="post" id="formRecuperaPassword" name="formRecuperaPassword">
    <input type="hidden" name="operation" value="recuperaPassword"/>
	<fieldset style="margin-left:0px;">
	<legend>Info</legend>
	<div class="padding5 align_justify">
	Per <b>recuperare la password</b> inserire: lo username e l\'indirizzo email usato in fase di registrazione.<br/>
	Il sistema generera\' una nuova password che avr&agrave; la validit&agrave; di un giorno, procedere immediatamente alla modifica.<br/>
	I dati vi verranno inviati all\'indirizzo email usato in fase di registrazione, per informazioni contattare l\'amministrazione.
		</div>
	</fieldset>
  <div>
    <label id="RecuperaPassword__userName_label" for="RecuperaPassword__userName">Nome utente</label><br/>
    <input class="input_text" type="text"  value="" name="RecuperaPassword__userName" id="RecuperaPassword__userName" maxlength="50" size="30"/>
  </div>
  <div>
    <label id="RecuperaPassword__email_label" for="RecuperaPassword__email">Email</label><br/>
    <input class="input_text colorblue" type="text"  value="" name="RecuperaPassword__email" id="RecuperaPassword__email" maxlength="50" size="30"/>
  </div>
    <div class="button_box">
      <a href="#" title="Recupera password." onclick="submitRecuperaPasswordForm(\'formRecuperaPassword\');" class="button">
        <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="Rec" width="16" height="16" />
        <span>Recupera</span>
      </a>
    </div>
  </form>
</div>';
        break;


    case "modificaPassword":
        echo '
<div  class="display_none" id="ModificaPasswordBox">
  <div class="thickboxCaption">
    <div class="float_left">
      Modifica password
    </div>    
      <div class="float_right padding2">
        <a href="#" title="Chiudi la finestra." onclick="$(\'#ModificaPasswordBox\').dialog(\'close\');return false;" class="button">
          <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
          <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
        </a>
      </div>
    <div class="clearer">&nbsp;</div>
  </div>
  <form  action="_LoginOperations.php" method="post" id="formModificaPassword" name="formModificaPassword">
    <input type="hidden" name="operation" value="modificaPassword"/>
	<fieldset style="margin-left:0px;">
	<legend>Info</legend>
	<div class="padding5 align_justify">
	Il "Codice in materia di protezione dei dati personali", con l\'allegato B al D. Lgs. n.196, impone che le password: siano segrete, debbano variare ogni 6 mesi, siano di lunghezza non inferiore agli 8 caratteri, contengano almeno un numero e una lettera maiuscola.<br/>
	<br/>
	Per <b>modificare la password</b> inserire: lo username, la password attuale, la nuova password e la ripetizione nuova password.
		</div>
	</fieldset>
  <div>
    <label id="ModificaPassword__userName_label" for="ModificaPassword__userName">Nome utente</label><br/>
    <input class="input_text" type="text"  value="" name="ModificaPassword__userName" id="ModificaPassword__userName" maxlength="50" size="30"/>
  </div>
  <div>
    <label id="ModificaPassword__userPassword_label" for="ModificaPassword__userPassword">Password attuale</label><br/>
    <input class="input_text" type="password"  value="" name="ModificaPassword__userPassword" id="ModificaPassword__userPassword" maxlength="20" size="30"/>
  </div>
  <div>
    <label id="ModificaPassword__userNewPassword_label" for="ModificaPassword__userNewPassword">Nuova password</label><br/>
    <input class="input_text" type="password"  value="" name="ModificaPassword__userNewPassword" id="ModificaPassword__userNewPassword" maxlength="20" size="30"/>
  </div>
  <div>
    <label id="ModificaPassword__userReNewPassword_label" for="ModificaPassword__userReNewPassword">Ripeti nuova password</label><br/>
    <input class="input_text" type="password"  value="" name="ModificaPassword__userReNewPassword" id="ModificaPassword__userReNewPassword" maxlength="20" size="30"/>
  </div>
    <div class="button_box">
      <a href="#" title="Modifica password." onclick="submitModificaPasswordForm(\'formModificaPassword\');" class="button">
        <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="Mod" width="16" height="16" />
        <span>Modifica</span>
      </a>
    </div>
  </form>
</div>';
        break;
}
echo '
    <br/><br/>
    <div id="login_div" class="form_div">
      <form action="_LoginOperations.php" method="post" name="form" id="form_Login" onkeypress="submitByEnter(\'form_Login\', event)">
        <input type="hidden" name="operation" value="login">
        <img src="../img/logo_big.png" title="Logo"/>
        <div>
            <label for="Login__userName">Utente</label><br/>
            <input class="input_text colorblue" type="text" name="Login__userName" id="Login__userName" maxlength="50" size="40" />
        </div>
        <div>
            <label id="Login__password_label" for="Login__userPassword">Password</label><br/>
            <input class="input_text colorblue" type="password" name="Login__userPassword" id="Login__userPassword" maxlength="50" size="40"/>
        </div>
        <br/>
	    <div class="align_center no_wrap">
	      <div style="float:left;">
              <a id="button_recuperaPassword" href="#" onclick="location.href=\'?operation=recuperaPassword\';return false;" title="Password dimenticata?" class="button">
                <img src="../img/icons/nuvola/16x16/actions/kgpg_import.png" alt="dim" width="16" height="16" />
                <span>Recupera</span>
              </a>
              <a id="button_modificaPassword" href="#" onclick="location.href=\'?operation=modificaPassword\';return false;" title="Modifica password" class="button">
                <img src="../img/icons/nuvola/16x16/actions/kgpg_sign.png" alt="mod" width="16" height="16" />
                <span>Modifica</span>
              </a>';

$DatabaseHelper = new DatabaseHelper($dbParams);
$File = new File($DatabaseHelper);
$File->loadUserManual();
if ($File->getId()) {
    echo '
              <a onclick="location.href=\'_Download.php?skipsession=1&File__id=' . $File->getId() . '\'" target="_blank" title="' . $File->getTitle() . '" class="button">
                <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
                <span>' . $File->getTitle() . '</span>
              </a>';
}
echo '
	      </div>
	      <div style="float:right;">
              <a href="#" onclick="submitLoginForm(\'form_Login\');" title="Accedi." class="button">
                <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
                <span>Accedi</span>
              </a>
	      </div>
	      <div class="clearer">&nbsp;</div>
	    </div>	        
	  </form>
    </div>
   <script>';

echo '
    $(\'#RecuperaPasswordBox\').dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        height: \'auto\',
        closeOnEscape: true,
        draggable: true,
        resizable: false,
        dialogClass: \'no-title-dialog\'
    });
    
    $(\'#ModificaPasswordBox\').dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        height: \'auto\',
        closeOnEscape: true,
        draggable: true,
        resizable: false,
        dialogClass: \'no-title-dialog\'
    });    

  $(document).ready(function(){';

if ($_REQUEST['operation'] == 'recuperaPassword') {
    echo '
    $(\'#RecuperaPasswordBox\').dialog(\'open\');';
}
if ($_REQUEST['operation'] == 'modificaPassword') {
    echo '
    $(\'#ModificaPasswordBox\').dialog(\'open\');';
}

if ($_REQUEST['message'] == 'loginError') {
    echo 'swal("Attenzione! Utente o password errati.");';
} else if ($_REQUEST['message'] == 'loginErrorNoMask') {
    echo 'swal("Attenzione! Nessuna maschera di accesso assegnata all\'utente, contattare l\'amministrazione.");';
} else if ($_REQUEST['message'] == 'authorizationDenied') {
    echo 'swal("Attenzione! Non si hanno le autorizzazioni necessarie per accedere al modulo selezionato.\nRieseguire il login.");';
} else if ($_REQUEST['message'] == 'timeout') {
    echo 'swal("Attenzione! Sessione scaduta per prolungata inattivita\'.\nRieseguire il login.");';
} else if ($_REQUEST['message'] == 'loginErrorPasswordExpired') {
    echo 'swal("Attenzione! La password e\' scaduta, procedere al rinnovo usando il bottone modifica.");';
} else if ($_REQUEST['message'] == 'invalidData') {
    echo 'swal("Attenzione! I dati inseriti non hanno corrispondenza nel nostro database, riprovare.");';
} else if ($_REQUEST['message'] == 'passwordSent') {
    echo 'swal("La nuova password e\' stata inviata all\' indirizzo di posta elettronica indicato.\n\nNella mail trovera\' tutti i dati per accedere al sistema.");';
} else if ($_REQUEST['message'] == 'passwordModified') {
    echo 'swal("Password modificata correttamente.");';
}

echo '
  });
  
	function validEmail(elementValue){
	  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	  return emailPattern.test(elementValue);
	}
	  
    
        function submitLoginForm(formId){
          if ($(\'#Login__userName\').val()==\'\')
            swal(\'Attenzione! Inserire uno username valido.\');
          else
            $(\'#\'+formId).submit();
          return false;
        }

        function submitByEnter(formId, e) {
          var keycode;
          if (window.event) keycode = window.event.keyCode;
          else if (e) keycode = e.which;
          else return true;

          if (keycode == 13){
            submitLoginForm(formId);
            return false;
          }
          else
            return true;
        }
		
        function submitRecuperaPasswordForm(formId){
          if ($(\'#RecuperaPassword__userName\').val()==\'\')
            swal(\'Attenzione! Inserire il proprio nome utente.\');
          else if (validEmail($(\'#RecuperaPassword__email\').val())==false){
            swal(\'Attenzione! Inserire la email utilizzata in fase di registrazione (nel formato xxxx@xxxx.xxx).\');
          }		            
          else
            $(\'#\'+formId).submit();
          return false;
        }
		
        function submitModificaPasswordForm(formId){
          if ($(\'#ModificaPassword__userName\').val()==\'\')
            swal(\'Attenzione! Inserire il proprio nome utente.\');
          else if ($(\'#ModificaPassword__userPassword\').val()==\'\')
            swal(\'Attenzione! Inserire la password attuale.\');		  
          else if ($(\'#ModificaPassword__userPassword\').val()==\'\')
            swal(\'Attenzione! Inserire la password attuale.\');		
          else if (!validPassword($(\'#ModificaPassword__userNewPassword\').val())){
            swal(\'Attenzione! La "Password" deve essere lunga almeno 8 caratteri, contenere almeno un numero e una lettera maiuscola.\');
            $(\'#ModificaPassword__userNewPassword\').val(\'\');
            $(\'#ModificaPassword__userReNewPassword\').val(\'\');
          }
          else if ($(\'#ModificaPassword__userNewPassword\').val()!=$(\'#ModificaPassword__userReNewPassword\').val()){
            swal(\'Attenzione! Il campo "Password" ed il campo "Ripeti Password" non coincidono.\');
            $(\'#ModificaPassword__userNewPassword\').val(\'\');
            $(\'#ModificaPassword__userReNewPassword\').val(\'\');
          }			    
          else
            $(\'#\'+formId).submit();
          return false;
        }



    function validPassword(password){
        if(password.length < 8)
            return false;
        if(!password.match(/[a-z]+/))
            return false;
        if(!password.match(/[0-9]+/))
            return false;    
        if(!password.match(/[A-Z]+/))
           return false;
        
        return true;
    }
    
 </script>';


$Design->privateHtmlClose($noswitch = 1);
?>