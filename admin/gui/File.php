<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Module.class.php");
require_once("../models/File.class.php");
require_once("../models/Action.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 8;
$Object = new File($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->addJs('../js/jquery.textareacrud-configurable.js');
$Design->privateHtmlOpen();

$File = new File($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
    $ModuleHelper->insertLog('filterOrder ant_File', json_encode($_REQUEST));
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterFile']);
    unset($_SESSION['OrderFile']);
    unset($_SESSION['NavbarFile']);
}
// Default order
if (!$_SESSION['OrderFile']) {
    $_SESSION['OrderFile']['field1'] = TBPX . 'File.lastDateTime';
    $_SESSION['OrderFile']['field1_direction'] = 'DESC';
    $_SESSION['OrderFile']['field2'] = '';
    $_SESSION['OrderFile']['field2_direction'] = '';
    $_SESSION['OrderFile']['field3'] = '';
    $_SESSION['OrderFile']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarFile']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarFile']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedFile'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedFile'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedFile'] = 0;
    $_SESSION['RemoteSearchFile']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchFile']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchFile']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $File->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $File_array = $File->getAsArrayById($_REQUEST['File__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchFile']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$File_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $File->loadById($_REQUEST['File__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $File->loadById($_REQUEST['File__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $File->loadById($_REQUEST['File__id']);
    $File->setId('');
    $_REQUEST['File__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {
    echo '

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$File->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="File.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form enctype="multipart/form-data" id="objectForm" name="objectForm" action="File_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>';
    $isCoverFile = false;
    if ($_REQUEST['operation'] == 'modify') {
        $isCoverFile = $File->isCoverFile();
        echo '
            <input type="hidden" name="File__id" id="File__id" value="' . $File->getId() . '" />';
    }
    echo '
        <div class="mandatory">
            <label for="File__objectName">Oggetto - nome</label><br/>
            <select name="File__objectName" id="File__objectName">
              <option value="">Seleziona...</option>';
    $Action = new Action($DatabaseHelper);
    $Modules_list = $Action->listModulesWithFilesManage();
    foreach ($Modules_list as $moduleWithFilesManage) {
        $selected = $moduleWithFilesManage['module_name'] == $File->getObjectName() ? 'selected="selected"' : '';
        echo '
              <option ' . $selected . ' value="' . htmlspecialchars($moduleWithFilesManage['module_name']) . '">' . htmlspecialchars($moduleWithFilesManage['module_name']) . "</option>\n";
    }
    echo '
            </select>
        </div>';
    echo '
        <div class="mandatory">
            <label for="File__objectId">Allegato a Oggetto - ID</label><br/>
            <input type="hidden" id="ForeignFile__objectId"/>
            <input class="input_text readonly" readonly="readonly" type="text" name="File__objectId" id="File__objectId" value="' . $File->getObjectId() . '" size="5"/>
            <a href="#" title="Cerca." onclick="
                if($(\'#File__objectName\').val())
                    window.open($(\'#File__objectName\').val()+\'.php?operation=remoteSearchCall&objectName=File&fieldName=objectId&foreignFieldsList=objectId\',\'WindowFile' . rand(0, 1000) . '\',\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\');
                else
                    swal(\'Selezionare prima un Oggetto nome\');
                return false;" class="button">
             <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
              <span>Cerca</span>
            </a>
            <a href="#" title="Pulisci campo." onclick="
              $(\'#File__objectId\').val(\'\');
              return false;"  class="button">
              <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
              <span>Pulisci</span>
            </a>
        </div>';

    echo '
            <div class="mandatory">
                <label for="File__title">Titolo</label>
                <br/>
                <input data-type="file" data-field="title" class="autocomplete input_text" type="text" name="File__title" id="File__title" maxlength="255" style="width:50%;" value="' . htmlspecialchars($File->getTitle()) . '"/>
                
                <input type="hidden" name="hasThumbnail" id="hasThumbnail" value="' . $File->getHasThumbnail() . '"/>
                <input type="hidden" name="isCoverFile" id="isCoverFile" value="' . ($isCoverFile ? 1 : 0) . '"/>                
                <input class="input_checkbox" ' . ($isCoverFile ? 'checked="checked"' : '') . ' type="checkbox" name="setObjectCoverFile" id="setObjectCoverFile" value="1"/>
                <label for="setObjectCoverFile" style="color:#333;">Usa come cover</label>                                                                
            </div>            
            <div ' . ($File->getId() ? '' : 'class="mandatory"') . '>
                <label for="File__file">Seleziona file' . ($File->getId() ? ' per sostituire l\'attuale' : '') . ' (dimensione massima ' . (UPLOADED_LIMIT_FILESIZE / 1048576) . 'MB)</label>
                <br/>
                <input class="input_text" type="file" name="File__file" id="File__file"/>                               
            </div>';
    echo '
            <div>
                <label for="File__description">Descrizione</label><br/>
                <textarea name="File__description" id="File__description" rows="3" cols="70">'.htmlspecialchars($File->getDescription()).'</textarea>
            </div>';
    if ($_REQUEST['operation'] == 'modify' && $File->getOriginalFilename()) {
        echo '
            <fieldset id="js-file-metatadata">
                <legend>Attributi file attuale (metadati automatici)</legend>';
        echo '
                <a href="#" onclick="location.href=\'_Download.php?File__id=' . $File->getId() . '\';return false;" class="vertical_middle" title="Download file originale">
                    '.$ModuleHelper->getImageHtml('file', 'thumbnail', $File->getId(), $File->getSecretKey()).'
                </a><br/>
                <span class="font9">Click sull\'immagine per scaricare il file originale.</span><br/>';

        $attrs = ($File->getOriginalFilename() ? '<label>Filename originale:</label> <span class="font12">'.$File->getOriginalFilename().'</span><br/>' : '').
            ($File->getExtension() ? '<label>Estensione:</label> <span class="font12">'.$File->getExtension().'</span><br/>' : '').
            ($File->getMimetype() ? '<label>Mimetype:</label> <span class="font12">'.$File->getMimetype().'</span><br/>' : '').
            ($File->getSizeKb() ? '<label>Dimensione kB:</label> <span class="font12">'.str_replace('.', ',', $File->getSizeKb()).'</span><br/>' : '').
            ($File->getWidthPx() ? '<label>Dimensioni px:</label> <span class="font12">'.$File->getWidthPx() . 'x' . $File->getHeightPx().'</span>' : '');
        echo '
                            <div>' . $attrs . '</div>';

        echo '  
            </fieldset>';
    }

    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="File.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            $field['name'] = $name;
            if ($field['name']=='coverFile' || $field['name']=='active')
                continue;
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina File allegati
        </div>';
        $fieldsArray= array(
                        'Oggetto - name' => TBPX . 'File.objectName',
                        'Oggetto - id' => TBPX . 'File.objectId',
                        'Titolo' => TBPX . 'File.title',
                        'Descrizione' => TBPX . 'File.description',
                        'Filename originale' => TBPX . 'File.originalFilename',
                        'Mimetype' => TBPX . 'File.mimetype',
                        'Estensione' => TBPX . 'File.extension',
                        'Dimensione kB' => TBPX . 'File.sizeKb',
                        'Larghezza px' => TBPX . 'File.widthPx',
                        'Altezza px' => TBPX . 'File.heightPx',
                        'Esiste thumbnail' => TBPX . 'File.hasThumbnail',
                        'Esiste resampled' => TBPX . 'File.hasResampled',
                        'Id' => TBPX . 'File.id',
//                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'File.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
//                        'Attivo' => TBPX . 'File.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderFile__field'.$i.'" name="OrderFile__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderFile']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderFile']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderFile__field'.$i.'_direction" name="OrderFile__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'File.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

//    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
//        && ($_REQUEST['operation'] == 'manageFiles')
//    ) {
//        $ModuleHelper->echoFileBox($File, $_REQUEST['File__id']);
//    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedFile']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterFile']) || isset($_SESSION['OrderFile']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'File.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'File_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

//    if ($_SESSION['Mask'][$moduleId]['PDF'])
//      echo '
//        <a href="#" title="Stampa in pdf." class="button" onclick="
//          location.href=\'File_operations.php?operation=pdf\';return false;
//        ">
//          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
//          <span>Pdf</span>
//        </a>';

//    if ($_SESSION['Mask'][$moduleId]['TRASH']){
//      if ($_SESSION['ViewTrashedFile']!=1){
//        $trashCount = $File->getTrashCount();
//        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
//        echo '
//          <a href="#" title="Visualizza cestinati." class="button" onclick="
//            location.href=\'File.php?operation=viewTrashed\';return false;
//          ">
//            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
//            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
//          </a>';
//      }
//      else
//        echo '
//          <a href="#" title="Torna al desktop." class="button" onclick="
//            location.href=\'File.php?operation=viewNotTrashed\';return false;
//          ">
//            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
//            <span>Desktop</span>
//          </a>';
//    }
    echo '
        </div>';

    echo $ModuleHelper->echoSelectOggetti();

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $File->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;

    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarFile']['bloccoAttuale'], $_SESSION['NavbarFile']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();
    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableFile">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableFile_header">';
    if ($_SESSION['ViewTrashedFile']!=1)
        echo '
            <th width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap">Thumb</th>            
            <th class="no_wrap"><a href="?operation=order&amp;OrderFile__field1=' . TBPX . 'File.title" title="Ordina per Titolo.">Titolo' . ($orderFields[TBPX . 'File.title'] ? '&nbsp;'.($orderFields[TBPX . 'File.title'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderFile__field1=' . TBPX . 'File.description" title="Ordina per Descrizione.">Descrizione' . ($orderFields[TBPX . 'File.description'] ? '&nbsp;'.($orderFields[TBPX . 'File.description'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
          <th class="no_wrap"><a href="?operation=order&amp;OrderFile__field1=' . TBPX . 'File.objectName" title="Ordina per Oggetto">Allegato a / Dati file' . ($orderFields[TBPX . 'File.objectName'] ? '&nbsp;'.($orderFields[TBPX . 'File.objectName'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>                        
            <th class="no_wrap"><a href="?operation=order&amp;OrderFile__field1=' . TBPX . 'File.lastDateTime" title="Ordina per Data agg.">Data agg' . ($orderFields[TBPX . 'File.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'File.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
//            <th width="1%"><a href="?operation=order&amp;OrderFile__field1=' . TBPX . 'File.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'File.active'] ? '&nbsp;'.($orderFields[TBPX . 'File.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
//    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
//      echo '
//            <th width="1%">&nbsp;</th>';
//    if ($_SESSION['ViewTrashedFile']!=1){

    echo '
            <th width="1%">&nbsp;</th>';

    echo '
            <th width="1%">&nbsp;</th>';

    if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['Mask'][$moduleId]['DELETE'])
        echo '
            <th width="1%">&nbsp;</th>';
//      if ($_SESSION['Mask'][$moduleId]['TRASH'])
//        echo '
//            <th width="1%">&nbsp;</th>';
//    }
//    else{
//      if ($_SESSION['Mask'][$moduleId]['TRASH'])
//        echo '
//            <th width="1%">&nbsp;</th>';
//    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $File->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while($File_array = $stmt->fetch(PDO::FETCH_ASSOC)){
        $isCoverFile = $File->isCoverFile($File_array['objectName'], $File_array['objectId'], $File_array['id']);

      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchFile']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$File_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedFile']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($File_array["id"]==$_REQUEST["File__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;File__id='.$File_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableFile_row" id="record'.$File_array['id'].'">';

      if ($_SESSION['ViewTrashedFile']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchFile']['objectName'].'__'.$_SESSION['RemoteSearchFile']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchFile']['objectName'].'__'.$_SESSION['RemoteSearchFile']['fieldName'].'\').value=\''.$File_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';

        echo '
            <td class="align_center">';
        if ($File_array['hasThumbnail'])
            echo $ModuleHelper->getImageHtml('file', 'thumbnail', $File_array['id'], $File_array['secretKey']);
        else
            echo 'Thumbnail non presente.';
        echo '
            </td>';
        echo ' 
            <td>'.$File_array['title'].'</td>
            <td>';
      $File_array['description'] = htmlspecialchars(trim($File_array['description']));
      if ($File_array['description']!=''){
        $maxlen = 35;
        if (strlen($File_array['description'])>$maxlen){
            echo '
              <div class="tooltip-ajax help" data-field="description" data-id="'.$File_array['id'].'" data-title="Descrizione">
                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                <span>'.mb_strimwidth($File_array['description'], 0, $maxlen, "...").'</span>
              </div>';
        } else
            echo $File_array['description'];
      }
      else
        echo '&nbsp;';
      echo '
            </td>';
        $ObjectData = '<div class="no_wrap"><b>'.$File_array['objectName'].'</b> <i>ID:</i> <b>'.$File_array['objectId'].'</b></div>
            <div class="no_wrap">'.($File_array['originalFilename'] ? '<i>File orig:</i> '.$File_array['originalFilename'] : '').'</div>
            <div class="no_wrap">'.($File_array['mimetype'] ? '<i>Mimetype:</i> '.$File_array['mimetype'] : '').'</div>
            <div class="no_wrap">'.($File_array['extension'] ? '<i>Estensione:</i> '.$File_array['extension'].' -- <i>Peso:</i> '.number_format($File_array['sizeKb'],2,',','.').'Kb' : '').'</div>
            <div class="no_wrap">'.($File_array['widthPx'] ? '<i>Larg:</i> '.$File_array['widthPx'].'px'.' -- <i>Alt:</i> '.$File_array['heightPx'].'px' : '');

        echo '
            <td>'.$ObjectData.'</td>';
        $File_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($File_array['lastDateTime']));
        echo '
            <td class="no_wrap"><span class="font8">'.$File_array['lastDateTime'].'<br/>ID: '.$File_array['id'].' User: '.$File_array['user_userName'].'</span></td> ';

        echo '
        <td class="align_center">';
        $Module = new Module($DatabaseHelper);
        $Module__id = $Module->getIdByName($File_array['objectName']);

        if ($_SESSION['Mask'][$Module__id]){
            echo '
              <span class="vertical_middle link" title="Visualizza scheda / modulo collegato" 
              onclick="location.href=\''.$File_array['objectName'].'.php?operation=filterOrder&amp;Filter'.$File_array['objectName'].'__id='.$File_array['objectId'].'&amp;Filter'.$File_array['objectName'].'__id_ALO=LIKE\';
              tree = parent.document.getElementById(\'_MenuFrame\').contentWindow.tree;
              idToPosition = parent.document.getElementById(\'_MenuFrame\').contentWindow.idToPosition;
              tree.s(idToPosition[\''.$Module__id.'\']);">
                <img src="../img/icons/nuvola/16x16/actions/editpaste.png" alt="ogg"/>
              </span>';
        }
        echo '    
            </td>';
        echo '    
            <td>';
        if ($File_array['originalFilename'])
            echo '
                <a href="#" onclick="location.href=\'_Download.php?File__id=' . $File_array['id'] . '\'" class="vertical_middle" title="Download file originale">
                    <img src="../img/icons/nuvola/16x16/actions/revert.png" alt="download"/>
                </a>';
        else
            echo '&nbsp;';
        echo '
            </td> ';


//         echo '
//            <td class="vertical_middle">';
//      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
//        if($File_array['active']==1)
//          echo '
//              <a href="#" title="disattiva" onclick="location.href=\'File_operations.php?operation=changeField&File__id='.$File_array['id'].'&File__active=0\';return false;">
//                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
//              </a>';
//        else
//          echo '
//              <a href="#" title="attiva" onclick="location.href=\'File_operations.php?operation=changeField&File__id='.$File_array['id'].'&File__active=1\';return false;">
//                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
//              </a>';
//      }
//      else{
//        if($File_array['active']==1)
//          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
//        else
//          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
//      }
//        echo '
//            </td>';

//      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
//        $filesCount = $ModuleHelper->getFilesCountByObjectId($File_array['id']);
//        if ($filesCount > 0){
//          $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
//          $dataTitle='File allegati';
//          $dataBody='Ci sono '.$filesCount.' file allegati.';
//        }
//        else{
//          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
//          $dataTitle='Nessun allegato';
//          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
//        }
//        echo '
//              <td class="no_wrap vertical_middle">
//                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&File__id='.$File_array['id'].'\';return false;">
//                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
//                </a>';
//        if ($File_array['coverFile']) {
//            echo '
//                <img class="tooltip-ajax help" data-type="file" data-field="thumbnail" data-id="' . $File_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/>';
//        }
//        echo '
//              </td>';
//      }

//      if ($_SESSION['ViewTrashedFile']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;File__id='.$File_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

        if ($_SESSION['Mask'][$moduleId]['DELETE'])
            echo '
                <td class="vertical_middle">
                  <a href="#" title="cancella" onclick="swal({title: \'Confermi la cancellazione del file?\', text: \'I file cancellati verranno rimossi fisicamente dal sistema e non saranno recuperabili.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                        function(){
                            location.href=\'File_operations.php?operation=delete&File__objectId=' . $File_array['objectId'] . '&File__id=' . $File_array['id'] . ($isCoverFile ? '&isCoverFile=1' : '') . '\';
                        }); return false;">
                    <img src="../img/icons/nuvola/16x16/actions/cancel.png" alt="can" width="16" height="16"/>
                  </a>
                </td>';


//
//        if ($_SESSION['Mask'][$moduleId]['TRASH'])
//            echo '
//              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($File_array['id']) . '</td>';
//      }
//      else{
//        if ($_SESSION['Mask'][$moduleId]['TRASH'])
//            echo '
//              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($File_array['id']) . '</td>';
//      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#File__objectName\').val()==\'\'){
	$(\'#File__objectName\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Oggetto - name.\');
    }
    else if ($(\'#File__objectId\').val()==\'\'){
	$(\'#File__objectId\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Oggetto - id.\');
    }
    else if ($(\'#File__title\').val()==\'\'){
	$(\'#File__title\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Titolo.\');
    }
    else if (!$(\'#File__id\').length && $(\'#File__file\').get(0).files.length === 0){
	$(\'#File__file\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: File.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }
    
    if ($("#js-file-metatadata").length){   
        $("#File__file").change(function (){
            if ($(this).val())
                $("#js-file-metatadata").hide();
            else
                $("#js-file-metatadata").show();            
        });
    }

    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchFile']['objectName'].'__'.$_SESSION['RemoteSearchFile']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchFile']['objectName'].'__'.$_SESSION['RemoteSearchFile']['fieldName'].'\').value=\''.$_REQUEST['File__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}

if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';

echo '
});
</script>';


$Design->privateHtmlClose();

?>
