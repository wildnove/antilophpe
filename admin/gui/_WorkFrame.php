<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);
$Design = new Design();
$Design->setDatabaseHelper($DatabaseHelper);
$Design->privateHtmlOpen();


$back = 'back_workframe.jpg';

echo '
<style>
html { 
  background: url("../img/' . $back . '") no-repeat center center fixed;   
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>

<br/>
<div class="workframe">
  <div style="padding-top: 160px; padding-left: 100px; padding-right: 30px;">
    <div class="align_left" style="font-color:#001343;font-size:12px;width:250px;">
      <span style="font-size:14px;">Benvenuto</span> <span class="bold colororange" style="font-size:14px;">' . ($_SESSION['User']['userName'] == 'admin' ? '<span class="colorred">' . $_SESSION['User']['denomination'] . '</span>' : $_SESSION['User']['denomination']) . '</span><br/>           
      <br/>
      <span>Maschera:</span> <span class="bold">' . ($_SESSION['User']['userName'] == 'admin' ? '<span class="colorred">superuser</span>' : $_SESSION['User']['mask_mask']) . '</span><br/>
      <span>Login data:</span> <span class="bold">' . date("d/m/Y", strtotime($_SESSION['User']['loginTimeStamp'])) . '</span><br/>
      <span>Login ora:</span> <span class="bold">' . date("H:i:s", strtotime($_SESSION['User']['loginTimeStamp'])) . '</span><br/>
      <span>Login IP:</span> <span class="bold">' . $_SESSION['User']['ip'] . '</span>
    </div>
  </div>';

echo '
</div>';
$Design->privateHtmlClose();
?>
