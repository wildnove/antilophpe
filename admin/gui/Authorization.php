<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Authorization.class.php");
require_once("../models/Action.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 6;
$Object = new Authorization($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->privateHtmlOpen();

$Authorization = new Authorization($DatabaseHelper);
$Action = new Action($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterAuthorization']);
    unset($_SESSION['OrderAuthorization']);
    unset($_SESSION['NavbarAuthorization']);
}
// Default order
if (!$_SESSION['OrderAuthorization']) {
    $_SESSION['OrderAuthorization']['field1'] = TBPX . 'Mask_mask.mask';
    $_SESSION['OrderAuthorization']['field1_direction'] = '';
    $_SESSION['OrderAuthorization']['field2'] = TBPX . 'Module_module.label';
    $_SESSION['OrderAuthorization']['field2_direction'] = '';
    $_SESSION['OrderAuthorization']['field3'] = TBPX . 'Action_action.action';
    $_SESSION['OrderAuthorization']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarAuthorization']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarAuthorization']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedAuthorization'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedAuthorization'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedAuthorization'] = 0;
    $_SESSION['RemoteSearchAuthorization']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchAuthorization']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchAuthorization']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $Authorization->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $Authorization_array = $Authorization->getAsArrayById($_REQUEST['Authorization__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchAuthorization']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$Authorization_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $Authorization->loadById($_REQUEST['Authorization__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $Authorization->loadById($_REQUEST['Authorization__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $Authorization->loadById($_REQUEST['Authorization__id']);
    $Authorization->setId('');
    $_REQUEST['Authorization__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {
    echo '

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$Authorization->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="Authorization.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="Authorization_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
            <input type="hidden" value="' . $Authorization->getId() . '" name="Authorization__id" id="Authorization__id"/>';
    }
    if ($Authorization->getMask() == 0)
        $Authorization->setMask('');
    echo '
            <div class="mandatory">
                <label for="Authorization__mask">Maschera</label><br/>
                <input data-foreignobject="Mask" data-foreignfield="mask" data-field="mask" class="autocomplete-foreign input_text" type="text" value="' . $Authorization->getMask_mask().'" name="ForeignAuthorization__mask" id="ForeignAuthorization__mask" size="50"/>
                &nbsp;&nbsp;
                <input class="input_text readonly" readonly="readonly" type="text" name="Authorization__mask" id="Authorization__mask" value="'.$Authorization->getMask().'" size="5"/>
                <a href="#" title="Cerca." onclick="window.open(\'Mask.php?operation=remoteSearchCall&amp;objectName=Authorization&amp;fieldName=mask&amp;foreignFieldsList=mask\',\'WindowMask'.rand(0,1000).'\',\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
                  <span>Cerca</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="
                  $(\'#ForeignAuthorization__mask\').val(\'\');
                  $(\'#Authorization__mask\').val(\'\');
                  return false;"  class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>';
    if ($Authorization->getAction() == 0)
        $Authorization->setAction('');
    echo '
            <div>
                <label for="Authorization__action">Azione</label><br/>
                <input class="input_text readonly" readonly="readonly" type="text" value="' . $Authorization->getAction_action().'" name="ForeignAuthorization__action" id="ForeignAuthorization__action" size="50"/>
                &nbsp;&nbsp;
                <input class="input_text readonly" readonly="readonly" type="text" name="Authorization__action" id="Authorization__action" value="'.$Authorization->getAction().'" size="5"/>
                <a href="#" title="Cerca." onclick="window.open(\'Action.php?operation=remoteSearchCall&amp;objectName=Authorization&amp;fieldName=action&amp;foreignFieldsList=action\',\'WindowAction'.rand(0,1000).'\',\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
                  <span>Cerca</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="
                  $(\'#ForeignAuthorization__action\').val(\'\');
                  $(\'#Authorization__action\').val(\'\');
                  return false;"  class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>';


    if ($_REQUEST['operation'] != 'modify') {
        echo '
            <div class="thickboxCaption">
                <div class="no_wrap">
                    <br/>oppure aggiungi tutte le autorizzazioni di un modulo
                </div>
            </div>';

        echo '
            <div>
                <label for="Authorization__module">Modulo</label><br/>
                <select name="Authorization__module" id="Authorization__module">
                  <option value="">Seleziona...</option>';
        foreach($Action->selectModulesWithActions() as $module){
            echo '
                  <option value="'.htmlspecialchars($module['module']).'">' . htmlspecialchars($module['module_label']) . "</option>\n";
        }
        echo '
                </select>
            </div>';
    }

    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="Authorization.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $field['name'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina Autorizzazioni
        </div>';
        $fieldsArray= array(
                        'Maschera' => TBPX . 'Mask_mask.mask',
                        'Azione' => TBPX . 'Action_action.action',
                        'Id' => TBPX . 'Authorization.id',
                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'Authorization.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
                        'Attivo' => TBPX . 'Authorization.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderAuthorization__field'.$i.'" name="OrderAuthorization__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderAuthorization']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderAuthorization']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderAuthorization__field'.$i.'_direction" name="OrderAuthorization__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Authorization.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
        && ($_REQUEST['operation'] == 'manageFiles')
    ) {
        $ModuleHelper->echoFileBox($Authorization, $_REQUEST['File__id']);
    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedAuthorization']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterAuthorization']) || isset($_SESSION['OrderAuthorization']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Authorization.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'Authorization_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['PDF'])
      echo '
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\'Authorization_operations.php?operation=pdf\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['TRASH']){
      if ($_SESSION['ViewTrashedAuthorization']!=1){
        $trashCount = $Authorization->getTrashCount();
        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
        echo '
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\'Authorization.php?operation=viewTrashed\';return false;
          ">
            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
          </a>';
      }
      else
        echo '
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\'Authorization.php?operation=viewNotTrashed\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>';
    }
    echo '
        </div>';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $Authorization->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarAuthorization']['bloccoAttuale'], $_SESSION['NavbarAuthorization']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();

    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    $masks = $Authorization->selectMasksWithAuthorizations();
    if ($masks) {
        echo '
        <div class="align_center">
            <span>Maschera:</span>
            <select onchange="
            if ($(this).val()==\'\')
                location.href=\'?operation=filterOrder&FilterAuthorization__mask_ALO=&FilterAuthorization__mask=\';
            else
                location.href=\'?operation=filterOrder&FilterAuthorization__mask_ALO=LIKE&FilterAuthorization__mask=\'+$(this).val();
            return false;">
                <option value="">Seleziona...</option>';
        foreach ($masks as $mask) {
            $selected = ($mask['mask_mask'] == $_SESSION['FilterAuthorization']['ant_Mask_mask.mask'] && $_SESSION['FilterAuthorization']['ant_Mask_mask.mask_ALO'] == 'LIKE') ? 'selected="selected"' : '';
            echo '
                <option ' . $selected . ' value="' . htmlspecialchars($mask['mask_mask']) . '">' . htmlspecialchars($mask['mask_mask']) . "</option>\n";
        }
        echo '
            </select>    
        </div>';
    }

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableAuthorization">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableAuthorization_header">';
    if ($_SESSION['ViewTrashedAuthorization']!=1)
      echo '
            <th  width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap"><a href="?operation=order&amp;OrderAuthorization__field1=' . TBPX . 'Mask_mask.mask" title="Ordina per Maschera.">Maschera' . ($orderFields[TBPX . 'Mask_mask.mask'] ? '&nbsp;'.($orderFields[TBPX . 'Mask_mask.mask'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderAuthorization__field1=' . TBPX . 'Module_module.label" title="Ordina per Modulo.">Modulo' . ($orderFields[TBPX . 'Module_module.label'] ? '&nbsp;' . ($orderFields[TBPX . 'Module_module.label'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderAuthorization__field1=' . TBPX . 'Action_action.action" title="Ordina per Azione.">Azione' . ($orderFields[TBPX . 'Action_action.action'] ? '&nbsp;'.($orderFields[TBPX . 'Action_action.action'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderAuthorization__field1=' . TBPX . 'Authorization.lastDateTime" title="Ordina per Data mod.">Data mod' . ($orderFields[TBPX . 'Authorization.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'Authorization.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th width="1%"><a href="?operation=order&amp;OrderAuthorization__field1=' . TBPX . 'Authorization.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'Authorization.active'] ? '&nbsp;'.($orderFields[TBPX . 'Authorization.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
      echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['ViewTrashedAuthorization']!=1){
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    else{
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $Authorization->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while($Authorization_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchAuthorization']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$Authorization_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedAuthorization']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($Authorization_array["id"]==$_REQUEST["Authorization__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;Authorization__id='.$Authorization_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableAuthorization_row" id="record'.$Authorization_array['id'].'">';

      if ($_SESSION['ViewTrashedAuthorization']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchAuthorization']['objectName'].'__'.$_SESSION['RemoteSearchAuthorization']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchAuthorization']['objectName'].'__'.$_SESSION['RemoteSearchAuthorization']['fieldName'].'\').value=\''.$Authorization_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';
      echo '
            <td>'.$Authorization_array['mask_mask'].'</td>
            <td>' . $Authorization_array['module_label'] . '</td>
            <td>'.$Authorization_array['action_action'].'</td>';
        $Authorization_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($Authorization_array['lastDateTime']));
      echo '
            <td class="no_wrap"><span class="font8">'.$Authorization_array['lastDateTime'].'<br/>ID: '.$Authorization_array['id'].' User: '.$Authorization_array['user_userName'].'</span></td> ';
         echo '
            <td class="vertical_middle">';
      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
        if($Authorization_array['active']==1)
          echo '
              <a href="#" title="disattiva" onclick="location.href=\'Authorization_operations.php?operation=changeField&Authorization__id='.$Authorization_array['id'].'&Authorization__active=0\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>';
        else
          echo '
              <a href="#" title="attiva" onclick="location.href=\'Authorization_operations.php?operation=changeField&Authorization__id='.$Authorization_array['id'].'&Authorization__active=1\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>';
      }
      else{
        if($Authorization_array['active']==1)
          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
        else
          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
      }
        echo '
            </td>';

      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($Authorization_array['id']);
        if ($filesCount > 0){
            $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
            $dataTitle='File allegati';
            $dataBody='Ci sono '.$filesCount.' file allegati.';
            if ($filesCount == 1){
                $dataTitle='File allegato';
                $dataBody='C\'&egrave; un file allegato.';
            }
        }
        else{
          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
          $dataTitle='Nessun allegato';
          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
        }
        echo '
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&Authorization__id='.$Authorization_array['id'].'\';return false;">
                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
                </a>';
        if ($Authorization_array['coverFile']) {
            echo '
                <a href="_Download.php?File__id=' . $Authorization_array['coverFile'] . '" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $Authorization_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>';
        }
        echo '
              </td>';
      }

      if ($_SESSION['ViewTrashedAuthorization']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;Authorization__id='.$Authorization_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($Authorization_array['id']) . '</td>';
      }
      else{
        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($Authorization_array['id']) . '</td>';
      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#Authorization__mask\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Maschera.\');
    }
    else if ($(\'#Authorization__action\').val()==\'\' && $(\'#Authorization__module\').val()==\'\'){
            swal(\'Attenzione! Compilare almeno uno tra i campi: Azione e Modulo.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }
    
    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchAuthorization']['objectName'].'__'.$_SESSION['RemoteSearchAuthorization']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchAuthorization']['objectName'].'__'.$_SESSION['RemoteSearchAuthorization']['fieldName'].'\').value=\''.$_REQUEST['Authorization__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}

if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';
echo '
});
</script>';


$Design->privateHtmlClose();

?>