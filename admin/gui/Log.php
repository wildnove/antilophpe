<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Log.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 9;
$Object = new Log($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->privateHtmlOpen();

$Log = new Log($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterLog']);
    unset($_SESSION['OrderLog']);
    unset($_SESSION['NavbarLog']);
}
// Default order
if (!$_SESSION['OrderLog']) {
    $_SESSION['OrderLog']['field1'] = TBPX . 'Log.lastDateTime';
    $_SESSION['OrderLog']['field1_direction'] = 'DESC';
    $_SESSION['OrderLog']['field2'] = '';
    $_SESSION['OrderLog']['field2_direction'] = '';
    $_SESSION['OrderLog']['field3'] = '';
    $_SESSION['OrderLog']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarLog']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarLog']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedLog'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedLog'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedLog'] = 0;
    $_SESSION['RemoteSearchLog']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchLog']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchLog']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $Log->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $Log_array = $Log->getAsArrayById($_REQUEST['Log__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchLog']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$Log_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $Log->loadById($_REQUEST['Log__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $Log->loadById($_REQUEST['Log__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $Log->loadById($_REQUEST['Log__id']);
    $Log->setId('');
    $_REQUEST['Log__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {
    echo '

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$Log->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="Log.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="Log_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
            <input type="hidden" value="' . $Log->getId() . '" name="Log__id" id="Log__id"/>';
    }
    echo '
            <div class="mandatory">
                <label for="Log__type">Tipo</label><br/>
                <input data-field="type" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Log->getType()).'" name="Log__type" id="Log__type" maxlength="100" size="50"/>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Log__log">Log</label><br/>
                <textarea name="Log__log" id="Log__log" rows="3" cols="70">'.htmlspecialchars($Log->getLog()).'</textarea>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Log__ip">IP</label><br/>
                <input data-field="ip" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Log->getIp()).'" name="Log__ip" id="Log__ip" maxlength="26" size="36"/>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Log__browser">Browser</label><br/>
                <input data-field="browser" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Log->getBrowser()).'" name="Log__browser" id="Log__browser" maxlength="255" size="50"/>
            </div>';
    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="Log.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $field['name'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina Log
        </div>';
        $fieldsArray= array(
                        'Tipo' => TBPX . 'Log.type',
                        'Log' => TBPX . 'Log.log',
                        'IP' => TBPX . 'Log.ip',
                        'Browser' => TBPX . 'Log.browser',
                        'Id' => TBPX . 'Log.id',
                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'Log.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
                        'Attivo' => TBPX . 'Log.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderLog__field'.$i.'" name="OrderLog__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderLog']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderLog']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderLog__field'.$i.'_direction" name="OrderLog__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Log.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
        && ($_REQUEST['operation'] == 'manageFiles')
    ) {
        $ModuleHelper->echoFileBox($Log, $_REQUEST['File__id']);
    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedLog']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterLog']) || isset($_SESSION['OrderLog']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Log.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'Log_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['PDF'])
      echo '
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\'Log_operations.php?operation=pdf\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['TRASH']){
      if ($_SESSION['ViewTrashedLog']!=1){
        $trashCount = $Log->getTrashCount();
        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
        echo '
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\'Log.php?operation=viewTrashed\';return false;
          ">
            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
          </a>';
      }
      else
        echo '
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\'Log.php?operation=viewNotTrashed\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>';
    }
    echo '
        </div>';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $Log->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarLog']['bloccoAttuale'], $_SESSION['NavbarLog']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();
    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableLog">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableLog_header">';
    if ($_SESSION['ViewTrashedLog']!=1)
      echo '
            <th  width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.type" title="Ordina per Tipo.">Tipo' . ($orderFields[TBPX . 'Log.type'] ? '&nbsp;'.($orderFields[TBPX . 'Log.type'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.log" title="Ordina per Log.">Log' . ($orderFields[TBPX . 'Log.log'] ? '&nbsp;'.($orderFields[TBPX . 'Log.log'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.ip" title="Ordina per IP.">IP' . ($orderFields[TBPX . 'Log.ip'] ? '&nbsp;'.($orderFields[TBPX . 'Log.ip'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.browser" title="Ordina per Browser.">Browser' . ($orderFields[TBPX . 'Log.browser'] ? '&nbsp;'.($orderFields[TBPX . 'Log.browser'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.lastDateTime" title="Ordina per Data mod.">Data mod' . ($orderFields[TBPX . 'Log.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'Log.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th width="1%"><a href="?operation=order&amp;OrderLog__field1=' . TBPX . 'Log.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'Log.active'] ? '&nbsp;'.($orderFields[TBPX . 'Log.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
      echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['ViewTrashedLog']!=1){
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    else{
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $Log->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while($Log_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchLog']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$Log_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedLog']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($Log_array["id"]==$_REQUEST["Log__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;Log__id='.$Log_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableLog_row" id="record'.$Log_array['id'].'">';

      if ($_SESSION['ViewTrashedLog']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchLog']['objectName'].'__'.$_SESSION['RemoteSearchLog']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchLog']['objectName'].'__'.$_SESSION['RemoteSearchLog']['fieldName'].'\').value=\''.$Log_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';
      echo '
            <td>'.$Log_array['type'].'</td>
            <td>
                <a href="#" title="Dettagli log." class="tooltip-ajax button button_help" data-field="_log"  data-id="'.$Log_array['id'].'" data-title="Dettagli Log"  onclick="return false;">
                  <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                  <span>Dettagli</span>
                </a>
            </td>
            <td>'.$Log_array['ip'].'</td>
            <td>'.$Log_array['browser'].'</td>';
        $Log_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($Log_array['lastDateTime']));
      echo '
            <td class="no_wrap"><span class="font8">'.$Log_array['lastDateTime'].'<br/>ID: '.$Log_array['id'].' User: '.$Log_array['user_userName'].'</span></td> ';
         echo '
            <td class="vertical_middle">';
      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
        if($Log_array['active']==1)
          echo '
              <a href="#" title="disattiva" onclick="location.href=\'Log_operations.php?operation=changeField&Log__id='.$Log_array['id'].'&Log__active=0\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>';
        else
          echo '
              <a href="#" title="attiva" onclick="location.href=\'Log_operations.php?operation=changeField&Log__id='.$Log_array['id'].'&Log__active=1\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>';
      }
      else{
        if($Log_array['active']==1)
          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
        else
          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
      }
        echo '
            </td>';

      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($Log_array['id']);
        if ($filesCount > 0){
            $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
            $dataTitle='File allegati';
            $dataBody='Ci sono '.$filesCount.' file allegati.';
            if ($filesCount == 1){
                $dataTitle='File allegato';
                $dataBody='C\'&egrave; un file allegato.';
            }
        }
        else{
          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
          $dataTitle='Nessun allegato';
          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
        }
        echo '
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&Log__id='.$Log_array['id'].'\';return false;">
                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
                </a>';
        if ($Log_array['coverFile']) {
            echo '
                <a href="_Download.php?File__id=' . $Log_array['coverFile'] . '" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $Log_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>';
        }
        echo '
              </td>';
      }

      if ($_SESSION['ViewTrashedLog']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;Log__id='.$Log_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($Log_array['id']) . '</td>';
      }
      else{
        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($Log_array['id']) . '</td>';
      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#Log__type\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Tipo.\');
    }
    else if ($(\'#Log__log\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Log.\');
    }
    else if ($(\'#Log__ip\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: IP.\');
    }
    else if ($(\'#Log__browser\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Browser.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }
    
    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchLog']['objectName'].'__'.$_SESSION['RemoteSearchLog']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchLog']['objectName'].'__'.$_SESSION['RemoteSearchLog']['fieldName'].'\').value=\''.$_REQUEST['Log__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}

if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';
echo '
});
</script>';


$Design->privateHtmlClose();

?>