<?php
require_once("../config/config.inc.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../models/User.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../core/EmailNotification.class.php");
require_once("../core/LdapAuthentication.class.php");
session_start();

$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 7;
$Object = new User($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
switch ($_REQUEST['operation']) {

    case "login":

        $hasExternalAuthorization = false;
        if (LDAP_ENABLED) {
            $LdapAuthentication = new LdapAuthentication(
                LDAP_HOSTNAME,
                LDAP_PORT,
                LDAP_BASE_DN,
                LDAP_USERS_DN,
                LDAP_ADMIN_USER,
                LDAP_ADMIN_PASSWORD,
                LDAP_ID_ATTR
            );
            $hasExternalAuthorization = $LdapAuthentication->ldap_connect_and_authenticate_user($_REQUEST['Login__userName'], $_REQUEST['Login__userPassword']);
        }

        $_USER = array();
        $_USER['User__userName'] = $_REQUEST['Login__userName'];

        $sql = "SELECT `" . TBPX . "User`.*,
                       `" . TBPX . "Mask_mask`.`mask` AS `mask_mask`         
                FROM `" . TBPX . "User`
                LEFT JOIN `" . TBPX . "Mask` `" . TBPX . "Mask_mask` ON `" . TBPX . "Mask_mask`.`id` = `" . TBPX . "User`.`mask`
                WHERE `" . TBPX . "User`.`userName` = " . $DatabaseHelper->pdo->quote($_USER['User__userName']) . "
                AND `" . TBPX . "User`.`active` = 1
                AND `" . TBPX . "User`.`trashed` = 0";

        // Se non ci sono sistemi di autenticazioni esterni 
        // procedo con l'auth locale
        if (!$hasExternalAuthorization && $_SERVER['HTTP_ORIGIN'] != 'http://localhost:90') {
            $_USER['User__userPassword'] = hash('sha256', $_REQUEST['Login__userPassword']);
            $sql .= "
            AND `" . TBPX . "User`.`userPassword` = " . $DatabaseHelper->pdo->quote($_USER['User__userPassword']);
        }

        $_USER['Login__userPassword'] = '****';
        $stmt = $DatabaseHelper->prepare($sql);
        $stmt->execute();
        $User_array = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($User_array['id']) {
            $passwordLastUpdateArray = date_parse($User_array['passwordLastUpdate']);
            $passwordExpirationDate = date("Y-m-d H:i:s", mktime($passwordLastUpdateArray['hour'], $passwordLastUpdateArray['minute'], $passwordLastUpdateArray['second'], $passwordLastUpdateArray['month'], $passwordLastUpdateArray['day'] + $User_array['passwordExpirationDays'], $passwordLastUpdateArray['year']));
            $dateTimeNow = date('Y-m-d H:i:s');
            if ($dateTimeNow > $passwordExpirationDate) {
                $ModuleHelper->insertLog('login error password expired', json_encode($_USER));
                header('Location: _Login.php?message=loginErrorPasswordExpired');
                exit;
            }
            $User_array['loginTimeStamp'] = $dateTimeNow;
            $User_array['ip'] = getenv(REMOTE_ADDR);
            if ($User_array['userName'] == 'admin') {
                $sql = "SELECT `" . TBPX . "Action`.`module`, `" . TBPX . "Action`.`action`
                     FROM `" . TBPX . "Action`
                     WHERE `active` = 1
                     AND `trashed` = 0";
                $stmt = $DatabaseHelper->prepare($sql);
                $stmt->execute();
                while ($MaskArray = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $_SESSION['Mask'][$MaskArray['module']][$MaskArray['action']] = true;
                }
            } else if ($User_array['mask']) {
                $sql = "SELECT `" . TBPX . "Action`.`module`, `" . TBPX . "Action`.`action`
                     FROM `" . TBPX . "Authorization`
                     LEFT JOIN `" . TBPX . "Action` ON `" . TBPX . "Action`.`id` = `" . TBPX . "Authorization`.`action`
                     WHERE `" . TBPX . "Authorization`.`mask` = " . $DatabaseHelper->pdo->quote($User_array['mask']) . "
                     AND `" . TBPX . "Authorization`.`active` = 1
                     AND `" . TBPX . "Authorization`.`trashed` = 0
                     AND `" . TBPX . "Action`.`active` = 1
                     AND `" . TBPX . "Action`.`trashed` = 0";
                $stmt = $DatabaseHelper->prepare($sql);
                $stmt->execute();
                while ($MaskArray = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $_SESSION['Mask'][$MaskArray['module']][$MaskArray['action']] = true;
                }
            } else {
                $ModuleHelper->insertLog('login error no mask', json_encode($_USER));
                header('Location: _Login.php?message=loginErrorNoMask');
                exit;
            }

            $_SESSION['User'] = $User_array;
            $ModuleHelper->insertLog('login success', json_encode($_USER));
            header('Location: _MainFrameSet.php');
            exit;
        } else {
            $ModuleHelper->insertLog('login error', json_encode($_USER));
            header('Location: _Login.php?message=loginError');
            exit;
        }
        break;


    case "logout":
        session_start();
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();

        if ($_REQUEST['message'] != '')
            $message = "?message=$_REQUEST[message]";
        else
            $message = '';
        header('Location: _Login.php' . $message);
        exit;
        break;


    case "recuperaPassword":
        $sql = "SELECT *
		         FROM `" . TBPX . "User`
		         WHERE `active` = 1
		         AND `trashed` = 0
		         AND `userName` = " . $DatabaseHelper->pdo->quote($_REQUEST['RecuperaPassword__userName']) . "
		         AND `email` = " . $DatabaseHelper->pdo->quote($_REQUEST['RecuperaPassword__email']);
        $stmt = $DatabaseHelper->prepare($sql);
        $stmt->execute();
        $User_array = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($User_array['email']) {
            $User = new User($DatabaseHelper);
            $newPassword = $User->generatePassword();
            $arrayUpdate = array();
            $arrayUpdate['User__userPassword'] = hash('sha256', $newPassword);
            $arrayUpdate['User__passwordLastUpdate'] = date('Y-m-d H:i:s');
            $arrayUpdate['User__passwordExpirationDays'] = 1;
            $arrayUpdate['User__id'] = $User_array['id'];
            $_SESSION['User']['id'] = $User_array['id'];
            $ModuleHelper->update($arrayUpdate);
            EmailNotification::sendProvisoryNewPassword($User_array['email'], $newPassword, $arrayUpdate['User__passwordExpirationDays']);
            unset($_SESSION['User']['id']);
            header('Location: _Login.php?message=passwordSent');
            exit;
        } else {
            $ModuleHelper->insertLog('login error passwod recover', json_encode($_REQUEST));
            header('Location: _Login.php?operation=recuperaPassword&message=invalidData');
            exit;
        }
        break;


    case "modificaPassword":
        $sql = "SELECT *
		         FROM `" . TBPX . "User`
		         WHERE `active` = 1
		         AND `trashed` = 0
		         AND `userName` = " . $DatabaseHelper->pdo->quote($_REQUEST['ModificaPassword__userName']) . "
		         AND `userPassword` = '" . hash('sha256', $_REQUEST['ModificaPassword__userPassword']) . "'";
        $stmt = $DatabaseHelper->prepare($sql);
        $stmt->execute();
        $User_array = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($User_array['id']) {
            $arrayUpdate = array();
            $arrayUpdate['User__userPassword'] = hash('sha256', $_REQUEST['ModificaPassword__userNewPassword']);
            $arrayUpdate['User__passwordLastUpdate'] = date('Y-m-d H:i:s');
            $arrayUpdate['User__passwordExpirationDays'] = DEFAULT_PASSWORD_EXPIRATION_DAYS;
            $arrayUpdate['User__id'] = $User_array['id'];
            $_SESSION['User']['id'] = $User_array['id'];
            $ModuleHelper->update($arrayUpdate);
            unset($_SESSION['User']['id']);
            header('Location: _Login.php?message=passwordModified');
            exit;
        } else {
            $ModuleHelper->insertLog('login error passwod modify', json_encode($_REQUEST));
            header('Location: _Login.php?operation=modificaPassword&message=invalidData');
            exit;
        }
        break;
}
?>
