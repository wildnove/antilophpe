<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../models/File.class.php");

if (!$_REQUEST['skipsession'])
    $SessionController = new SessionController();

$DatabaseHelper = new DatabaseHelper($dbParams);
$File = new File($DatabaseHelper);
$File->loadById($_REQUEST['File__id']);

$originalFilePath = UPLOADED_DIR . '/original/' . $File->getId() . '_' . $File->getSecretKey() . '.' . $File->getExtension();

if (file_exists($originalFilePath)) {
    $fileName = $File->getOriginalFilename() ? $File->getOriginalFilename() : basename($originalFilePath);
    $fileSize = filesize($originalFilePath);

    header("Cache-Control: private");
    header("Content-Type: application/stream");
    header("Content-Length: " . $fileSize);
    header("Content-Disposition: attachment; filename=\"" . $fileName . "\"");
    readfile($originalFilePath);
    exit;
} else {
    die('The provided file path is not valid.');
}
?>
