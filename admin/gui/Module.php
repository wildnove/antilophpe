<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/Module.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 3;
$Object = new Module($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->privateHtmlOpen();

$Module = new Module($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterModule']);
    unset($_SESSION['OrderModule']);
    unset($_SESSION['NavbarModule']);
}
// Default order
if (!$_SESSION['OrderModule']) {
    $_SESSION['OrderModule']['field1'] = TBPX . 'Module_parentModule.name';
    $_SESSION['OrderModule']['field1_direction'] = '';
    $_SESSION['OrderModule']['field2'] = TBPX . 'Module.siblingOrder';
    $_SESSION['OrderModule']['field2_direction'] = '';
    $_SESSION['OrderModule']['field3'] = '';
    $_SESSION['OrderModule']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarModule']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarModule']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedModule'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedModule'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedModule'] = 0;
    $_SESSION['RemoteSearchModule']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchModule']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchModule']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $Module->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $Module_array = $Module->getAsArrayById($_REQUEST['Module__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchModule']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$Module_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $Module->loadById($_REQUEST['Module__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $Module->loadById($_REQUEST['Module__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $Module->loadById($_REQUEST['Module__id']);
    $Module->setId('');
    $_REQUEST['Module__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {
    echo '

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$Module->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="Module.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="Module_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
            <input type="hidden" value="' . $Module->getId() . '" name="Module__id" id="Module__id"/>';
    }
    echo '
            <div>
                <label for="Module__name">Nome</label><br/>
                <input data-field="name" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Module->getName()).'" name="Module__name" id="Module__name" maxlength="50" size="60"/>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Module__label">Label</label><br/>
                <input data-field="label" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Module->getLabel()).'" name="Module__label" id="Module__label" maxlength="50" size="60"/>
            </div>';
    echo '
            <div>
                <label for="Module__url">URL</label><br/>
                <textarea name="Module__url" id="Module__url" rows="3" cols="70">'.htmlspecialchars($Module->getUrl()).'</textarea>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Module__urlTarget">URL target</label><br/>
                <input data-field="urlTarget" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Module->getUrlTarget()).'" name="Module__urlTarget" id="Module__urlTarget" maxlength="255" size="50"/>
            </div>';
    if ($Module->getParentModule() == 0)
        $Module->setParentModule('');
    echo '
            <div>
                <label for="Module__parentModule">Modulo padre</label><br/>
                <input data-foreignobject="Module" data-foreignfield="name" data-field="parentModule" class="autocomplete-foreign input_text" type="text" value="' . $Module->getParentModule_name().'" name="ForeignModule__parentModule" id="ForeignModule__parentModule" size="50"/>
                &nbsp;&nbsp;
                <input class="input_text readonly" readonly="readonly" type="text" name="Module__parentModule" id="Module__parentModule" value="'.$Module->getParentModule().'" size="5"/>
                <a href="#" title="Cerca." onclick="window.open(\'Module.php?operation=remoteSearchCall&amp;objectName=Module&amp;fieldName=parentModule&amp;foreignFieldsList=name\',\'WindowModule'.rand(0,1000).'\',\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
                  <span>Cerca</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="
                  $(\'#ForeignModule__parentModule\').val(\'\');
                  $(\'#Module__parentModule\').val(\'\');
                  return false;"  class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>';
    echo '
            <div>
                <label for="Module__siblingOrder">Ordine fratelli</label><br/>
                <input class="input_text" type="text"  value="'.htmlspecialchars($Module->getSiblingOrder()).'" name="Module__siblingOrder" id="Module__siblingOrder" maxlength="4" size="14"/>
            </div>';
    echo '
            <div class="mandatory">
                <label for="Module__icon">Icona</label><br/>
                <input data-field="icon" class="autocomplete input_text" type="text" value="'.htmlspecialchars($Module->getIcon()).'" name="Module__icon" id="Module__icon" maxlength="255" size="50"/>
            </div>';
    echo '
            <div>
                <label for="Module__jsonDescription">JSON Descrittivo</label><br/>
                <textarea name="Module__jsonDescription" id="Module__jsonDescription" rows="3" cols="70">'.htmlspecialchars($Module->getJsonDescription()).'</textarea>
            </div>';
    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="Module.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $field['name'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina Moduli
        </div>';
        $fieldsArray= array(
                        'Nome' => TBPX . 'Module.name',
                        'Label' => TBPX . 'Module.label',
                        'URL' => TBPX . 'Module.url',
                        'URL target' => TBPX . 'Module.urlTarget',
                        'Modulo padre' => TBPX . 'Module_parentModule.name',
                        'Ordine fratelli' => TBPX . 'Module.siblingOrder',
                        'Icona' => TBPX . 'Module.icon',
                        'JSON Descrittivo' => TBPX . 'Module.jsonDescription',
                        'Id' => TBPX . 'Module.id',
                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'Module.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
                        'Attivo' => TBPX . 'Module.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderModule__field'.$i.'" name="OrderModule__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderModule']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderModule']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderModule__field'.$i.'_direction" name="OrderModule__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Module.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
        && ($_REQUEST['operation'] == 'manageFiles')
    ) {
        $ModuleHelper->echoFileBox($Module, $_REQUEST['File__id']);
    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedModule']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterModule']) || isset($_SESSION['OrderModule']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'Module.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'Module_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['PDF'])
      echo '
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\'Module_operations.php?operation=pdf\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['TRASH']){
      if ($_SESSION['ViewTrashedModule']!=1){
        $trashCount = $Module->getTrashCount();
        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
        echo '
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\'Module.php?operation=viewTrashed\';return false;
          ">
            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
          </a>';
      }
      else
        echo '
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\'Module.php?operation=viewNotTrashed\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>';
    }
    echo '
        </div>';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $Module->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarModule']['bloccoAttuale'], $_SESSION['NavbarModule']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();

    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableModule">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableModule_header">';
    if ($_SESSION['ViewTrashedModule']!=1)
      echo '
            <th  width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.name" title="Ordina per Nome.">Nome' . ($orderFields[TBPX . 'Module.name'] ? '&nbsp;'.($orderFields[TBPX . 'Module.name'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.label" title="Ordina per Label.">Label' . ($orderFields[TBPX . 'Module.label'] ? '&nbsp;'.($orderFields[TBPX . 'Module.label'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.url" title="Ordina per URL.">URL' . ($orderFields[TBPX . 'Module.url'] ? '&nbsp;'.($orderFields[TBPX . 'Module.url'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.urlTarget" title="Ordina per URL target.">URL target' . ($orderFields[TBPX . 'Module.urlTarget'] ? '&nbsp;'.($orderFields[TBPX . 'Module.urlTarget'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module_parentModule.name" title="Ordina per Modulo padre.">Modulo padre' . ($orderFields[TBPX . 'Module_parentModule.name'] ? '&nbsp;'.($orderFields[TBPX . 'Module_parentModule.name'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.siblingOrder" title="Ordina per Ordine fratelli.">Ordine fratelli' . ($orderFields[TBPX . 'Module.siblingOrder'] ? '&nbsp;'.($orderFields[TBPX . 'Module.siblingOrder'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.icon" title="Ordina per Icona.">Icona' . ($orderFields[TBPX . 'Module.icon'] ? '&nbsp;'.($orderFields[TBPX . 'Module.icon'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.jsonDescription" title="Ordina per JSON Descrittivo.">JSON Descrittivo' . ($orderFields[TBPX . 'Module.jsonDescription'] ? '&nbsp;'.($orderFields[TBPX . 'Module.jsonDescription'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.lastDateTime" title="Ordina per Data mod.">Data mod' . ($orderFields[TBPX . 'Module.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'Module.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th width="1%"><a href="?operation=order&amp;OrderModule__field1=' . TBPX . 'Module.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'Module.active'] ? '&nbsp;'.($orderFields[TBPX . 'Module.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
      echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['ViewTrashedModule']!=1){
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    else{
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $Module->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    while($Module_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchModule']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$Module_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedModule']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($Module_array["id"]==$_REQUEST["Module__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;Module__id='.$Module_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableModule_row" id="record'.$Module_array['id'].'">';

      if ($_SESSION['ViewTrashedModule']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchModule']['objectName'].'__'.$_SESSION['RemoteSearchModule']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchModule']['objectName'].'__'.$_SESSION['RemoteSearchModule']['fieldName'].'\').value=\''.$Module_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';
      if ($Module_array['id']==1)
        echo '
            <td><img class="vertical_middle" alt="base" src="../lib/dtree/img/base.gif"/> '.APPLICATION_NAME.'</td>';
      else
        echo '
            <td><img class="vertical_middle" alt="'.$Module_array['icon'].'" src="../img/icons/nuvola/16x16/'.$Module_array['icon'].'"/> '.$Module_array['name'].'</td>';
      echo '
            <td>'.$Module_array['label'].'</td>
            <td>';
      $Module_array['url'] = htmlspecialchars(trim($Module_array['url']));
      if ($Module_array['url']!=''){
        $maxlen = 35;
        if (strlen($Module_array['url'])>$maxlen){
            echo '
              <div class="tooltip-ajax help" data-field="url" data-id="'.$Module_array['id'].'" data-title="URL">
                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                <span>'.mb_strimwidth($Module_array['url'], 0, $maxlen, "...").'</span>
              </div>';
        } else
            echo $Module_array['url'];
      }
      else
        echo '&nbsp;';
      echo '
            </td>
            <td>'.$Module_array['urlTarget'].'</td>
            <td>'.$Module_array['parentModule_name'].'</td>
            <td>'.$Module_array['siblingOrder'].'</td>
            <td>'.$Module_array['icon'].'</td>
            <td>';
      $Module_array['jsonDescription'] = htmlspecialchars(trim($Module_array['jsonDescription']));
      if ($Module_array['jsonDescription']!=''){
        $maxlen = 35;
        if (strlen($Module_array['jsonDescription'])>$maxlen){
            echo '
              <div class="tooltip-ajax help" data-field="jsonDescription" data-id="'.$Module_array['id'].'" data-title="JSON Descrittivo">
                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                <span>'.mb_strimwidth($Module_array['jsonDescription'], 0, $maxlen, "...").'</span>
              </div>';
        } else
            echo $Module_array['jsonDescription'];
      }
      else
        echo '&nbsp;';
      echo '
            </td>';
        $Module_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($Module_array['lastDateTime']));
      echo '
            <td class="no_wrap"><span class="font8">'.$Module_array['lastDateTime'].'<br/>ID: '.$Module_array['id'].' User: '.$Module_array['user_userName'].'</span></td> ';
         echo '
            <td class="vertical_middle">';
      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
        if($Module_array['active']==1)
          echo '
              <a href="#" title="disattiva" onclick="location.href=\'Module_operations.php?operation=changeField&Module__id='.$Module_array['id'].'&Module__active=0\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>';
        else
          echo '
              <a href="#" title="attiva" onclick="location.href=\'Module_operations.php?operation=changeField&Module__id='.$Module_array['id'].'&Module__active=1\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>';
      }
      else{
        if($Module_array['active']==1)
          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
        else
          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
      }
        echo '
            </td>';

      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($Module_array['id']);
        if ($filesCount > 0){
            $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
            $dataTitle='File allegati';
            $dataBody='Ci sono '.$filesCount.' file allegati.';
            if ($filesCount == 1){
                $dataTitle='File allegato';
                $dataBody='C\'&egrave; un file allegato.';
            }
        }
        else{
          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
          $dataTitle='Nessun allegato';
          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
        }
        echo '
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&Module__id='.$Module_array['id'].'\';return false;">
                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
                </a>';
        if ($Module_array['coverFile']) {
            echo '
                <a href="_Download.php?File__id=' . $Module_array['coverFile'] . '" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $Module_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>';
        }
        echo '
              </td>';
      }

      if ($_SESSION['ViewTrashedModule']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;Module__id='.$Module_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($Module_array['id']) . '</td>';
      }
      else{
        if ($_SESSION['Mask'][$moduleId]['TRASH'])
            echo '
              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($Module_array['id']) . '</td>';
      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#Module__label\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Label.\');
    }
    else if ($(\'#Module__urlTarget\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: URL target.\');
    }
    else if ($(\'#Module__icon\').val()==\'\'){
        swal(\'Attenzione! Compilare il campo: Icona.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }
    
    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchModule']['objectName'].'__'.$_SESSION['RemoteSearchModule']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchModule']['objectName'].'__'.$_SESSION['RemoteSearchModule']['fieldName'].'\').value=\''.$_REQUEST['Module__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}


if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';
echo '
});
</script>';


$Design->privateHtmlClose();

?>