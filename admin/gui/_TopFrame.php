<?php
require_once("../config/config.inc.php");
require_once("../core/Design.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/SessionController.class.php");
$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);
$Design = new Design();
$Design->setDatabaseHelper($DatabaseHelper);
$Design->privateHtmlOpen();

// rgba(0,55,77,1) -> blu scuro

// rgba(138,186,211,1) -> azzurro chiaro

echo '
    <style>
        #header-login-box{
            float:right;
            background:white;
            font-size:10px;
            margin-top:8px;
            margin-right:8px;
            padding:5px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;     
            max-width: 250px;
        }';

echo '   
    </style>
    <div id="header" style="padding:0;background-color: #010215;">
        <img src="../img/logo.png" alt="logo" style="height:81px;"/>
        <div id="header-login-box">
            <span>Utente:</span> <span class="bold">' . ($_SESSION['User']['denomination'] == ' Admin' ? '<span class="colorred">' . $_SESSION['User']['denomination'] . '</span>' : $_SESSION['User']['denomination']) . '</span><br/>
            <span>Maschera:</span> <span class="bold">' . ($_SESSION['User']['denomination'] == ' Admin' ? '<span class="colorred">superuser</span>' : $_SESSION['User']['mask_mask']) . '</span><br/>
            <span>IP:</span> <span class="bold"> ' . $_SESSION['User']['ip'] . ' </span><br/>                        
        </div>
    </div> ';
$Design->privateHtmlClose($noswitch = 1);
?>
