<?php
require_once("../config/config.inc.php");
require_once("../core/SessionController.class.php");
require_once("../core/DatabaseHelper.class.php");
require_once("../core/Design.class.php");
require_once("../core/Navbar.class.php");
require_once("../core/ModuleHelper.class.php");
require_once("../models/User.class.php");

$SessionController = new SessionController();
$DatabaseHelper = new DatabaseHelper($dbParams);

$moduleId = 7;
$Object = new User($DatabaseHelper);

$ModuleHelper = new ModuleHelper($DatabaseHelper, $moduleId, $Object);
$moduleDescription = $ModuleHelper->getModuleDescription();

if (!isset($_SESSION['Mask'][$moduleId])) {
    echo '
    <!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
      <head><title>Session intrusion</title></head>
      <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=intrusion\'</script>
      </body>
    </html>';
    exit;
}

$Design = new Design();
$Design->privateHtmlOpen();

$User = new User($DatabaseHelper);

if ($_REQUEST['operation'] == 'filterOrder') {
    $ModuleHelper->filterOrderToSession($_REQUEST);
}
if ($_REQUEST['operation']=='order'){
    $ModuleHelper->orderToSession($_REQUEST);
}
if ($_REQUEST['operation'] == 'emptyFilterOrder' || $_REQUEST['operation'] == 'viewTrashed' || $_REQUEST['operation'] == 'viewNotTrashed'){
    unset($_SESSION['FilterUser']);
    unset($_SESSION['OrderUser']);
    unset($_SESSION['NavbarUser']);
}
// Default order
if (!$_SESSION['OrderUser']) {
    $_SESSION['OrderUser']['field1'] = TBPX . 'User.denomination';
    $_SESSION['OrderUser']['field1_direction'] = '';
    $_SESSION['OrderUser']['field2'] = '';
    $_SESSION['OrderUser']['field2_direction'] = '';
    $_SESSION['OrderUser']['field3'] = '';
    $_SESSION['OrderUser']['field3_direction'] = '';
}
if ($_REQUEST['bloccoAttuale'] != '')
    $_SESSION['NavbarUser']['bloccoAttuale'] = $_REQUEST['bloccoAttuale'];
if ($_REQUEST['numeroPagina'] != '')
    $_SESSION['NavbarUser']['numeroPagina'] = $_REQUEST['numeroPagina'];

if ($_REQUEST['operation'] == 'viewTrashed')
    $_SESSION['ViewTrashedUser'] = 1;
if ($_REQUEST['operation'] == 'viewNotTrashed')
    $_SESSION['ViewTrashedUser'] = 0;

if ($_REQUEST['operation'] == 'remoteSearchCall') {
    $_SESSION['ViewTrashedUser'] = 0;
    $_SESSION['RemoteSearchUser']['foreignFieldsList'] = $_REQUEST['foreignFieldsList'];
    $_SESSION['RemoteSearchUser']['objectName'] = $_REQUEST['objectName'];
    $_SESSION['RemoteSearchUser']['fieldName'] = $_REQUEST['fieldName'];
}

if ($_REQUEST['operation'] == 'insert') {
    $User->setId('');
}
if ($_REQUEST['operation'] == 'updated') {
    $User_array = $User->getAsArrayById($_REQUEST['User__id']);
    $array_foreignFieldsList = explode(",", $_SESSION['RemoteSearchUser']['foreignFieldsList']);
    $foreignSequence_updated = '';
    foreach ($array_foreignFieldsList as $foreignField)
        $foreignSequence_updated .= "$User_array[$foreignField] ";
    $foreignSequence_updated = rtrim($foreignSequence_updated, ' ');
}
if ($_REQUEST['operation'] == 'modify') {
    $User->loadById($_REQUEST['User__id']);
}
if ($_REQUEST['operation'] == 'manageFiles') {
    $User->loadById($_REQUEST['User__id']);
}
if ($_REQUEST['operation'] == 'duplicateManual') {
    $User->loadById($_REQUEST['User__id']);
    $User->setId('');
    $_REQUEST['User__id'] = '';
}

if (($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['UPDATE'] || $_SESSION['Mask'][$moduleId]['DUPLICATE'])
    && ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual')
) {
    echo '

<div class="FormBoxHeader">
    <div class="FormBoxHeaderInside">
        <div class="float_left no_wrap">
            '.$ModuleHelper->getModuleLogoAndLabel().'<span class="vertical_middle"> - '.($_REQUEST['operation'] == 'modify' ? 'Modifica (Id: '.$User->getId().')' : 'Aggiungi'.($_REQUEST['operation'] == 'duplicateManual' ? ' (da duplicazione)' : '')).'</span>
        </div>
        <div class="float_right padding2">
            <a href="User.php" title="Chiudi la finestra." class="button">
                <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16"/>
                <span>Chiudi</span>
            </a>
        </div>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="FormBoxContent">
    <div class="FormBoxContentInside">
        <form id="objectForm" name="objectForm" action="User_operations.php" method="post">
            <input type="hidden" name="operation" value="save"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
            <input type="hidden" value="' . $User->getId() . '" name="User__id" id="User__id"/>';
    }
    echo '
            <div class="mandatory">
                <label for="User__denomination">Denominazione</label><br/>
                <input data-field="denomination" class="autocomplete input_text" type="text" value="'.htmlspecialchars($User->getDenomination()).'" name="User__denomination" id="User__denomination" maxlength="255" size="50"/>
            </div>';
    echo '
            <div class="mandatory">
                <label for="User__userName">Utente</label><br/>
                <input data-field="userName" class="autocomplete input_text" type="text" value="'.htmlspecialchars($User->getUserName()).'" name="User__userName" id="User__userName" maxlength="50" size="60"/>
            </div>';

    echo '
	<fieldset>
	<legend>Password</legend>
	<div class="padding5 align_justify">
	Il "Codice in materia di protezione dei dati personali", con l\'allegato B al D. Lgs. n.196, impone che le password: siano segrete, debbano variare ogni 6 mesi, siano di lunghezza non inferiore agli 8 caratteri, contengano almeno un numero e una lettera maiuscola.';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
		  	<div id="pannelloBottonePassword" style="margin-top:7px;margin-bottom:7px;">
			    <a href="#" onclick="
			    	$(\'#pannelloPassword\').show(\'slow\');
			    	$(\'#pannelloBottonePassword\').hide();
			    	$(\'#flagModificaPassword\').val(\'1\');
			    	return false;
			    	" title="Modifica la password." class="button">
			      <img src="../img/icons/nuvola/16x16/apps/password.png" alt="pass" width="16" height="16" />
			      <span>Modifica password</span>
			    </a>
			</div>';
        $style = "display: none";
        $modPasswordVal = "0";
    } else
        $modPasswordVal = "1";

    echo '
	<div id="pannelloPassword" style="' . $style . '">
	  <input type="hidden" value="' . $modPasswordVal . '" name="flagModificaPassword" id="flagModificaPassword"/>
	  <div style="width:130px;float:left;" class="mandatory">
	    <label id="User__password_label" for="User__userPassword">Password</label><br/>
	    <input class="input_text" type="password"  name="User__userPassword" id="User__userPassword" maxlength="20" size="10"/>
	  </div>
	  <div style="width:580px;float:left;" class="mandatory">
	    <label id="rePassword_label" for="rePassword">Ripeti password</label><br/>
	    <input class="input_text" type="password"  name="rePassword" id="rePassword" maxlength="20" size="10"/>';
    if ($_REQUEST['operation'] == 'modify') {
        echo '
	    <a href="#" onclick="
	    	$(\'#pannelloPassword\').hide();
	    	$(\'#flagModificaPassword\').val(\'0\');
	    	$(\'#pannelloBottonePassword\').show();
	    	return false;
	    	" title="Annulla modifica password." class="button">
	      <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="undo" width="16" height="16" />
	      <span>Annulla modifica password</span>
	   </a>';
    }
    echo '
        <a href="#" onclick="var p = passwordGenerator(8);
            swal(\'Password generata: \' + p);
            $(\'#User__userPassword\').val(p);
            $(\'#rePassword\').val(p);
            return false;"
            title="Genera password automaticamente." class="button">
          <img src="../img/icons/nuvola/16x16/custom/generate.png" alt="generate" />
          <span>Genera password</span>
        </a>';
    echo '
	  </div>	  
	  <div class="clearer">&nbsp;</div>
  	</div>';

    if ($User->getId() == '')
        $User->setPasswordExpirationDays(DEFAULT_PASSWORD_EXPIRATION_DAYS);

    echo '
            <div class="mandatory">
                <label for="User__passwordExpirationDays">GG scad. password</label><br/>
                <input class="input_text" type="text"  value="'.htmlspecialchars($User->getPasswordExpirationDays()).'" name="User__passwordExpirationDays" id="User__passwordExpirationDays" maxlength="4" size="14"/>
            </div>';

    echo '
	  	
		</div>  
	</fieldset>';


    if (!$User->getMask())
        $User->setMask('');
    echo '
            <div>
                <label for="User__mask">Maschera</label><br/>
                <input data-foreignobject="Mask" data-foreignfield="mask" data-field="mask" class="autocomplete-foreign input_text" type="text" value="' . $User->getMask_mask().'" name="ForeignUser__mask" id="ForeignUser__mask" size="50"/>
                &nbsp;&nbsp;
                <input class="input_text readonly" readonly="readonly" type="text" name="User__mask" id="User__mask" value="'.$User->getMask().'" size="5"/>
                <a href="#" title="Cerca." onclick="window.open(\'Mask.php?operation=remoteSearchCall&amp;objectName=User&amp;fieldName=mask&amp;foreignFieldsList=mask\',\'WindowMask'.rand(0,1000).'\',\'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes\');return false;" class="button">
                  <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="CERCA" width="16" height="16" />
                  <span>Cerca</span>
                </a>
                <a href="#" title="Pulisci campo." onclick="
                  $(\'#ForeignUser__mask\').val(\'\');
                  $(\'#User__mask\').val(\'\');
                  return false;"  class="button">
                  <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="PULISCI" width="16" height="16" />
                  <span>Pulisci</span>
                </a>
            </div>';
    echo '
            <div class="mandatory">
                <label for="User__email">Email</label><br/>
                <input data-field="email" class="autocomplete input_text" type="text" value="'.htmlspecialchars($User->getEmail()).'" name="User__email" id="User__email" maxlength="255" size="50"/>
            </div>';

    echo '
        </form>
    </div>
</div>
<div class="FormBoxFooter">
    <div class="FormBoxFooterInside">
        <a href="#" title="Salva." onclick="submitObjectForm();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
            <span>Salva</span>
        </a>
        <a href="#" title="Annulla modifiche." onclick="objectForm.reset();return false;" class="button">
            <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
            <span>Annulla modifiche</span>
        </a>
    </div>
</div>';
}
else{
      echo '
    <div class="display_none" id="FilterBox">
      <div id="FilterContainer">
       <form onkeypress="submitFilterFormByEnter(event)" action="User.php" method="post" name="filterForm" id="filterForm">
        <input type="hidden" name="operation" value="filterOrder"/>
        <div class="thickboxCaption">
          <div class="float_left no_wrap">
            Filtra<br/><span class="font9 italic">(Usare % come carattere jolly es: %parola%)</span>
          </div>
          <div class="float_right padding2">
            <a href="#" title="Chiudi la finestra." onclick="$(\'#FilterBox\').dialog(\'close\');return false;" class="button">
              <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
              <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
            </a>
          </div>
          <div class="clearer">&nbsp;</div>
        </div>
        <div class="modalFormOverflow">
            <table>
              <tr class="display_none">
                <th>Campo</th>
                <th>Operatore logico / Valore</th>
                <th>Valore</th>
              </tr>';
        $prevGroupLabel = '';
        foreach ($moduleDescription['fields'] AS $name => $field) {
            if ($field['grouplabel'] != $prevGroupLabel && $field['grouplabel']) {
                echo '
      <tr>
        <td colspan="3" class="filter_grouplabel"><label>&nbsp;' . $field['grouplabel'] . '</label></td>
       </tr>';
                $prevGroupLabel = $field['grouplabel'];
            }
            if ($field['grouplabel'])
                $field['label'] = '&nbsp;&nbsp;&nbsp;'.$field['label'];
            $field['name'] = $name;
            $ModuleHelper->echoDefaultFilterForField($field);
        }
        echo '
        </table>
        <br/>
        <div class="thickboxCaption">
          Ordina Utenti
        </div>';
        $fieldsArray= array(
                        'Denominazione' => TBPX . 'User.denomination',
                        'Utente' => TBPX . 'User.userName',
                        'Password' => TBPX . 'User.userPassword',
                        'Maschera' => TBPX . 'Mask_mask.mask',
                        'Email' => TBPX . 'User.email',
                        'Agg. password' => TBPX . 'User.passwordLastUpdate',
                        'GG scad. password' => TBPX . 'User.passwordExpirationDays',
                        'Id' => TBPX . 'User.id',
                        'File cover' => TBPX . 'File_coverFile.title',
                        'Data mod' => TBPX . 'User.lastDateTime',
                        'Utente mod' => TBPX . 'User_user.userName',
                        'Attivo' => TBPX . 'User.active',
                );
        echo '
            <table>
              <tr class="display_none">
                <th>Sequenza</th>
                <th>Campo</th>
                <th>Crescente / decrescente</th>
              </tr>';
        for($i=1; $i<=3; $i++){
            echo '
              <tr>
                <td><label>'.$i.'. Ordina per&nbsp;&nbsp;</label></td>
                <td>
                  <select id="OrderUser__field'.$i.'" name="OrderUser__field'.$i.'">
                    <option value="">Scegli...</option>';
            foreach($fieldsArray as $key => $value){
              echo '
                    <option '.($value == $_SESSION['OrderUser']['field'.$i] ? 'selected="selected"' : '').' value="'.$value.'">'.$key.'</option>';
            }
            echo '
                  </select>
                </td>
                <td>&nbsp;&nbsp; <input type="checkbox" '.($_SESSION['OrderUser']['field'.$i.'_direction']=='DESC' ? 'checked="checked"' : '').' value="DESC" id="OrderUser__field'.$i.'_direction" name="OrderUser__field'.$i.'_direction"> <span class="label_checkbox">decrescente</span></td>
              </tr>';
        }
        echo '
            </table>
        </div>
        <div class="button_box">
          <a href="#" title="Filtra e ordina la tabella." class="button" onclick="submitFilterForm();return false;">
            <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="filtra" width="16" height="16" />
            <span>Filtra/Ordina</span>
          </a>
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'User.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>
        </div>
      </form>
      </div>
    </div>';

    if (($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
        && ($_REQUEST['operation'] == 'manageFiles')
    ) {
        $ModuleHelper->echoFileBox($User, $_REQUEST['File__id']);
    }

    echo '
        <div id="box_operation_buttons" class="align_center">';

    if ($_SESSION['ViewTrashedUser']!=1 && ($_SESSION['Mask'][$moduleId]['INSERT'] || $_SESSION['Mask'][$moduleId]['DUPLICATE']))
      echo '
        <a href="?operation=insert" title="Aggiungi." class="button">
          <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
          <span>Aggiungi</span>
        </a>';

    echo '
        <a href="#" onclick="$(\'#FilterBox\').dialog(\'open\');return false;" title="Filtra e ordina la tabella." class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag.png" alt="filtra/ordina" width="16" height="16" />
          <span>Filtra/Ordina</span>
        </a>';

    if (isset($_SESSION['FilterUser']) || isset($_SESSION['OrderUser']))
      echo '
          <a href="#" title="Reset filtro e ordinamento tabella." class="button" onclick="
            location.href=\'User.php?operation=emptyFilterOrder\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/actions/undo.png" alt="x" width="16" height="16" />
            <span>Reset filtro</span>
          </a>';

    if ($_SESSION['Mask'][$moduleId]['EXCEL'])
      echo '
        <a href="#" title="Esporta la tabella in Excel." class="button" onclick="
          location.href=\'User_operations.php?operation=excel\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/custom/excel_icon.png" alt="excel" width="16" height="16" />
          <span>Excel</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['PDF'])
      echo '
        <a href="#" title="Stampa in pdf." class="button" onclick="
          location.href=\'User_operations.php?operation=pdf\';return false;
        ">
          <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="pdf" width="16" height="16" />
          <span>Pdf</span>
        </a>';

    if ($_SESSION['Mask'][$moduleId]['TRASH']){
      if ($_SESSION['ViewTrashedUser']!=1){
        $trashCount = $User->getTrashCount();
        $trashImg = $trashCount > 0 ? '../img/icons/nuvola/16x16/filesystems/trashcan_full.png' : '../img/icons/nuvola/16x16/filesystems/trashcan_empty.png';
        echo '
          <a href="#" title="Visualizza cestinati." class="button" onclick="
            location.href=\'User.php?operation=viewTrashed\';return false;
          ">
            <img src="'.$trashImg.'" alt="cestino" width="16" height="16" />
            <span>Cestino'.($trashCount > 0 ? ' ('.$trashCount.')' : '').'</span>
          </a>';
      }
      else
        echo '
          <a href="#" title="Torna al desktop." class="button" onclick="
            location.href=\'User.php?operation=viewNotTrashed\';return false;
          ">
            <img src="../img/icons/nuvola/16x16/filesystems/desktop.png" alt="torna" width="16" height="16" />
            <span>Desktop</span>
          </a>';
    }
    echo '
        </div>';

    list($whereFilters, $orderOrder, $orderFields) = $ModuleHelper->makeWhereAndOrder();
    $sql = $User->getDefaultSelectSql($countSql = true);
    $sql .= $whereFilters;
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $totRecords = $stmt->fetch(PDO::FETCH_NUM);

    $Navbar = new Navbar($totRecords[0], 20, 10);
    $navBar = $Navbar->makeNavBar($_SESSION['NavbarUser']['bloccoAttuale'], $_SESSION['NavbarUser']['numeroPagina'], $_REQUEST['getRequest']);
    $offset = $navBar["start"];
    $limit = $Navbar->getRighePerPagina();
    $getRequest .= "&";
    echo $navBar['css'];
    echo $navBar['barra'];

    echo '
    <input type="hidden" name="Navbar_tot_records" id="Navbar_tot_records" value="'.intval($totRecords[0]).'"/>
    <div class="align_left table_box">
      <table border="1" id="TableUser">
        <caption class="align_center font12 bold gray padding2 vertical_middle">'.$ModuleHelper->getModuleLogoAndLabel().'</caption>
        <thead>
          <tr class="TableUser_header">';
    if ($_SESSION['ViewTrashedUser']!=1)
      echo '
            <th  width="1%" class="selectorArrow">&nbsp;</th>';
    echo '
            <th class="no_wrap"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'User.denomination" title="Ordina per Denominazione.">Denominazione' . ($orderFields[TBPX . 'User.denomination'] ? '&nbsp;'.($orderFields[TBPX . 'User.denomination'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'User.userName" title="Ordina per Utente.">Utente' . ($orderFields[TBPX . 'User.userName'] ? '&nbsp;'.($orderFields[TBPX . 'User.userName'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'Mask_mask.mask" title="Ordina per Maschera.">Maschera' . ($orderFields[TBPX . 'Mask_mask.mask'] ? '&nbsp;'.($orderFields[TBPX . 'Mask_mask.mask'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'User.email" title="Ordina per Email.">Email' . ($orderFields[TBPX . 'User.email'] ? '&nbsp;'.($orderFields[TBPX . 'User.email'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th class="no_wrap">Scadenza password</th>   
            <th class="no_wrap"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'User.lastDateTime" title="Ordina per Data mod.">Data mod' . ($orderFields[TBPX . 'User.lastDateTime'] ? '&nbsp;'.($orderFields[TBPX . 'User.lastDateTime'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>
            <th width="1%"><a href="?operation=order&amp;OrderUser__field1=' . TBPX . 'User.active" title="Ordina per stato attivo/non attivo.">AT' . ($orderFields[TBPX . 'User.active'] ? '&nbsp;'.($orderFields[TBPX . 'User.active'] == 'DESC' ? '&#9662;' : '&#9652;') : '') . '</a></th>';
    if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE'])
      echo '
            <th width="1%">&nbsp;</th>';
    if ($_SESSION['ViewTrashedUser']!=1){
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo '
            <th width="1%">&nbsp;</th>';
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    else{
      if ($_SESSION['Mask'][$moduleId]['TRASH'])
        echo '
            <th width="1%">&nbsp;</th>';
    }
    echo '
          </tr>
        </thead>
        <tbody>';

    $sql = $User->getDefaultSelectSql($countSql = false);
    $sql .= $whereFilters;
    $sql .= $orderOrder;
    $sql = $ModuleHelper->limit($sql, $offset,$limit);
    $stmt = $DatabaseHelper->prepare($sql);
    $stmt->execute();
    $count = 0;
    while($User_array = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_foreignFieldsList=explode(",",$_SESSION['RemoteSearchUser']['foreignFieldsList']);
      $foreignSequence='';
      foreach($array_foreignFieldsList as $foreignField)
        $foreignSequence.="$User_array[$foreignField] ";
      $foreignSequence=rtrim($foreignSequence,' ');

      if ($_SESSION['ViewTrashedUser']==1){
        $count++;if($count%2==1) $colorazione_riga="gray"; else $colorazione_riga="lightgray";
      }
      else{
        $count++;if($count%2==1) $colorazione_riga="lightgray"; else $colorazione_riga="";
      }

      if ($User_array["id"]==$_REQUEST["User__id"])
        $colorazione_riga="selected_table_row";

      echo '
            <tr';
      if ($_SESSION['Mask'][$moduleId]['UPDATE'])
        echo ' ondblclick="location.href=\'?operation=modify&amp;User__id='.$User_array['id'].'\';"';
      echo ' class="'.$colorazione_riga.' contextMenuTableRecord TableUser_row" id="record'.$User_array['id'].'">';

      if ($_SESSION['ViewTrashedUser']!=1)
        echo '
              <td class="link selectorArrow">
                <a href="#" title="seleziona"
                  onclick="
                    if (window.opener){
                      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchUser']['objectName'].'__'.$_SESSION['RemoteSearchUser']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence))).'\');
                      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchUser']['objectName'].'__'.$_SESSION['RemoteSearchUser']['fieldName'].'\').value=\''.$User_array['id'].'\';
                      window.close();
                    }
                    else
                      swal(\'Nessuna operazione di selezione richiesta.\');
                    return false;
                  "><img src="../img/icons/nuvola/16x16/actions/forward.png" alt="seleziona" width="16" height="16"/></a>
              </td>';
      echo '
            <td>'.$User_array['denomination'].'</td>
            <td>'.$User_array['userName'].'</td>
            <td>'.$User_array['mask_mask'].'</td>
            <td>'.$User_array['email'].'</td>';
        $passwordLastUpdateArray = date_parse($User_array['passwordLastUpdate']);
        $passwordExpirationDate = date("d/m/Y H:i:s", mktime($passwordLastUpdateArray['hour'], $passwordLastUpdateArray['minute'], $passwordLastUpdateArray['second'], $passwordLastUpdateArray['month'], $passwordLastUpdateArray['day'] + $User_array['passwordExpirationDays'], $passwordLastUpdateArray['year']));
        echo '
            <td>'.$passwordExpirationDate.'</td>';
        $User_array['lastDateTime']=date('d/m/Y H:i:s', strtotime($User_array['lastDateTime']));
      echo '
            <td class="no_wrap"><span class="font8">'.$User_array['lastDateTime'].'<br/>ID: '.$User_array['id'].' User: '.$User_array['user_userName'].'</span></td> ';
         echo '
            <td class="vertical_middle">';
      if ($_SESSION['Mask'][$moduleId]['ACTIVATE']){
        if($User_array['active']==1)
          echo '
              <a href="#" title="disattiva" onclick="location.href=\'User_operations.php?operation=changeField&User__id='.$User_array['id'].'&User__active=0\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>
              </a>';
        else
          echo '
              <a href="#" title="attiva" onclick="location.href=\'User_operations.php?operation=changeField&User__id='.$User_array['id'].'&User__active=1\';return false;">
                <img src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>
              </a>';
      }
      else{
        if($User_array['active']==1)
          echo '<img src="../img/icons/nuvola/16x16/actions/ledgreen.png" alt="green" width="16" height="16"/>';
        else
          echo '<img  src="../img/icons/nuvola/16x16/actions/ledred.png" alt="red" width="16" height="16"/>';
      }
        echo '
            </td>';

      if ($_SESSION['Mask'][$moduleId]['FILESVIEW'] || $_SESSION['Mask'][$moduleId]['FILESMANAGE']){
        $filesCount = $ModuleHelper->getFilesCountByObjectId($User_array['id']);
        if ($filesCount > 0){
            $icon='../img/icons/nuvola/16x16/filesystems/folder_green.png';
            $dataTitle='File allegati';
            $dataBody='Ci sono '.$filesCount.' file allegati.';
            if ($filesCount == 1){
                $dataTitle='File allegato';
                $dataBody='C\'&egrave; un file allegato.';
            }
        }
        else{
          $icon='../img/icons/nuvola/16x16/filesystems/folder_grey_open.png';
          $dataTitle='Nessun allegato';
          $dataBody='Non ci sono file allegati. Click per aggiungerne.';
        }
        echo '
              <td class="no_wrap vertical_middle">
                <a href="#" class="tooltip link no_underline" data-title="'.$dataTitle.'" data-body="'.$dataBody.'" onclick="location.href=\'?operation=manageFiles&User__id='.$User_array['id'].'\';return false;">
                  <img src="'.$icon.'" alt="files" width="16" height="16"/>
                </a>';
        if ($User_array['coverFile']) {
            echo '
                <a href="_Download.php?File__id=' . $User_array['coverFile'] . '" class="vertical_middle" title="Download file originale"><img class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $User_array['coverFile'] . '" data-title="Cover" src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/></a>';
        }
        echo '
              </td>';
      }

      if ($_SESSION['ViewTrashedUser']!=1){
        if ($_SESSION['Mask'][$moduleId]['UPDATE'])
          echo '
                <td class="vertical_middle">
                  <a href="#" title="modifica" onclick="location.href=\'?operation=modify&amp;User__id='.$User_array['id'].'\';return false;">
                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="modifica" width="16" height="16"/>
                  </a>
                </td>';

        if ($_SESSION['Mask'][$moduleId]['TRASH'])
          echo '
              <td class="vertical_middle">' . $ModuleHelper->renderTrashAction($User_array['id']) . '</td>';
      }
      else{
        if ($_SESSION['Mask'][$moduleId]['TRASH'])
          echo '
              <td class="vertical_middle">' . $ModuleHelper->renderRestoreAction($User_array['id']) . '</td>';
      }
      echo '
            </tr>';
    }
    echo '
          </tbody>
        </table>
      </div>';
    echo $navBar['barra'];
    echo '<br/>';
}
echo '
<script>
function passwordGenerator( len ) {
    var length = (len)?(len):(10);
    var string = "abcdefghjkmnpqrstuvwxyz"; //to upper 
    var numeric = \'23456789\';
    var punctuation = \'\';
    var password = "";
    var character = "";
    var crunch = true;
    while( password.length<length ) {
        entity1 = Math.ceil(string.length * Math.random()*Math.random());
        entity2 = Math.ceil(numeric.length * Math.random()*Math.random());
        entity3 = Math.ceil(punctuation.length * Math.random()*Math.random());
        hold = string.charAt( entity1 );
        hold = (password.length%4==0)?(hold.toUpperCase()):(hold);
        character += hold;
        character += numeric.charAt( entity2 );
        character += punctuation.charAt( entity3 );
        password = character;
    }
    password=password.split(\'\').sort(function(){return 0.5-Math.random()}).join(\'\');
    return password.substr(0,len);
}

function validEmail(elementValue){
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(elementValue);
}

function validPassword(password){
    if(password.length < 8)
        return false;
    if(!password.match(/[a-z]+/))
        return false;
    if(!password.match(/[0-9]+/))
        return false;
    if(!password.match(/[A-Z]+/))
        return false;
    return true;
}

function submitObjectForm(){
    $(window).unbind(\'beforeunload\');
    
    if (false){}
    else if ($(\'#User__denomination\').val()==\'\'){
        $(\'#User__denomination\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Denominazione.\');
    }
    else if ($(\'#User__userName\').val()==\'\'){
        $(\'#User__userName\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Utente.\');
    }
    else if (!validPassword($(\'#User__userPassword\').val()) && $(\'#flagModificaPassword\').val()==\'1\'){
	$(\'#User__userPassword\').scrollViewAndFocus();
        swal(\'Attenzione! La "Password" deve essere lunga almeno 8 caratteri, contenere almeno un numero e una lettera maiuscola.\');
        $(\'#User__userPassword\').val(\'\');
        $(\'#rePassword\').val(\'\');
    }
    else if ($(\'#User__userPassword\').val()!=$(\'#rePassword\').val() && $(\'#flagModificaPassword\').val()==\'1\'){
	$(\'#User__userPassword\').scrollViewAndFocus();        
	swal(\'Attenzione! Il campo "Password" ed il campo "Ripeti Password" non coincidono.\');
        $(\'#User__userPassword\').val(\'\');
        $(\'#rePassword\').val(\'\');
    }    
    else if ($(\'#User__email\').val()==\'\'){
        $(\'#User__email\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: Email.\');
    }
    else if (!validEmail($(\'#User__email\').val())){
	$(\'#User__email\').scrollViewAndFocus();
        swal(\'Attenzione! Email non valida.\');
    }
    else if ($(\'#User__passwordExpirationDays\').val()==\'\'){
        $(\'#User__passwordExpirationDays\').scrollViewAndFocus();
        swal(\'Attenzione! Compilare il campo: GG scad. password.\');
    }
    else
        $(\'#objectForm\').submit();
    return false;
}';
    $ModuleHelper->echoDefaultJsFunctions();
echo '
$(document).ready(function(){
    if (window.opener){
        $(\'.selectorArrow\').show();        
    }

    if (window.opener && \''.$_REQUEST['operation'].'\' == \'updated\'){
      window.opener.document.getElementById(\'Foreign'.$_SESSION['RemoteSearchUser']['objectName'].'__'.$_SESSION['RemoteSearchUser']['fieldName'].'\').value=unescape(\''.str_replace('+',' ',urlencode(utf8_decode($foreignSequence_updated))).'\');
      window.opener.document.getElementById(\''.$_SESSION['RemoteSearchUser']['objectName'].'__'.$_SESSION['RemoteSearchUser']['fieldName'].'\').value=\''.$_REQUEST['User__id'].'\';
      window.close();
      return false;
    }';
    $ModuleHelper->echoDefaultJsInit();

if ($_REQUEST['operation']=='manageFiles'){
    echo '
    $(\'#FileBox\').dialog(\'open\');';
}

if ($_REQUEST['operation']=='remoteSearchCall'){
    echo '
    $(\'#FilterBox\').dialog(\'open\');';
}

if ($_REQUEST['mex']!='')
    echo 'swal(\''.$_REQUEST['mex'].'\')';
           
echo '
});
</script>';


$Design->privateHtmlClose();

?>