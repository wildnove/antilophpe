function pdfMultiplePrint(object_name, tot_records, pdf_multiple_max_rows) {
    if (tot_records <= pdf_multiple_max_rows) {
        location.href = object_name + '_operations.php?operation=objectPdf';
    } else {
        swal({
                title: 'Stampa multipla pdf',
                text: 'Dato il filtro e l\'ordinamento attuale inserire il numero di scheda di partenza tra 1 a ' + tot_records + ' per la stampa multipla in pdf di max ' + pdf_multiple_max_rows + ' schede successive. Gli estremi sono compresi, es: inserendo 1 verranno stampate le schede estratte dalla 1 alla ' + pdf_multiple_max_rows + ' comprese, quindi il blocco successivo partira\' da ' + (pdf_multiple_max_rows + 1) + '.\n\nAttenzione l\'operazione potrebbe richiedere anche qualche minuto, attendere.',
                type: 'input',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                animation: 'slide-from-top',
                inputPlaceholder: 'Numero di partenza'
            },
            function (inputValue) {
                if (inputValue === false)
                    return false;

                if (inputValue === '') {
                    swal.showInputError('Inserire un numero');
                    return false;
                }

                if (1 <= parseInt(inputValue) && parseInt(inputValue) <= parseInt(tot_records)) {
                    location.href = object_name + '_operations.php?operation=objectPdf&start=' + inputValue;
                    return false;
                } else {
                    swal.showInputError('Inserire un numero tra 1 e ' + tot_records);
                    return false;
                }
            });
    }
    return false;
}
