(function ($) {

    $.fn.textareacrud_configurable = function (objectName, fieldJson, readOnly) {

        this.each(function () {

            if (this.nodeName.toLowerCase() !== "textarea") {
                console.log('This is not a <textarea>');
                return false;
            }

            var textarea = this;

            var fieldDescription = JSON.parse(fieldJson);
            var fieldsCount = Object.keys(fieldDescription['subfields']).length;

            var htmlForm = '<div class="textareacrud_configurable-wrapper align_left table_box" style="padding:0px;" tabindex="-1">' +
                '<table>' +
                '<thead>' +
                (!readOnly ? '<tr>' +
                    '<td colspan="' + (fieldsCount + 1) + '"><i>Attenzione! La riga non viene salvata in automatico con la scheda, cliccare su Archivia e salvare la scheda per salvare i dati.</i></td>' +
                    '</tr>' : '') +
                '<tr>';

            for (var key in fieldDescription['subfields']) {
                var subfield = fieldDescription['subfields'][key];

                if (subfield['mandatory'] !== false)
                    htmlForm += '<td class="mandatory"><label>' + subfield['label'] + '</label></td>';
                else
                    htmlForm += '<td><label>' + subfield['label'] + '</label></td>';
            }

            htmlForm += (!readOnly ? '<td></td>' : '') +
                '</tr>' +
                '</thead>' +
                '<tbody>';


            htmlForm += '' +
                '<tr class="js-tbody">';
            if (!readOnly) {
                for (var key in fieldDescription['subfields']) {
                    var subfield = fieldDescription['subfields'][key];
                    htmlForm += '<td>';
                    if (subfield['type'] == 'select') {
                        htmlForm += '<select id="js-' + key + '" style="max-width:90%">' +
                            '<option value="">Seleziona...</option>';
                        subfield['typeoptions']['values'].forEach(function (entry) {
                            htmlForm += '<option value="' + entry + '">' + entry + '</option>';
                        });
                        htmlForm += '</select>';
                    } else if (subfield['type'] == 'text') {
                        htmlForm += '<textarea id="js-' + key + '" style="width:90%"></textarea>';
                    } else if (subfield['type'] == 'object') {
                        htmlForm += '<input type="text" class="foreignobject" placeholder="Seleziona" data-objectname="' + subfield['typeoptions']['name'] + '" data-sourcedestfields="id|js-' + key + ',' + subfield['typeoptions']['field'] + '|js-' + subfield['typeoptions']['field'] + '" id="js-' + key + '" maxlength="' + subfield['maxlen'] + '" size="6" style="background: url(../img/icons/nuvola/16x16/actions/viewmag.png) no-repeat scroll 3px 3px;padding-left:24px;"/>';
                    } else {
                        htmlForm += '<input type="text" id="js-' + key + '" maxlength="' + subfield['maxlen'] + '" style="width:90%"/>';
                    }
                    htmlForm += '</td>';
                }
                htmlForm += '<td><button type="button" class="js-aggiungi">Archivia</button></td>';
            }
            htmlForm += '' +
                '</tr>';

            htmlForm +=
                '</tbody>' +
                '</table>' +
                '</div>';

            var wrapper = $(htmlForm).insertAfter($(textarea).closest('div'));
            $(textarea).closest('div').prependTo(wrapper);
            $(textarea).hide();

            var css = '.textareacrud_configurable-wrapper table tr:hover{ background-color: #FFFFFF !important; }';
            style = document.createElement('style');
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            document.getElementsByTagName('head')[0].appendChild(style);

            var wrapperDiv = $(textarea).closest('.textareacrud_configurable-wrapper');


            var jsonValue = $(textarea).val();
            var jsonObj = [];
            if (jsonValue)
                jsonObj = JSON.parse(jsonValue);

            renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);

            wrapperDiv.find('.js-aggiungi').bind("click", function () {

                for (var key in fieldDescription['subfields']) {
                    var subfield = fieldDescription['subfields'][key];
                    if (subfield['mandatory'] !== false && !$('#js-' + key).val().trim()) {
                        swal('Attenzione! Compilare correttamente il campo ' + subfield['label'] + '.');
                        return false;
                    }
                }
                var row = {}
                for (var key in fieldDescription['subfields']) {
                    row[key] = $('#js-' + key).val();
                    $('#js-' + key).val('');
                }
                jsonObj.push(row);
                renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);

            });

            wrapperDiv.find('.foreignobject').bind("click", function () {
                window.open($(this).data("objectname") + '.php?operation=remoteSearchCall&sourceDestFields=' + $(this).data("sourcedestfields"), 'Window' + $(this).data("objectname"), 'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
                return false;
            });

        });


        return this;
    };


    function move(array, oldIndex, newIndex) {
        if (newIndex >= array.length) {
            newIndex = array.length - 1;
        }
        array.splice(newIndex, 0, array.splice(oldIndex, 1)[0]);
        return array;
    };

    function renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly) {

        wrapperDiv.find('.crud_row').remove();

        htmlRows = '';
        var counterRow = 0;
        jsonObj.forEach(function (row) {
            counterRow++;
            htmlRows = '<tr class="crud_row">';
            for (var key in fieldDescription['subfields']) {
                htmlRows += '<td>' + (row[key] == null ? '' : row[key]) + '</td>';
            }
            if (!readOnly) {
                htmlRows += '<td class="no_wrap">';
                htmlRows += '<button type="button" title="modifica" class="js-edit"><span class="icon icon-pencil"></span></button>';
                htmlRows += '<button type="button" title="duplica" class="js-clone"><span class="icon icon-copy"></span></button>';
                htmlRows += '<button type="button" title="cancella" class="js-delete"><span class="icon icon-cross"></span></button>';
                if (counterRow > 1)
                    htmlRows += '<button type="button" class="js-moveup"><span class="icon icon-arrow-up"></span></button>';
                htmlRows += '</td>';
            }
            htmlRows += '</tr>';

            wrapperDiv.find('.js-tbody').before(htmlRows);

            wrapperDiv.find('.js-edit').unbind("click").bind("click", function (evt) {
                wrapperDiv.find('.js-tbody').hide();
                var tr = $(this).closest('tr');
                var index = tr.index();

                editForm = '<tr class="crud_row">';
                for (var key in fieldDescription['subfields']) {
                    var subfield = fieldDescription['subfields'][key];
                    jsonObj[index][key] = (jsonObj[index][key] == null ? '' : jsonObj[index][key]);
                    editForm += '<td>';
                    if (subfield['type'] == 'select') {

                        editForm += '<select id="js-' + key + '-' + index + '" style="max-width:90%">' +
                            '<option value="">Seleziona...</option>';
                        subfield['typeoptions']['values'].forEach(function (entry) {
                            if (jsonObj[index][key] == entry)
                                editForm += '<option selected="selected" value="' + entry + '">' + entry + '</option>';
                            else
                                editForm += '<option value="' + entry + '">' + entry + '</option>';
                        });
                        editForm += '</select>';
                    } else if (subfield['type'] == 'text') {
                        editForm += '<textarea id="js-' + key + '-' + index + '" style="width:90%">' + jsonObj[index][key] + '</textarea>';
                    } else if (subfield['type'] == 'object') {
                        editForm += '<input value="' + jsonObj[index][key].replace(/"/g, '&quot;') + '" type="text" class="foreignobject readonly" readonly="readonly" data-objectname="' + subfield['typeoptions']['name'] + '" data-sourcedestfields="id|js-' + key + '-' + index + ',' + subfield['typeoptions']['field'] + '|js-' + subfield['typeoptions']['field'] + '-' + index + '" id="js-' + key + '-' + index + '" maxlength="' + subfield['maxlen'] + '" style="width:90%"/>';
                    } else {
                        editForm += '<input value="' + jsonObj[index][key].replace(/"/g, '&quot;') + '" type="text" id="js-' + key + '-' + index + '" maxlength="' + subfield['maxlen'] + '" style="width:90%"/>';
                    }
                    editForm += '</td>';
                }

                editForm += '<td class="no_wrap">';
                editForm += '<button type="button" title="annulla" class="js-undo-edit"><span class="icon icon-undo"></span></button>';
                editForm += '<button type="button" title="ok" class="js-ok-edit"><span class="icon icon-checkmark"></span></button>';
                editForm += '</td>';
                editForm += '</tr>';

                tr.replaceWith(editForm);

                wrapperDiv.find('.foreignobject').bind("click", function () {
                    window.open($(this).data("objectname") + '.php?operation=remoteSearchCall&sourceDestFields=' + $(this).data("sourcedestfields"), 'Window' + $(this).data("objectname"), 'width=1100,height=650 ,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
                    return false;
                });

                wrapperDiv.find('.js-undo-edit').unbind("click").bind("click", function (evt) {
                    swal({
                            title: 'Confermi annullamento modifiche della riga?',
                            text: '',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Confermo',
                            closeOnConfirm: true
                        },
                        function () {
                            renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);
                            $(textarea).val(JSON.stringify(jsonObj));
                            wrapperDiv.find('.js-tbody').show();
                        });
                });

                wrapperDiv.find('.js-ok-edit').unbind("click").bind("click", function (evt) {
                    var tr = $(this).closest('tr');
                    var index = tr.index();

                    for (var key in fieldDescription['subfields']) {
                        var subfield = fieldDescription['subfields'][key];
                        if (subfield['mandatory'] !== false && !$('#js-' + key + '-' + index).val().trim()) {
                            swal('Attenzione! Compilare correttamente il campo ' + subfield['label'] + '.');
                            return false;
                        }
                    }
                    var row = {}
                    for (var key in fieldDescription['subfields']) {
                        row[key] = $('#js-' + key + '-' + index).val();
                    }
                    jsonObj[index] = row;
                    renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);
                    $(textarea).val(JSON.stringify(jsonObj));
                    wrapperDiv.find('.js-tbody').show();
                });

            });

            wrapperDiv.find('.js-clone').unbind("click").bind("click", function (evt) {
                var tr = $(this).closest('tr');
                swal({
                        title: 'Confermi duplicazione riga? I campi compilati saranno sovrascritti.',
                        text: '',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Confermo',
                        closeOnConfirm: true
                    },
                    function () {
                        var index = tr.index();
                        for (var key in fieldDescription['subfields']) {
                            var subfield = fieldDescription['subfields'][key];
                            jsonObj[index][key] = (jsonObj[index][key] == null ? '' : jsonObj[index][key]);
                            $('#js-' + key).val(jsonObj[index][key]);
                        }
                    });
            });

            wrapperDiv.find('.js-delete').unbind("click").bind("click", function (evt) {
                var tr = $(this).closest('tr');
                swal({
                        title: 'Confermi rimozione della riga?',
                        text: '',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Confermo',
                        closeOnConfirm: true
                    },
                    function () {
                        var index = tr.index();
                        jsonObj.splice(index, 1);
                        renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);
                        $(textarea).val(JSON.stringify(jsonObj));
                    });
            });

            wrapperDiv.find('.js-moveup').unbind("click").bind("click", function (evt) {
                var tr = $(this).closest('tr');
                var index = tr.index();
                move(jsonObj, index, index - 1);
                renderTable(wrapperDiv, jsonObj, fieldDescription, textarea, readOnly);
                $(textarea).val(JSON.stringify(jsonObj));
            });

        });

      //  if (!colorAllRows) {
      //      wrapperDiv.find('.crud_row').css({color: "black", fontWeight: "normal"});
      //      wrapperDiv.find('.crud_row:last').css({color: "blue", fontWeight: "bold"});
      //  }
        if (counterRow > 0)
            $(textarea).val(JSON.stringify(jsonObj));
    };


}(jQuery));

