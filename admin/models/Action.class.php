<?php

class Action
{
    private $DatabaseHelper;
    private $module;
    private $module_name;
    private $action;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setModule($module)
    {
        $this->module = $module;
    }

    function getModule()
    {
        return $this->module;
    }

    function setModule_name($module_name)
    {
        $this->module_name = $module_name;
    }

    function getModule_name()
    {
        return $this->module_name;
    }

    function setAction($action)
    {
        $this->action = $action;
    }

    function getAction()
    {
        return $this->action;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "Action`.*,
                     `" . TBPX . "Module_module`.`name` AS `module_name`,
                     `" . TBPX . "Module_module`.`label` AS `module_label`,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "Action`
                     LEFT JOIN `" . TBPX . "Module` `" . TBPX . "Module_module` ON `" . TBPX . "Module_module`.`id` = `" . TBPX . "Action`.`module`
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "Action`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "Action`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Action`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "Action`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    function listModulesWithFilesManage()
    {
        $sql = $this->getDefaultSelectSql() . " 
                WHERE `" . TBPX . "Action`.`action` = 'FILESMANAGE'
                AND `" . TBPX . "Action`.`active` = 1
                AND `" . TBPX . "Action`.`trashed` = 0";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function selectModulesWithActions()
    {
        // GROUP BY: utilizzare subselect
        $sql = "SELECT `module`, `module_label` FROM (" 
            . $this->getDefaultSelectSql() . "
            ) `tbl` GROUP BY `module`, `module_label`";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}

?>
