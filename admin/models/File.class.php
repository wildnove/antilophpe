<?php

class File
{
    private $DatabaseHelper;
    private $objectName;
    private $objectId;
    private $secretKey;
    private $title;
    private $description;
    private $originalFilename;
    private $mimetype;
    private $extension;
    private $sizeKb;
    private $widthPx;
    private $heightPx;
    private $hasThumbnail;
    private $hasResampled;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setObjectName($objectName)
    {
        $this->objectName = $objectName;
    }

    function getObjectName()
    {
        return $this->objectName;
    }

    function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    function getObjectId()
    {
        return $this->objectId;
    }

    function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    function getSecretKey()
    {
        return $this->secretKey;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function getTitle()
    {
        return $this->title;
    }

    function setDescription($description)
    {
        $this->description = $description;
    }

    function getDescription()
    {
        return $this->description;
    }

    function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;
    }

    function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;
    }

    function getMimetype()
    {
        return $this->mimetype;
    }

    function setExtension($extension)
    {
        $this->extension = $extension;
    }

    function getExtension()
    {
        return $this->extension;
    }

    function setSizeKb($sizeKb)
    {
        $this->sizeKb = $sizeKb;
    }

    function getSizeKb()
    {
        return $this->sizeKb;
    }

    function setWidthPx($widthPx)
    {
        $this->widthPx = $widthPx;
    }

    function getWidthPx()
    {
        return $this->widthPx;
    }

    function setHeightPx($heightPx)
    {
        $this->heightPx = $heightPx;
    }

    function getHeightPx()
    {
        return $this->heightPx;
    }

    function setHasThumbnail($hasThumbnail)
    {
        $this->hasThumbnail = $hasThumbnail;
    }

    function getHasThumbnail()
    {
        return $this->hasThumbnail;
    }

    function setHasResampled($hasResampled)
    {
        $this->hasResampled = $hasResampled;
    }

    function getHasResampled()
    {
        return $this->hasResampled;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "File`.*,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "File`
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "File`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "File`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "File`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        if ($record) {
            foreach ($record as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "File`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    function isCoverFile($objectName = null, $objectId = null, $id = null)
    {
        if (!$objectName)
            $objectName = $this->getObjectName();
        if (!$objectId)
            $objectId = $this->getObjectId();
        if (!$id)
            $id = $this->getId();

        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . $objectName . "`
                WHERE `id` = " . $objectId . "
                AND `coverFile` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0] == 1 ? true : false;
    }

    function loadUserManual()
    {
        $sql = "SELECT `id`
                FROM `" . TBPX . "File`
                WHERE `objectId` = 1
                AND `objectName` = 'Module'";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $manual = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($manual['id']) {
            $record = $this->getAsArrayById($manual['id']);
            if ($record) {
                foreach ($record as $key => $value) {
                    $this->$key = $value;
                }
            }
        }
    }


}

?>
