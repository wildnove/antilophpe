<?php

class User
{
    private $DatabaseHelper;
    private $denomination;
    private $userName;
    private $userPassword;
    private $mask;
    private $mask_mask;
    private $email;
    private $passwordLastUpdate;
    private $passwordExpirationDays;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setDenomination($denomination)
    {
        $this->denomination = $denomination;
    }

    function getDenomination()
    {
        return $this->denomination;
    }

    function setUserName($userName)
    {
        $this->userName = $userName;
    }

    function getUserName()
    {
        return $this->userName;
    }

    function setUserPassword($userPassword)
    {
        $this->userPassword = $userPassword;
    }

    function getUserPassword()
    {
        return $this->userPassword;
    }

    function setMask($mask)
    {
        $this->mask = $mask;
    }

    function getMask()
    {
        return $this->mask;
    }

    function setMask_mask($mask_mask)
    {
        $this->mask_mask = $mask_mask;
    }

    function getMask_mask()
    {
        return $this->mask_mask;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function getEmail()
    {
        return $this->email;
    }

    function setPasswordLastUpdate($passwordLastUpdate)
    {
        $this->passwordLastUpdate = $passwordLastUpdate;
    }

    function getPasswordLastUpdate()
    {
        return $this->passwordLastUpdate;
    }

    function setPasswordExpirationDays($passwordExpirationDays)
    {
        $this->passwordExpirationDays = $passwordExpirationDays;
    }

    function getPasswordExpirationDays()
    {
        return $this->passwordExpirationDays;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "User`.*,
                     `" . TBPX . "Mask_mask`.`mask` AS `mask_mask`,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "User`
                     LEFT JOIN `" . TBPX . "Mask` `" . TBPX . "Mask_mask` ON `" . TBPX . "Mask_mask`.`id` = `" . TBPX . "User`.`mask`
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "User`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "User`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "User`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "User`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    function userNameIsFree($userName, $id = null)
    {
        $rows = $this->DatabaseHelper->select(TBPX . 'User', array('userName' => $userName));
        if (!$rows[0]['userName'])
            return true;
        if ($rows[0]['id'] != $id)
            return false;
        else
            return true;
    }

    function generatePassword($length = 8, $strength = 5)
    {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    function listUsersWithSpecificMask($maskId){
        $sql = $this->getDefaultSelectSql() . "
            WHERE `" . TBPX . "User`.`trashed` = 0 
            AND `" . TBPX . "User`.`active` = 1
            AND `ant_User`.`mask` = ".$maskId;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function listUsersWithAction($idModule, $action){
        require_once("Authorization.class.php");
        $Authorization = new Authorization($this->DatabaseHelper);
        $masksIds = $Authorization->listMasksWithSpecificAuthorization($idModule, $action);
        $listUsers = [];
        foreach ($masksIds as $maskId){
            $listUsers = array_merge($listUsers, $this->listUsersWithSpecificMask($maskId));
        }
        return $listUsers;

    }


}

?>
