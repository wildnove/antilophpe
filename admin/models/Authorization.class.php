<?php

class Authorization
{
    private $DatabaseHelper;
    private $mask;
    private $mask_mask;
    private $action;
    private $action_action;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setMask($mask)
    {
        $this->mask = $mask;
    }

    function getMask()
    {
        return $this->mask;
    }

    function setMask_mask($mask_mask)
    {
        $this->mask_mask = $mask_mask;
    }

    function getMask_mask()
    {
        return $this->mask_mask;
    }

    function setAction($action)
    {
        $this->action = $action;
    }

    function getAction()
    {
        return $this->action;
    }

    function setAction_action($action_action)
    {
        $this->action_action = $action_action;
    }

    function getAction_action()
    {
        return $this->action_action;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "Authorization`.*,
                     `" . TBPX . "Mask_mask`.`mask` AS `mask_mask`,
                     `" . TBPX . "Action_action`.`action` AS `action_action`,
                     `" . TBPX . "Module_module`.`id` AS `module_id`,
                     `" . TBPX . "Module_module`.`name` AS `module_name`,
                     `" . TBPX . "Module_module`.`label` AS `module_label`,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "Authorization`
                     LEFT JOIN `" . TBPX . "Mask` `" . TBPX . "Mask_mask` ON `" . TBPX . "Mask_mask`.`id` = `" . TBPX . "Authorization`.`mask`
                     LEFT JOIN `" . TBPX . "Action` `" . TBPX . "Action_action` ON `" . TBPX . "Action_action`.`id` = `" . TBPX . "Authorization`.`action`
                     LEFT JOIN `" . TBPX . "Module` `" . TBPX . "Module_module` ON `" . TBPX . "Module_module`.`id` = `" . TBPX . "Action_action`.`module`                     
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "Authorization`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "Authorization`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Authorization`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "Authorization`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    function selectMasksWithAuthorizations()
    {
        // GROUP BY: utilizzare subselect
        $sql = "SELECT `mask_mask` FROM ("
            . $this->getDefaultSelectSql() . "
            ) `tbl` GROUP BY `mask_mask`";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    function listMasksWithSpecificAuthorization($idModule, $action)
    {
        // GROUP BY: utilizzare subselect
        $sql = "SELECT `mask` FROM ("
            . $this->getDefaultSelectSql() . "
            WHERE `" . TBPX . "Authorization`.`trashed` = 0 
            AND `" . TBPX . "Authorization`.`active` = 1
            AND `" . TBPX . "Module_module`.`id` = ".$idModule."
            AND `" . TBPX . "Action_action`.`action` = '".$action."'
            ) `tbl` GROUP BY `mask`";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $masksIds = [];
        while($Mask_array = $stmt->fetch(PDO::FETCH_ASSOC)){
            $masksIds[] = $Mask_array['mask'];
        }
        return $masksIds;
    }

}

?>
