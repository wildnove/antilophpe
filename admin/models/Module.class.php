<?php

class Module
{
    private $DatabaseHelper;
    private $name;
    private $label;
    private $url;
    private $urlTarget;
    private $parentModule;
    private $parentModule_name;
    private $siblingOrder;
    private $icon;
    private $jsonDescription;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setName($name)
    {
        $this->name = $name;
    }

    function getName()
    {
        return $this->name;
    }

    function setLabel($label)
    {
        $this->label = $label;
    }

    function getLabel()
    {
        return $this->label;
    }

    function setUrl($url)
    {
        $this->url = $url;
    }

    function getUrl()
    {
        return $this->url;
    }

    function setUrlTarget($urlTarget)
    {
        $this->urlTarget = $urlTarget;
    }

    function getUrlTarget()
    {
        return $this->urlTarget;
    }

    function setParentModule($parentModule)
    {
        $this->parentModule = $parentModule;
    }

    function getParentModule()
    {
        return $this->parentModule;
    }

    function setParentModule_name($parentModule_name)
    {
        $this->parentModule_name = $parentModule_name;
    }

    function getParentModule_name()
    {
        return $this->parentModule_name;
    }

    function setSiblingOrder($siblingOrder)
    {
        $this->siblingOrder = $siblingOrder;
    }

    function getSiblingOrder()
    {
        return $this->siblingOrder;
    }

    function setIcon($icon)
    {
        $this->icon = $icon;
    }

    function getIcon()
    {
        return $this->icon;
    }

    function setJsonDescription($jsonDescription)
    {
        $this->jsonDescription = $jsonDescription;
    }

    function getJsonDescription()
    {
        return $this->jsonDescription;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "Module`.*,
                     `" . TBPX . "Module_parentModule`.`name` AS `parentModule_name`,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "Module`
                     LEFT JOIN `" . TBPX . "Module` `" . TBPX . "Module_parentModule` ON `" . TBPX . "Module_parentModule`.`id` = `" . TBPX . "Module`.`parentModule`
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "Module`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "Module`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Module`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "Module`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    function getIdByName($name)
    {
        $sql = "SELECT `id`
                FROM `" . TBPX . "Module`
                WHERE `" . TBPX . "Module`.`name` = :name";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->bindValue(":name", $name);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row)
            return $row['id'];
        return false;
    }

}

?>