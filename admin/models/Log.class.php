<?php

class Log
{
    private $DatabaseHelper;
    private $type;
    private $log;
    private $ip;
    private $browser;
    private $id;
    private $coverFile;
    private $coverFile_title;
    private $lastDateTime;
    private $user;
    private $user_userName;
    private $active;
    private $trashed;

    function setType($type)
    {
        $this->type = $type;
    }

    function getType()
    {
        return $this->type;
    }

    function setLog($log)
    {
        $this->log = $log;
    }

    function getLog()
    {
        return $this->log;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function getIp()
    {
        return $this->ip;
    }

    function setBrowser($browser)
    {
        $this->browser = $browser;
    }

    function getBrowser()
    {
        return $this->browser;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function getId()
    {
        return $this->id;
    }

    function setCoverFile($coverFile)
    {
        $this->coverFile = $coverFile;
    }

    function getCoverFile()
    {
        return $this->coverFile;
    }

    function setCoverFile_title($coverFile_title)
    {
        $this->coverFile_title = $coverFile_title;
    }

    function getCoverFile_title()
    {
        return $this->coverFile_title;
    }

    function setLastDateTime($lastDateTime)
    {
        $this->lastDateTime = $lastDateTime;
    }

    function getLastDateTime()
    {
        return $this->lastDateTime;
    }

    function setUser($user)
    {
        $this->user = $user;
    }

    function getUser()
    {
        return $this->user;
    }

    function setUser_userName($user_userName)
    {
        $this->user_userName = $user_userName;
    }

    function getUser_userName()
    {
        return $this->user_userName;
    }

    function setActive($active)
    {
        $this->active = $active;
    }

    function getActive()
    {
        return $this->active;
    }

    function setTrashed($trashed)
    {
        $this->trashed = $trashed;
    }

    function getTrashed()
    {
        return $this->trashed;
    }

    function __construct($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function __destruct()
    {
    }

    function getDefaultSelectSql($countSql = false)
    {
        if ($countSql)
            $stmt = "SELECT COUNT(*)";
        else
            $stmt = "SELECT `" . TBPX . "Log`.*,
                     `" . TBPX . "File_coverFile`.`title` AS `coverFile_title`,
                     `" . TBPX . "User_user`.`userName` AS `user_userName`";
        return $stmt . "
                     FROM `" . TBPX . "Log`
                     LEFT JOIN `" . TBPX . "File` `" . TBPX . "File_coverFile` ON `" . TBPX . "File_coverFile`.`id` = `" . TBPX . "Log`.`coverFile`
                     LEFT JOIN `" . TBPX . "User` `" . TBPX . "User_user` ON `" . TBPX . "User_user`.`id` = `" . TBPX . "Log`.`user`";
    }

    function getAsArrayById($id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Log`.`id` = " . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function getPrevAsArrayByType($id, $type)
    {
        $type = str_replace(' insert ', ' % ', $type);
        $type = str_replace(' update ', ' % ', $type);
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Log`.`id` < " . $id . " AND `" . TBPX . "Log`.`type` LIKE '" . $type . "' ORDER BY `" . TBPX . "Log`.`id` DESC LIMIT 0,1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function getSqlFilteredByObject($objectType, $id)
    {
        $sql = $this->getDefaultSelectSql() . " WHERE `" . TBPX . "Log`.`type` LIKE '%" . TBPX . $objectType . " id=" . $id . "' ORDER BY `" . TBPX . "Log`.`id` DESC";
        return $sql;
    }

    function loadById($id)
    {
        $record = $this->getAsArrayById($id);
        foreach ($record as $key => $value) {
            $this->$key = $value;
        }
    }

    function getTrashCount()
    {
        $sql = "SELECT COUNT(*)
                FROM `" . TBPX . "Log`
                WHERE `trashed` = 1";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        return $record[0];
    }

    static function getObjectNameByType($type)
    {
        $objectName = '';
        if (strpos($type, TBPX) !== false && strpos($type, ' id=') !== false) {
            $exp = explode(TBPX, $type);
            $exp = explode(' id=', $exp[1]);
            $objectName = $exp[0];
        }
        return $objectName;
    }

}

?>