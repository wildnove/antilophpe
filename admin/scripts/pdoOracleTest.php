<?php
require_once("../config/config.inc.php");

try {
    $pdo_oci = new PDO("oci:dbname=" . $dbParams['dbname'], $dbParams['user'], $dbParams['password']);
    $sql = "SELECT * FROM dual";
    $stmt = $pdo_oci->prepare($sql);
    $stmt->execute();
    if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        var_dump($row);
    }
} catch (PDOException $e) {
    var_dump($e->getMessage());
}