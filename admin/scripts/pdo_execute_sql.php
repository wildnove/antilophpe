<?php
exit;

require_once("../core/DatabaseHelper.class.php");

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}


echo '
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
  <head>
    <title>PDO execute sql</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <meta name="author" content="www.ncfsistemi.com"/>
    <meta name="robots" content="noindex, nofollow"/>
    <script src="../lib/jquery/jquery.min.js" language="javascript" type="text/javascript"></script>
    <style>
        div{
        margin:5px;
        }
    </style>
  </head>
  <body>
  <h1>PDO execute sql</h1>';

if (!isset($_REQUEST['multiLineSql']) || $_REQUEST['multiLineSql'] == '') {
    echo '
<form action="" method="post">
    <div>
        <label>Use config.inc.php</label>
        <input type="checkbox" id="useConfig" name="useConfig" checked="checked" value="1"/>
    </div>
    <fieldset id="dbParams">
        <legend>DB params</legend>
        <div>
            <label>DB driver</label>
            <input type="text" id="driver" name="driver"/>
        </div>
        <div>
            <label>DB host</label>
            <input type="text" id="host" name="host"/>
        </div>
        <div>
            <label>DB name</label>
            <input type="text" id="dbname" name="dbname"/>
        </div>        
        <div>
            <label>DB user</label>
            <input type="text" id="user" name="user"/>
        </div>
        <div>
            <label>DB password</label>
            <input type="password" id="password" name="password"/>        
        </div>    
        <div>
            <label>DB charset</label>
            <input type="text" id="charset" name="charset"/>
        </div>      
    </fieldset>
    <div>
        <label>Multi Line Sql</label><br/>
        <textarea style="width:90%;height:300px" id="multiLineSql" name="multiLineSql"></textarea>
    </div>
    <div>
        <label>Only preview (do not run queries)</label>
        <input type="checkbox" id="onlyPreview" name="onlyPreview" checked="checked" value="1"/>
    </div>
    <input type="submit" value="GO">
</form>';
} else {
    if ($_REQUEST['useConfig'] == '1') {
        require_once("../config/config.inc.php");
    } else {
        $dbParams['driver'] = $_REQUEST['driver'];
        $dbParams['host'] = $_REQUEST['host'];
        $dbParams['dbname'] = $_REQUEST['dbname'];
        $dbParams['user'] = $_REQUEST['user'];
        $dbParams['password'] = $_REQUEST['password'];
        $dbParams['charset'] = $_REQUEST['charset'];
    }

    $DatabaseHelper = new DatabaseHelper($dbParams);
    try {
        $_REQUEST['multiLineSql'] = preg_replace('%(/\*)(.*?)(\*/)%s', "", $_REQUEST['multiLineSql']);
        $_REQUEST['multiLineSql'] = preg_replace('%(--).*%', "", $_REQUEST['multiLineSql']);

        $multiLineSql_exp = preg_split("/;\s*\n/", $_REQUEST['multiLineSql']);
        foreach ($multiLineSql_exp as $sql) {
            $sql = trim($sql);
            if (!$sql)
                continue;
            if (substr($sql, 0, strlen('CREATE OR REPLACE TRIGGER')) === 'CREATE OR REPLACE TRIGGER') {
                $sql .= '; 
END;
';
            } else if (substr($sql, 0, strlen('/')) === '/') {
                $sql = trim(ltrim($sql, '/'));
            } else if (substr($sql, 0, strlen('END')) === 'END') {
                continue;
            } else{
                $sql = trim(rtrim($sql, ';'));
            }
            echo '<pre>' . $sql . '</pre><hr/>';
            if ($_REQUEST['onlyPreview'] != 1) {
                $stmt = $DatabaseHelper->prepare($sql);
                $stmt->execute();
            }
        }

    } catch (PDOException $e) {
        var_dump($e->getMessage());
    }
}

echo "
<script>
$(document).ready(function() {
    $('#useConfig').change(function() {
        if(this.checked) 
            $('#dbParams').hide();        
        else
            $('#dbParams').show();                     
    });

    $('#useConfig').prop('checked', false);
    $('#onlyPreview').prop('checked', true);    
});
</script>
    </body>
</html>
";