<?php
/*
Copyright (C) 2018-2019 Mauro Fasolo <mr.yattle@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
class GetOpts {

    private $name = "";
    private $cmd = array();
    private $optional = array();
    private $required = array();
    private $index = array();
    private $user_options = array();

    function __construct($name, $cmd) {
        $this->name = (!empty($name) ? $name : basename(__FILE__));
        $this->cmd = $cmd;
        $this->cmd[] = array("name" => "help", 
            "short" => "h",
            "long" => "help");
        $this->required = array_filter($this->cmd, 
            function($el) {
                return (array_key_exists("required", $el) &&
                $el["required"]);
            });
        $this->optional = array_filter($this->cmd, 
            function($el) {
                return (!array_key_exists("required", $el) || 
                    !$el["required"]);
            });
        $this->indexing(); 
    }

    private function indexing() {
        foreach($this->cmd AS $el) {
            if(!array_key_exists("name", $el))
                throw new Exception((array_key_exists("long", $el)?"".$el["long"]:"".$el["short"] ).": misssing name property");  
            if(array_key_exists("short", $el))
                $this->index[$el["short"]] = $el["name"];
            if(array_key_exists("long", $el))
                $this->index[$el["long"]] = $el["name"];
        }
    } 

    public function args() {
        $shortopts = implode("", array_map(
            function($el) {
                $constraint = "";
                if(array_key_exists("value", $el))
                    $constraint = ($el["required"]?":":"::");
                return $el["short"].$constraint;
            },
            array_filter($this->cmd, 
                function($el) {
                    return array_key_exists("short", $el);
                })
        ));

        $longopts  = array_map(
            function($el) {
                $constraint = "";
                if(array_key_exists("value", $el))
                    $constraint = ($el["required"]?":":"::");
                return $el["long"].$constraint;
            },
            array_filter($this->cmd, 
                function($el) {
                    return array_key_exists("long", $el);
                })
        );

        $_user_options = getopt($shortopts, $longopts);
        foreach($_user_options AS $key => $value) {
            $this->user_options[ $this->index[$key] ] = $value;
        }
        return $this->user_options;
    }

    public function missing_args() {
        return (sizeof($this->user_options) < 
            sizeof($this->required));
    }

    public function help() {
        return (array_key_exists("help", $this->user_options)
            || array_key_exists("h", $this->user_options));
    }

    private function print_options_usage($section, $max_long, $label="") {

        if(!empty($label))
            $output = "$label:\n";

        foreach($section as $el) {
            $short = "";
            if(array_key_exists("short", $el)) {
                $short = "-".$el["short"].",";
                if(!array_key_exists("long", $el)) {
                    $short = rtrim($short, ",");
                    $short .= " ";
                }
            }
            $output .= str_pad($short." ", 6, " ", STR_PAD_LEFT);

            $long = "";
            if(array_key_exists("long", $el))
                $long = "--".$el["long"];
            $output .= str_pad($long, $max_long+5, " ", STR_PAD_RIGHT);

            if(array_key_exists("description", $el)) {
                $lines = explode("\n", $el["description"]);
                $output .= (!empty($el["value"])?"<".$el["value"]."> ":"") .$lines[0];
                for($i = 1; $i < sizeof($lines); $i++) {
                    $output .= "\n".
                        str_repeat(" ", $max_long-1).
                        $lines[$i];
                }
            }
            $output .= "\n";
        }

        echo $output;

    }

    private function required_command() {
        if(sizeof($this->required) == 0)
            return "";

        return " ".implode(" ", array_map(
            function($el) {
                if(!array_key_exists("value", $el))
                    throw new Exception((array_key_exists("long", $el)?"--".$el["long"]:"-".$el["short"] ).": declaring required field you need to specify also value and value_constraint => true");                
                if(!array_key_exists("long", $el)) {
                    return "-".$el["short"].(!empty($el["value"])?"<".$el["value"].">":"");
                }
                return "--".$el["long"].(!empty($el["value"])?"=<".$el["value"].">":"");                
            },
            $this->required
        ));
    }

    private function optional_command() {
        if(sizeof($this->optional) == 0)
            return "";

        return " [".implode(" ", array_map(
            function($el) {
                if(!array_key_exists("long", $el)) {
                    return "-".$el["short"].(!empty($el["value"])?"<".$el["value"].">":"");
                }
                return "--".$el["long"].(!empty($el["value"])?"=<".$el["value"].">":"");     
            },
            $this->optional
        ))."]";
    }    

    public function usage() {

        $max_long = 0;
        foreach($this->cmd as $el) {
            if(strlen($el["long"])>$max_long)
                $max_long = intval(strlen($el["long"]));
        }

        echo "Usage:\n";
        echo str_repeat(" ",2) .
            "./" . $this->name .
            $this->required_command(). 
            $this->optional_command();
        echo "\n\n";

        if(sizeof($this->required) > 0) 
            $this->print_options_usage(
                $this->required,
                $max_long,
                "Required options");
        echo "\n";
        if(sizeof($this->optional) > 0) 
            $this->print_options_usage(
                $this->optional,
                $max_long,
                "Other options");
        echo "\n";
    } 

}

/*
$cmd = array(
    array("name" = "tns",
        "short" => "n",
        "long" => "tns",
        "description" => "TNS that can be used to connect Oracle Database
            eg: --tns=".str_replace(" ", "", str_replace("\n", "", "(DESCRIPTION =
                    (ADDRESS_LIST =
                      (ADDRESS = (PROTOCOL = TCP)
                        (HOST = localhost)(PORT = 1521))
                    )
                    (CONNECT_DATA =
                      (SERVICE_NAME = XE)
                    )
                )")),
        "required" => true,
        "value" => "tns"),
    array("name" = "pretty",
        "long" => "prettyprint",
        "description" => "SQL Pretty print",
        "value" => "SQL"),
    array("name" = "res",
        "short" => "r",
        "description" => "Reset you momma",
        "required" => false,
        "value" => "reset"),
    array("name" = "gino",
        "short" => "g",
        "description" => "Do not dump the specified table.
            To specify more than one table to ignore, use the directive multiple times, 
            once for each table.  
            Each table must be specified with both database and table names, e.g.,")


);

$opt = new GetOpts($cmd);
$user_args = $opt->args();
if($opt->help()) {
    $opt->usage();
    exit(0);
}
if($opt->missing_args()) {
    $opt->usage();
    exit(0);
}
*/
?>