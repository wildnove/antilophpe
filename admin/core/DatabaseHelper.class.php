<?php

class DatabaseHelper
{
    public $pdo;
    public $error;
    public $driver = "mysql";

    function __construct($dbParams)
    {
        try {
            if ( array_key_exists("driver", $dbParams))
                $this->driver = $dbParams['driver'];

            $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

            if ($dbParams['driver'] == 'mysql') {
                if ($dbParams['charset'] and version_compare(PHP_VERSION, '5.3.6', '<'))
                    $options[PDO::MYSQL_ATTR_INIT_COMMAND] = "SET NAMES '" . $dbParams['charset'] . "'";

                $this->pdo = new PDO(
                    $dbParams['driver'] . ":host=" . $dbParams['host'] . ";dbname=" . $dbParams['dbname'] . ";charset=" . $dbParams['charset'],
                    $dbParams['user'],
                    $dbParams['password'],
                    $options);

                $serverversion = $this->pdo->getAttribute(PDO::ATTR_SERVER_VERSION);
                $emulatePrepares = (version_compare($serverversion, '5.1.17', '<'));
                $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, $emulatePrepares);
                $this->pdo->query("SET sql_mode = '';"); // Automatically truncate data when inserting
            } else if ($dbParams['driver'] == 'pgsql') {
                $this->pdo = new PDO(
                    $dbParams['driver'] . ":host=" . $dbParams['host'] . ";dbname=" . $dbParams['dbname'],
                    $dbParams['user'],
                    $dbParams['password'],
                    $options);
                $this->pdo->query("SET NAMES '" . $dbParams['charset'] . "'");
            } else if ($dbParams['driver'] == 'oci') {
                $charset = "";
                if ( array_key_exists("charset", $dbParams))
                    $charset = sprintf(";charset=%s", $dbParams["charset"]);
                $this->pdo = new PDO(
                    $dbParams['driver'] . ":dbname=".$dbParams['dbname'].$charset,
                    $dbParams['user'],
                    $dbParams['password'],
                    $options);
                $this->pdo->query("ALTER SESSION SET nls_date_format='DD-MM-YYYY HH24:MI:SS'");
                // This is useful to enable case insentitive LIKE in Oracle OCI8
                $this->pdo->query("ALTER SESSION SET NLS_COMP=LINGUISTIC");
                $this->pdo->query("ALTER SESSION SET NLS_SORT=BINARY_CI");
            } else {
                $this->pdo = new PDO(
                    $dbParams['driver'] . ":host=" . $dbParams['host'] . ";dbname=" . $dbParams['dbname'] . ";charset=" . $dbParams['charset'],
                    $dbParams['user'],
                    $dbParams['password'],
                    $options);
            }
        } catch (Exception $e) {
            die($e);
        }
    }

    function __destruct()
    {
        $this->pdo = null;
    }

    private static function startswith($haystack, $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    static public function cleanFieldsArray($Object_name, $dirtyFieldsArray)
    {
        $fieldsArray = array();
        foreach ($dirtyFieldsArray as $key => $value) {
            if (self::startswith($key, $Object_name . '__') || !$Object_name)
                $fieldsArray[str_replace($Object_name . '__', '', $key)] = $value;
        }
        return $fieldsArray;
    }

    public function dbescape($sql) {
        $word_escape = "`";
        if($this->driver == "oci")
            $word_escape = '"'; 
        if($this->driver == "pgsql")
            $word_escape = '"'; 
        return str_replace("`", $word_escape, $sql);    
    }

    public function getDriver() {
        return $this->driver;   
    }

    public function bindVarName($field) { // MUST BE PUBLIC
        // prevent var names from db forbidden words
        return $this->driver."_".$field;
    }

    public function sqlBindFormatter($field, $value) { // MUST BE PUBLIC
        // Others
        if($this->driver != "oci")
            return ":".$this->bindVarName($field);  

        // Oracle
        
        // Timestamp
        if(strlen($value) == 19 &&
            preg_match('/[0-9]{4}\-[0-9]{2}\-[0-9]{2}\ [0-9]{2}:[0-9]{2}:[0-9]{2}/', 
                $value))
          return "TO_TIMESTAMP(:".$this->bindVarName($field).", 'YYYY-MM-DD HH24:MI:SS')"; 
        
        // Date
        if(strlen($value) == 10 &&
            preg_match('/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/', 
                $value))
          return "TO_DATE(:".$this->bindVarName($field).", 'YYYY-MM-DD')"; 

        return ":".$this->bindVarName($field);     
    }

    private function sqlInsertBindValues($fieldsArray) {
        $values = "";
        foreach ($fieldsArray as $field => $value) {
            $values .= " ,".$this->sqlBindFormatter($field, $value);
        }
        return ltrim($values, ", ");
    }

    public function prepare($sql) {
        return  $this->pdo->prepare( $this->dbescape($sql) );
    } 

    function limit($sql, $offset = 0, $limit = 20) {
        if($this->driver != "oci")
            return $sql. " LIMIT $offset, $limit";
        
        return "SELECT * FROM (
            SELECT rownum rnum, a.* 
            FROM(
                $sql
            ) a 
            WHERE rownum <=$offset+$limit
        )
        WHERE rnum >=$offset";
    }    

    public function insert($table, $fieldsArray, $exceptionDie = true)
    {
        // https://stackoverflow.com/questions/26578313/how-do-i-create-a-sequence-in-mysql
        // viso che MySQL non utillzza le sequenze dobbiamo utilizzare
        // PRE: pesco la sequenza, inserisco il record
        // POST: inserisco il record, ottendo il last insert Id
        // PRE: Oracle
        // POSt: MySQL
        $sql = '';
        try {
            // PRE: Oracle
            if($this->driver == "oci") {
                $stmt = $this->pdo->prepare('SELECT 
                    "' . $table . '_seq".NEXTVAL AS "new_id" FROM DUAL');
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_ASSOC);      
                $fieldsArray["id"] = $result["new_id"];
            }

            $sql = 'INSERT INTO `' . $table . '` (`' . implode('`, `', array_keys($fieldsArray)) . '`) VALUES ('.$this->sqlInsertBindValues($fieldsArray).')';
            $stmt = $this->prepare($sql);
            foreach ($fieldsArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field), $value);
            }
            $stmt->execute();

            // PRE: Oracle
            if($this->driver == "oci") {
                return $fieldsArray["id"]; 
            }

            // MySQL
            return $this->pdo->lastInsertId();
            
        } catch (Exception $e) {
            if ($exceptionDie)
                die($e . ' ' . $sql);
            else {
                $this->error = $e;
                return false;
            }
        }
    }

    public function update($table, $fieldsArray, $whereArray)
    {
        $sql = '';
        try {
            $nameValueFieldsArray = array();
            foreach($fieldsArray as $key => $value) { 
                $nameValueFieldsArray[] = array($key, $value); 
            }

            $instance = $this;

            $updateString = implode(', ', array_map(
                function ($fieldValue) use ($instance) {
                    list($field, $value) = $fieldValue;
                    return '`' . $field . '`=' . $instance->sqlBindFormatter($field, $value);
                },
                $nameValueFieldsArray
            ));
            
            $nameValueWhereArray = array();
            foreach($whereArray as $key => $value) { 
                $nameValueWhereArray[] = array($key, $value); 
            }
            $whereString = implode(' AND ', array_map(
                function ($fieldValue) use ($instance) {
                    list($field, $value) = $fieldValue;
                    return '`' . $field . '`=' . $instance->sqlBindFormatter($field, $value) . 'Where';
                },
                $nameValueWhereArray
            ));

            $sql = 'UPDATE `' . $table . '` SET ' . $updateString . ' WHERE ' . $whereString;
            $stmt = $this->prepare($sql);

            foreach ($fieldsArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field), $value);
            }
            foreach ($whereArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field) . 'Where', $value);
            }
            if ($stmt->execute())
                return $stmt->rowCount();
            else
                return false;
        } catch (Exception $e) {
            die($e . ' ' . $sql);
        }
    }

    public function insertOrUpdate($table, $fieldsArray, $uniquesArray)
    {
        $sql = '';
        try {
            $fieldsNoUniquesArray = $fieldsArray;
            foreach ($uniquesArray as $unique) {
                unset($fieldsNoUniquesArray[$unique]);
            }

            $instance = $this;

            $updateString = implode(', ', array_map(
                function ($field) use ($instance) {
                    return '`' . $field . '`=:' . $instance->bindVarName($field) . 'NoUnique';
                },
                array_keys($fieldsNoUniquesArray)
            ));

            $sql = 'INSERT INTO `' . $table . '` (`' . implode('`, `', array_keys($fieldsArray)) . '`) VALUES (:' . implode(', :', array_keys($fieldsArray)) . ') ON DUPLICATE KEY UPDATE ' . $updateString;
            $stmt = $this->prepare($sql);

            foreach ($fieldsArray as $field => $value) {
                $stmt->bindValue(":" . $field, $value);
            }
            foreach ($fieldsNoUniquesArray as $field => $value) {
                $stmt->bindValue(":" . $field . 'NoUnique', $value);
            }
            return $stmt->execute();
        } catch (Exception $e) {
            die($e . ' ' . $sql);
        }
    }

    public function delete($table, $whereArray)
    {
        $sql = '';
        try {
            $instance = $this;

            $whereString = implode(' AND ', array_map(
                function ($field) use ($instance) {
                    return '`' . $field . '`=:' . $instance->bindVarName($field) . 'Where';
                },
                array_keys($whereArray)
            ));
            $sql = 'DELETE FROM `' . $table . '`  WHERE ' . $whereString;
            $stmt = $this->prepare($sql);
            foreach ($whereArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field) . 'Where', $value);
            }
            return $stmt->execute();
        } catch (Exception $e) {
            die($e . ' ' . $sql);
        }
    }

    public function select($table, $whereArray, $whereAndOr = 'AND')
    {
        $sql = '';
        try {
            $instance = $this;

            $whereString = implode(' ' . $whereAndOr . ' ', array_map(
                function ($field) use ($instance) {
                    return '`' . $field . '`=:' . $instance->bindVarName($field);
                },
                array_keys($whereArray)
            ));
            $sql = 'SELECT * FROM `' . $table . '` WHERE ' . $whereString;
            $stmt = $this->prepare($sql);
            foreach ($whereArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field), $value);
            }

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e . ' ' . $sql);
        }
    }

    public function exists($table, $whereArray)
    {
        $sql = '';
        try {
            $instance = $this;

            $whereString = implode(' AND ', array_map(
                function ($field) use ($instance) {
                    return '`' . $field . '`=:' . $instance->bindVarName($field) . 'Where';
                },
                array_keys($whereArray)
            ));
            $sql = 'SELECT COUNT(*) FROM `' . $table . '`  WHERE ' . $whereString;
            $stmt = $this->prepare($sql);
            foreach ($whereArray as $field => $value) {
                $stmt->bindValue(":" . $this->bindVarName($field) . 'Where', $value);
            }
            $stmt->execute();
            $record = $stmt->fetch(PDO::FETCH_NUM);
            return $record[0]>0;
        } catch (Exception $e) {
            die($e . ' ' . $sql);
        }
    }


}

?>