<?php

class EmailNotification
{

    static function sendEmailNotification($subject, $body, $toEmailArray, $debug = false)
    {
        require_once(__DIR__ . "/../lib/Mail/class.phpmailer.php");
        require_once(__DIR__ . "/../config/config.inc.php");
        $Mail = new PHPMailer();
        $Mail->SetLanguage("it", __DIR__ . "/../lib/Mail/language/");
        $Mail->From = FROM_EMAIL;
        $Mail->FromName = FROM_NAME;
        $Mail->IsSMTP();
        $Mail->SMTPDebug = $debug;
        $Mail->Host = SMTP;
        $Mail->Username = SMTP_USERNAME;
        $Mail->Password = SMTP_PASSWORD;
        $Mail->SMTPAuth = SMTP_AUTH;
        $Mail->SMTPSecure = false;
        $Mail->SMTPAutoTLS = true;
        $Mail->Subject = $subject;
        $Mail->Body = $body;
        $Mail->Body .= FROM_SIGNATURE;
        foreach ($toEmailArray as $toEmail)
            $Mail->AddAddress($toEmail);
        $Mail->Send();
        $Mail->ClearAddresses();
    }

    static function sendAutomaticStandardNotification($toEmailArray, $addMessage, $debug = false)
    {
        $subject = 'Notifica di operazione automatica eseguita';
        $body = "Buongiorno,
            
di seguito il messaggio collegato a questa notifica:

" . $addMessage . "

";
        self::sendEmailNotification($subject, $body, $toEmailArray);
    }

    public static function sendProvisoryNewPassword($mailTo, $userPassword, $passwordExpirationDays)
    {
        $subject = 'Nuova password provvisoria';
        $body = "Buongiorno,
	        
di seguito la nuova password provvisoria autogenerata da " . APPLICATION_NAME . "

Password: " . $userPassword . "

La password e' valida per ".($passwordExpirationDays==1 ? 'un giorno' : $passwordExpirationDays.' giorni')." e va aggiornata cliccando sul bottone \"Modifica\" dal pannello di autenticazione:

" . APPLICATION_URL . "

";
        self::sendEmailNotification($subject, $body, array($mailTo));

    }

}

?>
