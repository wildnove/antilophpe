<?php
/**
 * File Navbar.class.php
 *
 * This file contains the definition of the class object
 * type Navbar.
 * @author Dott. Marco Novo <mnovo@ncfsistemi.com>
 * @version 1.0
 */

/**
 * This class defines fields and methods of Navbar.
 * Methods of this class are usefull to
 * control Antilope's table pagination.
 */
class Navbar
{
    public $numeroRigheTotali;
    public $righePerPagina;
    public $paginePerBlocco;

    /**
     * Constructor of the class
     */
    function __construct($numeroRigheTotali, $righePerPagina = 10, $paginePerBlocco = 8)
    {
        $this->righePerPagina = $righePerPagina;
        $this->paginePerBlocco = $paginePerBlocco;
        $this->numeroRigheTotali = $numeroRigheTotali;
    }

    /**
     * Destructor of the class
     */
    function __destruct()
    {
    }

    /**
     * Gets the value of field righePerPagina
     * @return integer
     */
    function getRighePerPagina()
    {
        return $this->righePerPagina;
    }

    function makeNavBar($bloccoAttuale, $numeroPagina, $getRequest)
    {
        $navbar['css'] = '
<style type="text/css">

  .nav_link, .nav_label{
    font-family:sans-serif;
    font-size:12px;
  }

  #nav_box{
    text-align:center;
    margin-bottom:5px;
  }

  a.nav_link{
    background-color:#eeeeee;
    border:1px solid white;
    color:#555555;
    text-decoration:none;
  }

  a.nav_link:hover{
    background-color:white;
    border:1px outset black;
  }

  .nav_selected{
    font-weight:bold;
    color:white;
    background-color:#555555;
  }
</style>
    ';

        // Calcola il numero delle pagine totali
        if ($this->numeroRigheTotali % $this->righePerPagina != 0)
            $numero_pagine_totali = (int)($this->numeroRigheTotali / $this->righePerPagina) + 1;
        else
            $numero_pagine_totali = $this->numeroRigheTotali / $this->righePerPagina;

        $navbar['numero_pagine_totali'] = $numero_pagine_totali;

        // Calcola il numero di blocchi
        if ($numero_pagine_totali % $this->paginePerBlocco != 0)
            $blocco_finale = (int)($numero_pagine_totali / $this->paginePerBlocco);
        else
            $blocco_finale = ($numero_pagine_totali / $this->paginePerBlocco) - 1;
        $pagine_in_avanzo = $numero_pagine_totali % $this->paginePerBlocco;
        $navbar['barra'] = "\n<div id=\"nav_box\"><span class=\"nav_label\">&nbsp;&nbsp;RISULTATI :&nbsp;&nbsp;<b>" . $this->numeroRigheTotali . "</b>&nbsp;</span>\r";

        $bloccoAttuale = intval($bloccoAttuale);
        $blocco_attuale_inizio = $this->paginePerBlocco * $bloccoAttuale;
        $blocco_attuale_fine = ($this->paginePerBlocco * $bloccoAttuale) + $this->paginePerBlocco;

        if ($numeroPagina == "")
            $numeroPagina = 1 + $blocco_attuale_inizio;

        if ($blocco_attuale_fine > $numero_pagine_totali) {
            $blocco_attuale_fine = ($blocco_attuale_fine - $this->paginePerBlocco) + $pagine_in_avanzo;
            $bottone_avanti = 'disabled';
        }
        if ($blocco_attuale_fine == $numero_pagine_totali)
            $bottone_avanti = 'disabled';


        $navbar['start'] = ($numeroPagina - 1) * $this->righePerPagina;
        if ($bloccoAttuale != 0) {
            $navbar['barra'] .= "<a class=\"nav_link\" href=\"?" . $getRequest . "bloccoAttuale=0&numeroPagina=1\">&nbsp;&nbsp;PRIMA&nbsp;&nbsp;</a>\r";
            $navbar['barra'] .= "<a class=\"nav_link\" href=\"?" . $getRequest . "numeroPagina=" . (($bloccoAttuale) * $this->paginePerBlocco) . "&amp;bloccoAttuale=" . ($bloccoAttuale - 1) . "&\">&nbsp;&#171;&nbsp;</a>\r";
        }
        for ($i = $blocco_attuale_inizio + 1; $i <= $blocco_attuale_fine; $i++) {
            if ($numeroPagina == $i)
                $navbar['barra'] .= "<span class=\"nav_link nav_selected\">&nbsp;&nbsp;$i&nbsp;&nbsp;</span>\r";
            else
                $navbar['barra'] .= "<a class=\"nav_link\" href=\"?" . $getRequest . "numeroPagina=$i&amp;bloccoAttuale=$bloccoAttuale\">&nbsp;&nbsp;$i&nbsp;&nbsp;</a>\r";
        }
        if ($bottone_avanti != 'disabled') {
            $navbar['barra'] .= "<a class=\"nav_link\" href=\"?" . $getRequest . "numeroPagina=" . (1 + ($this->paginePerBlocco * ($bloccoAttuale + 1))) . "&amp;bloccoAttuale=" . ($bloccoAttuale + 1) . "\">&nbsp;&#187;&nbsp;</a>\r";
            $navbar['barra'] .= "<a class=\"nav_link\" href=\"?" . $getRequest . "numeroPagina=$numero_pagine_totali&amp;bloccoAttuale=$blocco_finale\">&nbsp;&nbsp;ULTIMA&nbsp;&nbsp;</a>\r";
        }
        $navbar['barra'] .= "<span class=\"nav_label\">&nbsp;&nbsp;PAGINE:&nbsp;&nbsp;<b>" . $numero_pagine_totali . "</b>&nbsp;</span></div>\n\n\r";

        return $navbar;
    }

}

?>
