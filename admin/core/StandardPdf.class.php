<?php
require_once("../lib/Pdf/fpdf.php");

class StandardPdf extends FPDF
{
    public $moduleLabel = '';
    public $row1 = '';
    public $row2 = '';
    public $row3 = '';
    public $logo = '../img/logo.png';

    function Header()
    {
        $this->Image($this->logo, 10, 2, 50);
        $this->SetFont('Arial', 'B', 14);
        $this->Cell(80);
        $this->Cell(200, 10, 'Esportazione ' . $this->moduleLabel . ' del ' . date('Y-m-d'), 1, 0, 'C');
        $this->Ln(20);
    }

    function Footer()
    {
        $this->SetY(-23);
        $width_footer = 275;
        $this->Image($this->logo, 10, 192, 30);

        $this->SetDrawColor(0, 0, 0);
        $this->SetLineWidth(0.6);
        $this->Cell($width_footer, 2, '', 'B', 1, 'L');

        $this->Cell(60, 10, '', 0, 0, 'L');

        $this->SetFont('Arial', 'B', 7);
        $this->Cell($width_footer - 60, 5, 'Esportazione ' . $this->moduleLabel . ' del ' . date('Y-m-d') . '           Pagina ' . $this->PageNo() . '/{nb}', 0, 2, 'R');
        $this->SetFont('Arial', '', 6);
        $this->Cell($width_footer - 60, 2, $this->row1, 0, 2, 'L');
        $this->Cell($width_footer - 60, 2, $this->row2, 0, 2, 'L');
        $this->Cell($width_footer - 60, 2, $this->row3, 0, 2, 'L');

        $this->SetTextColor(150);
        $this->SetFont('Arial', 'B', 7);
        $this->Cell($width_footer - 60, 3, "Antilophpe 3 - www.ncfsistemi.com", 0, 2, 'L');
    }
}

?>