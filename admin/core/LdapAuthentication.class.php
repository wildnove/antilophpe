<?php

class LdapAuthentication
{

    private $ldap_hostname, $ldap_port, $ldap_base_dn, $ldap_user_dn, $ldap_admin_user, $ldap_admin_password, $debug;

    function __construct($ldap_hostname, $ldap_port, $ldap_base_dn, $ldap_user_dn, 
        $ldap_admin_user, $ldap_admin_password, $ldap_id_attr, $debug = false)
    {
        $this->ldap_hostname = $ldap_hostname;
        $this->ldap_port = $ldap_port;
        $this->ldap_base_dn = $ldap_base_dn;
        $this->ldap_user_dn = $ldap_user_dn;
        $this->ldap_admin_user = $ldap_admin_user;
        $this->ldap_admin_password = $ldap_admin_password;
        $this->ldap_id_attr = $ldap_id_attr;

        $this->debug = $debug;
    }

    function __destruct()
    {
    }

    function find_user($ldapconn, $ldapdn, $username) {
        if(is_null($this->ldap_admin_user))
            return null;        

        if(!$ldapconn)
            throw new Exception("Cannot connect to LDAP server", 500);

        if(empty($ldapdn))
            throw new Exception("Missing admin DN for binding", 500);

        $ldapbind = @ldap_bind($ldapconn, $ldapdn, 
            $this->ldap_admin_password);

        if ($this->debug)
            echo "LDAP admin bind successful...<br/>";

        if(!$ldapbind) {
            ldap_unbind($ldapconn);
            throw new Exception("Cannot bind with this admin user", 500);
        }   

        $result = ldap_search($ldapconn, $this->ldap_base_dn, 
            "(uid=" . $username . ")");
        if ($this->debug)
            echo ldap_error($ldapconn) . '<br/>';
        $info = ldap_get_entries($ldapconn, $result);  

        if ($info['count'] == 0)
            return null;

        if ($info['count'] > 2)
            throw new Exception("Multiple user found with same id", 500);

        return $info[0]; 
    }


    function ldap_connect_and_authenticate_user($username, $userpass)
    {
        // The common way to authenticate is to get the users name, use search and perhaps selection to the user to get her DN (single value)
        // then attempt to BIND to the ldapserver using that dn and the offered password.  If it works, then it's the right password.
        // Note that the password offered MUST NOT BE EMPTY or many LDAPs will presume you meant to authenticate anonymously and it will succeed,
        // leaving you thinking it's the right password.

        $ldapconn = @ldap_connect($this->ldap_hostname, $this->ldap_port);
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

        if ($ldapconn) {
           if ($this->debug)
                echo '$ldapconn: ' . $ldapconn . '<hr/>';

            $user_dn = sprintf("%s=%s,%s,%s", $this->ldap_id_attr, $username, 
                $this->ldap_base_dn, $this->ldap_base_dn);

            // find user 
            $ldapdn = null;
            if(!is_null($this->ldap_admin_user))
                $ldapdn = sprintf("%s=%s,%s", $this->ldap_id_attr, 
                    $this->ldap_admin_user, $this->ldap_base_dn);

            $user = $this->find_user($ldapconn, $ldapdn, $username);
            if(!is_null($user)) 
                $user_dn = $user['dn'];

            // Test user password in LDAP server
            $user_ldapbind = @ldap_bind($ldapconn, $user_dn, $userpass);
            if ($user_ldapbind) {
                if ($this->debug)
                    echo "LDAP user bind successful...<br/>";
                ldap_unbind($ldapconn);
                return true;
            } else {
                ldap_unbind($ldapconn);
                return false;
            }
            return false;            
        } else
            return false;
    }
}