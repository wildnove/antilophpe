<?php

class SessionController
{
    
    function __construct()
    {
        session_start();
        if ($_SESSION['User']['id'] == '') {
            echo '
<html>
    <head><title>Session time out</title></head>
    <body>
        <script>top.location.href=\'_LoginOperations.php?operation=logout&message=timeout\'</script>
    </body>
</html>';
            exit;
        }
    }

    function __destruct()
    {
    }
}

?>