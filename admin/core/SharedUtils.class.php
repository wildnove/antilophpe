<?php

class SharedUtils
{
    static function isInternalIp($ip)
    {
        $reserved_ips = array(          // Not an exhaustive list
            '167772160' => 184549375,   //    10.0.0.0 -  10.255.255.255
            '3232235520' => 3232301055, // 192.168.0.0 - 192.168.255.255
            '2130706432' => 2147483647, //   127.0.0.0 - 127.255.255.255
            '2851995648' => 2852061183, // 169.254.0.0 - 169.254.255.255
            '2886729728' => 2887778303, //  172.16.0.0 -  172.31.255.255
            '3758096384' => 4026531839, //   224.0.0.0 - 239.255.255.255
        );

        $ip_long = sprintf('%u', ip2long($ip));

        foreach ($reserved_ips as $ip_start => $ip_end) {
            if (($ip_long >= $ip_start) && ($ip_long <= $ip_end)) {
                return true;
            }
        }
        return false;
    }


    static function computePasswordExpirationDays($passwordExpirationDate)
    {
        return round((strtotime($passwordExpirationDate) - strtotime(date("Y-m-d"))) / (60 * 60 * 24));
    }


    static function fromExcelToLinux($excel_time)
    {
        return ($excel_time - 25569) * 86400;
    }

    static function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

}

?>
