<?php
require_once(__DIR__ . '/../config/config.inc.php');
require_once(__DIR__ . '/../models/Module.class.php');
require_once(__DIR__ . '/../models/File.class.php');
require_once(__DIR__ . '/../models/Log.class.php');

class ModuleHelper
{
    protected $DatabaseHelper;
    protected $Module;
    protected $File;
    protected $Object;
    protected $objectName;
    protected $tableName;
    protected $moduleDescription;

    function __construct($DatabaseHelper, $moduleId, $Object)
    {
        $this->DatabaseHelper = $DatabaseHelper;
        $Module = new Module($this->DatabaseHelper);
        $Module->loadById($moduleId);
        $this->Module = $Module;
        $this->File = new File($this->DatabaseHelper);
        $this->Object = $Object;
        $this->objectName = $this->Module->getName();
        $this->tableName = TBPX . $this->objectName;
        $this->moduleDescription = self::addDefaultFieldsToDescription(json_decode($Module->getJsonDescription(), true));
    }

    function getModuleDescription()
    {
        return $this->moduleDescription;
    }

    function setModuleDescription($moduleDescription)
    {
        $this->moduleDescription = $moduleDescription;
    }

    static function addDefaultFieldsToDescription($moduleDescription)
    {
        if (isset($moduleDescription['fields']['id']) ||
            isset($moduleDescription['fields']['coverFile']) ||
            isset($moduleDescription['fields']['lastDateTime']) ||
            isset($moduleDescription['fields']['user']) ||
            isset($moduleDescription['fields']['active']) ||
            isset($moduleDescription['fields']['trashed'])
        )
            return false;

        // Antilophpe fix remove + for compatibility
        $moduleDescription['fields'] = array_merge($moduleDescription['fields'], array(
            "id" => array(
                "label" => 'Id',
                "type" => 'integer'
            ),
            "coverFile" => array(
                "label" => 'File cover',
                "type" => 'object',
                "typeoptions" => array(
                    "name" => "File",
                    "field" => "title"
                )
            ),
            "lastDateTime" => array(
                "label" => 'Data mod',
                "type" => 'datetime'),
            "user" => array(
                "label" => 'Utente mod',
                "type" => 'object',
                "typeoptions" => array(
                    "name" => "User",
                    "field" => "userName"
                )
            ),
            "active" => array(
                "label" => 'Attivo',
                "type" => 'flag'),
            "trashed" => array(
                "label" => 'Cestinato',
                "type" => 'flag',
                "infilter" => false,
                "intable" => false)
        ));
        return $moduleDescription;
    }

    function insertLog($type, $log)
    {
        $logArray['type'] = $type;
        $logArray['log'] = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $log)));
        $logArray['ip'] = $_SERVER['REMOTE_ADDR'];
        $logArray['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $logArray['lastDateTime'] = date("Y-m-d H:i:s");
        $logArray['user'] = $_SESSION['User']['id'];
        $this->DatabaseHelper->insert(TBPX . 'Log', $logArray);
    }

    function filterOrderToSession($request)
    {
        foreach ($this->moduleDescription['fields'] as $name => $field) {
            $field['name'] = $name;

            if ($request['suboperation'] == 'addToFilter' &&
                (
                    ($field['type'] != 'date' && !isset($request['Filter' . $this->objectName . '__' . $field['name']]))
                    ||
                    ($field['type'] == 'date' && !isset($request['Filter' . $this->objectName . '__' . $field['name'] . '_ADF']))
                )
            )
                continue;

            if (is_bool($field['infilter']) && $field['infilter'] === false)
                continue;
            if ($field['type'] == 'object') {
                $foreignTable = TBPX . $field['typeoptions']['name'];
                $_SESSION['Filter' . $this->objectName][$foreignTable . '_' . $field['name'] . '.' . $field['typeoptions']['field']] = $request['Filter' . $this->objectName . '__' . $field['name']];
                $_SESSION['Filter' . $this->objectName][$foreignTable . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '_ALO'] = $request['Filter' . $this->objectName . '__' . $field['name'] . '_ALO'];

                $_SESSION['Filter' . $this->objectName][$foreignTable . '_' . $field['name'] . '.id'] = $request['Filter' . $this->objectName . '__' . $field['name'] . 'Id'];
                $_SESSION['Filter' . $this->objectName][$foreignTable . '_' . $field['name'] . '.id_ALO'] = $request['Filter' . $this->objectName . '__' . $field['name'] . 'Id_ALO'];
            } else if ($field['type'] == 'decimal') {
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name']] = str_replace(",", ".", $request['Filter' . $this->objectName . '__' . $field['name']]);
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] = $request['Filter' . $this->objectName . '__' . $field['name'] . '_ALO'];
            } else if ($field['type'] == 'date' || $field['type'] == 'datetime') {
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ADF'] = $request['Filter' . $this->objectName . '__' . $field['name'] . '_ADF'];
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ADT'] = $request['Filter' . $this->objectName . '__' . $field['name'] . '_ADT'];
            } else { // select, string or text
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name']] = $request['Filter' . $this->objectName . '__' . $field['name']];
                $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] = $request['Filter' . $this->objectName . '__' . $field['name'] . '_ALO'];
            }
        }

        $_SESSION['Navbar' . $this->objectName]['bloccoAttuale'] = '';
        $_SESSION['Navbar' . $this->objectName]['numeroPagina'] = '';

        if ($request['suboperation'] == 'addToFilter')
            return;

        $_SESSION['Order' . $this->objectName]['field1'] = $request['Order' . $this->objectName . '__field1'];
        $_SESSION['Order' . $this->objectName]['field1_direction'] = $request['Order' . $this->objectName . '__field1_direction'];
        $_SESSION['Order' . $this->objectName]['field2'] = $request['Order' . $this->objectName . '__field2'];
        $_SESSION['Order' . $this->objectName]['field2_direction'] = $request['Order' . $this->objectName . '__field2_direction'];
        $_SESSION['Order' . $this->objectName]['field3'] = $request['Order' . $this->objectName . '__field3'];
        $_SESSION['Order' . $this->objectName]['field3_direction'] = $request['Order' . $this->objectName . '__field3_direction'];
        if ($request['Order' . $this->objectName . '__field1'] == '' && $request['Order' . $this->objectName . '__field2'] == '' && $request['Order' . $this->objectName . '__field3'] == '')
            unset($_SESSION['Order' . $this->objectName]);
    }

    function orderToSession($request)
    {
        if ($_SESSION['Order' . $this->objectName]['field1'] == $request['Order' . $this->objectName . '__field1']) {
            if ($_SESSION['Order' . $this->objectName]['field1_direction'] == '') $_SESSION['Order' . $this->objectName]['field1_direction'] = 'DESC';
            else $_SESSION['Order' . $this->objectName]['field1_direction'] = '';
        } else {
            $_SESSION['Order' . $this->objectName]['field1'] = $request['Order' . $this->objectName . '__field1'];
            $_SESSION['Order' . $this->objectName]['field1_direction'] = $request['Order' . $this->objectName . '__field1_direction'];
        }
        if ($_SESSION['Order' . $this->objectName]['field2'] == $request['Order' . $this->objectName . '__field2']) {
            if ($_SESSION['Order' . $this->objectName]['field2_direction'] == '') $_SESSION['Order' . $this->objectName]['field2_direction'] = 'DESC';
            else $_SESSION['Order' . $this->objectName]['field2_direction'] = '';
        } else {
            $_SESSION['Order' . $this->objectName]['field2'] = $request['Order' . $this->objectName . '__field2'];
            $_SESSION['Order' . $this->objectName]['field2_direction'] = $request['Order' . $this->objectName . '__field2_direction'];
        }
        if ($_SESSION['Order' . $this->objectName]['field3'] == $request['Order' . $this->objectName . '__field3']) {
            if ($_SESSION['Order' . $this->objectName]['field3_direction'] == '') $_SESSION['Order' . $this->objectName]['field3_direction'] = 'DESC';
            else $_SESSION['Order' . $this->objectName]['field3_direction'] = '';
        } else {
            $_SESSION['Order' . $this->objectName]['field3'] = $request['Order' . $this->objectName . '__field3'];
            $_SESSION['Order' . $this->objectName]['field3_direction'] = $request['Order' . $this->objectName . '__field3_direction'];
        }
        if (!$request['Order' . $this->objectName . '__field1'] && ($request['Order' . $this->objectName . '__field2'] || $request['Order' . $this->objectName . '__field3'])) {
            $_SESSION['Order' . $this->objectName]['field1'] = '';
            $_SESSION['Order' . $this->objectName]['field1_direction'] = '';
        }
        if (!$request['Order' . $this->objectName . '__field2'] && ($request['Order' . $this->objectName . '__field1'] || $request['Order' . $this->objectName . '__field3'])) {
            $_SESSION['Order' . $this->objectName]['field2'] = '';
            $_SESSION['Order' . $this->objectName]['field2_direction'] = '';
        }
        if (!$request['Order' . $this->objectName . '__field3'] && ($request['Order' . $this->objectName . '__field1'] || $request['Order' . $this->objectName . '__field2'])) {
            $_SESSION['Order' . $this->objectName]['field3'] = '';
            $_SESSION['Order' . $this->objectName]['field3_direction'] = '';
        }
    }

    function echoDefaultFilterForField($field)
    {
        $Numeric_ALO_array = array(
            "uguale a" => "LIKE",
            "diverso da" => "NOT LIKE",
            "maggiore di" => ">",
            "minore di" => "<",
            "maggiore o uguale a" => ">=",
            "minore o uguale a" => "<="
        );
        $String_ALO_array = array(
            "uguale a" => "LIKE",
            "diverso da" => "NOT LIKE"
        );
        $Flag_ALO_array = array(
            "SI" => "=",
            "NO" => "!="
        );
        $Empty_ALO_array = array(
            "SI" => "IS NULL",
            "NO" => "IS NOT NULL"
        );

        if (is_bool($field['infilter']) && $field['infilter'] === false)
            echo '';
        else if ($field['type'] == 'integer' || $field['type'] == 'decimal') {
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '">' . $field['label'] . '&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . '_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">
            <option value="">Scegli...</option>';
            foreach ($Numeric_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>
        </td>
        <td><input onKeyUp="if (this.value!=\'\' && $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val()==\'\') $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val(\'LIKE\');" class="input_text" type="text" value="' . htmlspecialchars($_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name']]) . '" id="Filter' . $this->objectName . '__' . $field['name'] . '" name="Filter' . $this->objectName . '__' . $field['name'] . '"/></td>
      </tr>';
        } else if ($field['type'] == 'object') {
            $foreignTable = TBPX . $field['typeoptions']['name'];
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '">' . $field['label'] . '&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . '_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">
            <option value="">Scegli...</option>';
            foreach ($String_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName]['' . $foreignTable . '_' . $field['name'] . '.' . $field['typeoptions']['field'] . '_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>
        </td>
        <td><input class="input_text" onKeyUp="if (this.value!=\'\' && $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val()==\'\') $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val(\'LIKE\');" type="text" value="' . htmlspecialchars($_SESSION['Filter' . $this->objectName]['' . $foreignTable . '_' . $field['name'] . '.' . $field['typeoptions']['field']]) . '" id="Filter' . $this->objectName . '__' . $field['name'] . '" name="Filter' . $this->objectName . '__' . $field['name'] . '"/></td>
      </tr>';


            $foreignTable = TBPX . $field['typeoptions']['name'];
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . 'Id">' . $field['label'] . ' (id)&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . 'Id_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . 'Id_ALO">
            <option value="">Scegli...</option>';
            foreach ($String_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName]['' . $foreignTable . '_' . $field['name'] . '.id_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>
        </td>
        <td><input class="input_text" onKeyUp="if (this.value!=\'\' && $(\'#Filter' . $this->objectName . '__' . $field['name'] . 'Id_ALO\').val()==\'\') $(\'#Filter' . $this->objectName . '__' . $field['name'] . 'Id_ALO\').val(\'LIKE\');" type="text" value="' . htmlspecialchars($_SESSION['Filter' . $this->objectName]['' . $foreignTable . '_' . $field['name'] . '.id']) . '" id="Filter' . $this->objectName . '__' . $field['name'] . 'Id" name="Filter' . $this->objectName . '__' . $field['name'] . 'Id"/></td>
      </tr>';


        } else if ($field['type'] == 'flag') {
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">' . $field['label'] . '&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . '_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">
            <option value="">Scegli...</option>';
            foreach ($Flag_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>             
        </td>
        <td>
          <input type="hidden" name="Filter' . $this->objectName . '__' . $field['name'] . '" id="Filter' . $this->objectName . '__' . $field['name'] . '" value="1"/>
          &nbsp;
        </td>
      </tr>';
        } else if ($field['type'] == 'empty') {
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">' . $field['label'] . '&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . '_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">
            <option value="">Scegli...</option>';
            foreach ($Empty_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>             
        </td>
        <td>
          <input type="hidden" name="Filter' . $this->objectName . '__' . $field['name'] . '" id="Filter' . $this->objectName . '__' . $field['name'] . '" value=""/>
          &nbsp;
        </td>
      </tr>';
        } else if ($field['type'] == 'date' || $field['type'] == 'datetime') {
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '_ADF">' . $field['label'] . ' (da/a)&nbsp;</label></td>
        <td class="no_wrap">
          <input type="text" class="input_text dateRange" name="Filter' . $this->objectName . '__' . $field['name'] . '_ADF" id="Filter' . $this->objectName . '__' . $field['name'] . '_ADF" max_length="12" size="12" value="' . $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ADF'] . '"/>
        </td>
        <td class="no_wrap">
          <input type="text" class="input_text dateRange" name="Filter' . $this->objectName . '__' . $field['name'] . '_ADT" id="Filter' . $this->objectName . '__' . $field['name'] . '_ADT" max_length="12" size="12" value="' . $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ADT'] . '"/>
        </td>
      </tr>';
        } else { // text or string or select
            echo '
      <tr>
        <td><label for="Filter' . $this->objectName . '__' . $field['name'] . '">' . $field['label'] . '&nbsp;</label></td>
        <td>
          <select id="Filter' . $this->objectName . '__' . $field['name'] . '_ALO" name="Filter' . $this->objectName . '__' . $field['name'] . '_ALO">
            <option value="">Scegli...</option>';
            foreach ($String_ALO_array as $key => $value) {
                echo '
            <option ' . ($value == $_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name'] . '_ALO'] ? 'selected="selected"' : '') . ' value="' . $value . '">' . $key . '</option>';
            }
            echo '
          </select>
        </td>
        <td><input class="input_text" onKeyUp="if (this.value!=\'\' && $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val()==\'\') $(\'#Filter' . $this->objectName . '__' . $field['name'] . '_ALO\').val(\'LIKE\');" type="text" value="' . htmlspecialchars($_SESSION['Filter' . $this->objectName][$this->tableName . '.' . $field['name']]) . '" id="Filter' . $this->objectName . '__' . $field['name'] . '" name="Filter' . $this->objectName . '__' . $field['name'] . '"/></td>
      </tr>';
        }
    }

    function makeWhereAndOrder()
    {
        $where = "
    WHERE `" . $this->tableName . "`.`trashed`=" . ($_SESSION['ViewTrashed' . $this->objectName] == 1 ? 1 : 0);
        if (is_array($_SESSION['Filter' . $this->objectName])) {
            foreach ($_SESSION['Filter' . $this->objectName] as $key => $value) {
                if (strpos($key, '_ALO') !== false && $value != '') {
                    $fieldName = str_replace('_ALO', '', $key);
                    $fieldValue = $_SESSION['Filter' . $this->objectName][$fieldName];
                    $operator = $value;
                    if (strpos($operator, 'NULL') === false)
                        $fieldValue = $this->DatabaseHelper->pdo->quote($fieldValue);
                } else if (strpos($key, '_ADF') !== false && $value != '') {
                    $fieldName = str_replace('_ADF', '', $key);
                    $fieldValue = date("Y-m-d", strtotime(str_replace('/', '-', $value)));
                    $fieldValue = str_replace(':' . $this->DatabaseHelper->bindVarName($fieldName), "'" . $fieldValue . "'", $this->DatabaseHelper->sqlBindFormatter($fieldName, $fieldValue));
                    $operator = '>=';
                } else if (strpos($key, '_ADT') !== false && $value != '') {
                    $fieldName = str_replace('_ADT', '', $key);
                    $fieldValue = date("Y-m-d", strtotime(str_replace('/', '-', $value)));
                    $fieldValue = str_replace(':' . $this->DatabaseHelper->bindVarName($fieldName), "'" . $fieldValue . "'", $this->DatabaseHelper->sqlBindFormatter($fieldName, $fieldValue));
                    $operator = '<=';
                } else {
                    continue;
                }
                $dbFieldName = str_replace(".", "`.`", $fieldName);
                $where .= "
    AND `$dbFieldName` $operator $fieldValue"; //@TODO: bind param!
            }
        }
        $orderByFields = array();
        $orderBy = '';
        if (is_array($_SESSION['Order' . $this->objectName])) {
            foreach ($_SESSION['Order' . $this->objectName] as $key => $value) {
                if (strpos($key, '_direction') === false && $value != '') {
                    $dbvalue = str_replace(".", "`.`", $value);
                    $orderBy .= " `$dbvalue` " . $_SESSION['Order' . $this->objectName][$key . '_direction'] . ",";
                    $orderByFields[$value] = $_SESSION['Order' . $this->objectName][$key . '_direction'] == 'DESC' ? 'DESC' : 'ASC';
                }
            }
        }
        if ($orderBy)
            $orderBy = '
    ORDER BY ' . rtrim($orderBy, ',') . ($this->DatabaseHelper->driver == 'oci' ? ',rownum' : '');
        return array($where, $orderBy, $orderByFields);
    }

    function limit($sql, $offset = 0, $limit = 45)
    {
        return $this->DatabaseHelper->limit($sql,
            $offset, $limit);
    }

    function autocomplete($type, $field, $term)
    {
        $items = array();
        if ($term == '@suggest@') {
            if ($type == 'file') {
                $Module = new Module($this->DatabaseHelper);
                $Module->loadById(8);
                $fileModuleDescription = self::addDefaultFieldsToDescription(json_decode($Module->getJsonDescription(), true));
                $values = $fileModuleDescription['fields'][$field]['typeoptions']['values'];
            } else
                $values = $this->moduleDescription['fields'][$field]['typeoptions']['values'];
            foreach ($values as $id => $value) {
                $row['id'] = $id;
                $row['field'] = $field;
                $row['label'] = $value;
                $row['value'] = $value;
                $items[] = $row;
            }
        } else {
            $sql = "SELECT max(`id`) AS `id`, `" . $field . "` AS `field`
                FROM `" . ($type == 'file' ? TBPX . 'File' : $this->tableName) . "`
                WHERE `trashed` = 0
                AND `" . $field . "` LIKE " . $this->DatabaseHelper->pdo->quote("%" . trim($term) . "%") . "
                " . ($type == 'file' ? "AND `objectName` = '" . $this->objectName . "'" : '') . "
                GROUP BY `id`, `" . $field . "`";
            $stmt = $this->DatabaseHelper->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $row['label'] = $row['field'];
                $row['value'] = $row['label'];
                $items[] = $row;
            }
        }
        return json_encode($items);
    }


    function getField($type, $field, $id)
    {
        $sql = "SELECT `" . $field . "`
                FROM `" . ($type == 'file' ? TBPX . 'File`' : $this->tableName) . "`
                WHERE `id`=" . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    function getDetailsHtml($id)
    {
        $returnHtml = '';
        if (!$id)
            return $returnHtml;
        $sql = $this->Object->getDefaultSelectSql($countSql = false);
        $sql .= " WHERE `" . $this->tableName . "`.`id`=" . $id;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row['id'])
            return $returnHtml;
        foreach ($this->moduleDescription['fields'] as $name => $field) {
            if ($name == 'coverFile')
                continue;
            $field['name'] = $name;
            if ($field['type'] == 'date')
                $row[$field['name']] = date("d/m/Y", strtotime($row[$field['name']]));
            if ($field['type'] == 'object')
                $row[$field['name']] = $row[$field['name'] . '_' . $field['typeoptions']['field']] . ' <i class="font_weight_normal">(ID: ' . $row[$field['name']] . ')</i>';
            if ($field['type'] == 'flag')
                $row[$field['name']] = $row[$field['name']] ? 'SI' : 'NO';
            if ($field['type'] == 'datetime')
                $row[$field['name']] = date("d/m/Y H:i:s", strtotime($row[$field['name']]));
            $returnHtml .= $row[$field['name']] ? '<div><i>' . $field['label'] . ': </i><b>' . $row[$field['name']] . '</b></div>' : '';
        }
        return $returnHtml;
    }

    function getLogHtml($id)
    {
        $returnHtml = '';
        if (!$id)
            return $returnHtml;
        $Log = new Log($this->DatabaseHelper);
        $Log_array = $Log->getAsArrayById($id);
        $objectName = Log::getObjectNameByType($Log_array['type']);
        $logModuleDescription = array();
        if ($objectName) {
            $ObjectLogModule = new Module($this->DatabaseHelper);
            $ObjectLogModule->loadById($ObjectLogModule->getIdByName($objectName));
            $logModuleDescription = self::addDefaultFieldsToDescription(json_decode($ObjectLogModule->getJsonDescription(), true));
        }

        $value_array = json_decode($Log_array['log'], 1);
        if (strpos($Log_array['type'], 'stmt insert ') !== false || strpos($Log_array['type'], 'stmt update ') !== false) {
            if (strpos($Log_array['type'], 'stmt update ') !== false) {
                $PrevLog_array = $Log->getPrevAsArrayByType($id, $Log_array['type']);
                $prevValue_array = json_decode($PrevLog_array['log'], 1);
            }
        }
        $returnHtml .= '<div>In <i class="colorred">rosso</i> i campi modificati (rispetto al precedente log)</div><br/>';
        foreach ($value_array as $key => $item) {
            $returnHtml .= '<div class="no_wrap">' . ($logModuleDescription['fields'][$key]['label'] ? $logModuleDescription['fields'][$key]['label'] : $key) . ': <b ' . ($item != $prevValue_array[$key] && $prevValue_array[$key] ? 'class="colorred"' : '') . '>' . $item . '</b></div>';
        }
        return $returnHtml;
    }

    function listFieldValues($type, $field)
    {
        $sql = "SELECT `" . $field . "`
                FROM `" . ($type == 'file' ? TBPX . 'File' : $this->tableName) . "`
                WHERE `trashed` = 0
                AND `" . $field . "` != ''
                GROUP BY `" . $field . "`
                ORDER BY `" . $field . "`";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $items = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $items[] = $row[$field];
        }
        return $items;
    }

    function getImageHtml($type, $field, $id, $secretKey='')
    {
        if ($type == 'file' && $field == 'thumbnail') {
            if (!$secretKey){
                $sql = "SELECT `secretKey`
                FROM `" . TBPX . 'File'. "`
                WHERE `id` = $id";
                $stmt = $this->DatabaseHelper->prepare($sql);
                $stmt->execute();
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $secretKey = $row['secretKey'];
            }
            $thumbnailPath = UPLOADED_DIR . '/thumbnail/' . $id . '_' . $secretKey . '.jpg';
            if (file_exists($thumbnailPath)) {
                list($width, $height) = getimagesize($thumbnailPath);
                return '<a href="_Download.php?File__id=' . $id . '" class="vertical_middle" title="Download file originale"><img width="' . $width . '" height="' . $height . '" class="file-thumbnail" src="' . $thumbnailPath . '" alt="thumb"/></a>';
            } else {
                $thumbnailPath = '../img/no_image_available.jpg';
                list($width, $height) = getimagesize($thumbnailPath);
                return '<img width="' . $width . '" height="' . $height . '" class="file-thumbnail" src="' . $thumbnailPath . '" alt="thumb"/>';
            }
        } else
            return '';
    }


    function insert($arrayObject, $exceptionDie = true)
    {
        // if (!$_SESSION['Mask'][$this->Module->getId()]['ACTIVATE'])
        //    $arrayObject[$this->objectName . '__active'] = 0;

        $arrayObject[$this->objectName . '__lastDateTime'] = date("Y-m-d H:i:s");
        $arrayObject[$this->objectName . '__user'] = $_SESSION['User']['id'];
        $arrayObject = $this->DatabaseHelper->cleanFieldsArray($this->objectName, $arrayObject);
        $id = $this->DatabaseHelper->insert($this->tableName, $arrayObject, $exceptionDie);
        // if (!$result)
        //    return false;
        //$id = $this->DatabaseHelper->pdo->lastInsertId();
        $this->insertLog('stmt insert ' . $this->tableName . ' id=' . $id, json_encode($arrayObject));
        return $id;
    }


    function update($arrayObject)
    {
        $arrayObject[$this->objectName . '__lastDateTime'] = date("Y-m-d H:i:s");
        $arrayObject[$this->objectName . '__user'] = $_SESSION['User']['id'];
        $id = $arrayObject[$this->objectName . '__id'];
        unset($arrayObject[$this->objectName . '__id']);
        $arrayObject = $this->DatabaseHelper->cleanFieldsArray($this->objectName, $arrayObject);
        $result = $this->DatabaseHelper->update($this->tableName, $arrayObject, array('id' => $id));
        $this->insertLog('stmt update ' . $this->tableName . ' id=' . $id, json_encode($arrayObject));
        return $id;
    }


    function delete($id)
    {
        $this->DatabaseHelper->delete($this->tableName, array('id' => $id));
        $this->insertLog('stmt delete ' . $this->tableName . ' id=' . $id, json_encode(array('id' => $id)));
        return $id;
    }

    function getDatabaseError()
    {
        return $this->DatabaseHelper->error;
    }

    function outputExcel()
    {
        set_time_limit(10000);
        ini_set('memory_limit', '1000M');
        require_once dirname(__FILE__) . '/../lib/Excel/Classes/PHPExcel.php';
        require_once dirname(__FILE__) . '/../lib/Excel/Classes/PHPExcel/IOFactory.php';

        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array('memoryCacheSize' => '16MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $sheetPHPExcel = $objPHPExcel->getActiveSheet();

        $sql = $this->Object->getDefaultSelectSql($countSql = false);
        list($where, $orderBy, $orderFields) = $this->makeWhereAndOrder();
        $sql .= $where;
        $sql .= $orderBy;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $rowCounter = 1;
        $keys = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($rowCounter == 1) {
                $keys = array_keys($row);
                $tableHeadCells = array();
                foreach ($keys as $key) {
                    if ($key == 'coverFile')
                        break;
                    $tableHeadCells[] = $this->moduleDescription['fields'][$key]['label'];
                }
                $styleArray = array('font' => array('size' => '10', 'bold' => true, 'color' => array('rgb' => 'FFFFFF')), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER), 'fill' => array('type' => PHPExcel_Style_Fill::FILL_PATTERN_GRAY125));
                $colCounter = 0;
                foreach ($tableHeadCells as $tableHeadCell) {
                    $column = PHPExcel_Cell::stringFromColumnIndex($colCounter++);
                    $cellCoord = $column . $rowCounter;
                    $sheetPHPExcel->setCellValue($cellCoord, $tableHeadCell);
                    $sheetPHPExcel->getStyle($cellCoord)->applyFromArray($styleArray);
                }
                $sheetPHPExcel->getRowDimension($rowCounter)->setRowHeight(-1);
                PHPExcel_Cell::setValueBinder(new PHPExcel_Cell_AdvancedValueBinder());

            }
            $rowCounter++;
            $sheetPHPExcel->getRowDimension($rowCounter)->setRowHeight(25);
            $colCounter = 0;
            foreach ($row as $key => $value) {
                if ($key == 'coverFile')
                    break;
                if ($this->moduleDescription['fields'][$key]['type'] == 'object')
                    $value = $row[$key . '_' . $this->moduleDescription['fields'][$key]['typeoptions']['field']];
                // Le due righe qui sotto non funzionano perchè formattano come stringa!!!
//                if ($this->moduleDescription['fields'][$key]['type'] == 'decimal')
//                    $value = number_format($value,2,'.','');

                $column = PHPExcel_Cell::stringFromColumnIndex($colCounter++);
                $cellCoord = $column . $rowCounter;
                $styleArray = array('font' => array('size' => '10'), 'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER));
                $value = html_entity_decode($value);
                $sheetPHPExcel->setCellValue($cellCoord, $value);
                $sheetPHPExcel->getStyle($cellCoord)->applyFromArray($styleArray);
                $sheetPHPExcel->getColumnDimension($column)->setAutoSize(true);
            }
            $sheetPHPExcel->getRowDimension($rowCounter)->setRowHeight(-1);
            PHPExcel_Cell::setValueBinder(new PHPExcel_Cell_AdvancedValueBinder());
        }

        date_default_timezone_set('Europe/Rome');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $filename = date('Y-m-d') . "_" . str_replace(" ", "_", $this->Module->getLabel()) . ".xls";
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        header("Content-Disposition: attachment;filename=" . $filename);
        header("Content-Transfer-Encoding: binary ");
        $objWriter->save('php://output');

    }


    function outputCsv()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: 0");
        header('Content-Type: text/csv; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . date('Y-m-d') . "_" . str_replace(" ", "_", $this->Module->getLabel()) . ".csv");
        header("Pragma: no-cache");

        $out = fopen('php://output', 'w');
        $sql = $this->Object->getDefaultSelectSql($countSql = false);
        list($where, $orderBy, $orderFields) = $this->makeWhereAndOrder();
        $sql .= $where;
        $sql .= $orderBy;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $counter = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $counter++;
            if ($counter == 1) {
                $keys = array_keys($row);
                $tableHeader = array();
                foreach ($keys as $key) {
                    if ($key == 'coverFile')
                        break;
                    $tableHeader[] = $this->moduleDescription['fields'][$key]['label'];
                }
                fputcsv($out, $tableHeader);
            }
            $tableBodyRow = array();
            foreach ($row as $key => $value) {
                if ($key == 'coverFile')
                    break;
                if ($this->moduleDescription['fields'][$key]['type'] == 'object')
                    $value = $row[$key . '_' . $this->moduleDescription['fields'][$key]['typeoptions']['field']];
                $tableBodyRow[] = $value;
            }
            fputcsv($out, $tableBodyRow);
        }
        fclose($out);
    }


    function outputPdfNote($type)
    {
        require_once("CustomPdf.class.php");
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Comune di Padova');

        $pdf->setHeaderTemplateAutoreset(true);
        $pdf->startPageGroup();

        $label = $this->Module->getLabel();


        $sql = $this->Object->getDefaultSelectSql(true);
        $sql .= $whereOrder;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $record = $stmt->fetch(PDO::FETCH_NUM);
        $tot = $record[0];

        $testoHeader = " . strtoupper($label) . " (TOT: " . $tot . ")";
        $pdf->SetHeaderData('../../../img/logo.jpg', 60, 'Test', $testoHeader, array(0, 0, 0), array(0, 0, 0));

        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, 12);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $l = array();
        $l['a_meta_charset'] = 'UTF-8';
        $l['a_meta_dir'] = 'ltr';
        $l['a_meta_language'] = 'it';
        $l['w_page'] = 'pagina';
        $pdf->setLanguageArray($l);

        $pdf->setFontSubsetting(true);

        $pdf->SetFont('dejavusans', '', 12, '', true);

        $pdf->AddPage('landscape');
        $html = '
<style>
table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

table td, table th {
    border: 1px solid #ddd;
}

table#firme {
    border-collapse: collapse;
    width: 100%;
    padding-top:20px;
}

table#firme td {
    border: 1px solid white;
}

</style>
<table cellpadding="5">';

        $sql = $this->Object->getDefaultSelectSql($countSql = false);
        $sql .= $whereOrder;
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();

        $rowCounter = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $rowCounter++;

        }
        $html .= '
</table>';
        $pdf->writeHTML($html, true, false, false, false, '');
        $pdf->Output($label . '.pdf', 'I');
    }


    function getFilesCountByObjectId($objectId)
    {
        $sql = $this->File->getDefaultSelectSql($countSql = true);
        $sql .= "
                 WHERE `" . TBPX . "File`.`objectName` = '" . $this->objectName . "' 
                 AND `" . TBPX . "File`.`objectId`=" . $objectId . "
                 ORDER BY `" . TBPX . "File`.`lastDateTime` DESC";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    function listFilesByObjectId($objectId)
    {
        $sql = $this->File->getDefaultSelectSql($countSql = false);
        $sql .= "
                 WHERE `" . TBPX . "File`.`objectName` = '" . $this->objectName . "' 
                 AND `" . TBPX . "File`.`objectId`=" . $objectId . "
                 ORDER BY `" . TBPX . "File`.`lastDateTime` DESC";
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $resultList = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
            $resultList[] = $row;
        return $resultList;
    }

    function saveFile($arrayFileObject, $files, $uploaded_dir = UPLOADED_DIR)
    {
        $sourceFilePath = '';
        $hasThumbnail = $arrayFileObject['hasThumbnail'] == 1 ? true : false;
        $objectName = $arrayFileObject['File__objectName'] ? $arrayFileObject['File__objectName'] : $this->objectName; // added due to File.php form
        $OldFileExtension = '';

        if ($files['File__file']['tmp_name']) {
            $sourceFilePath = $files['File__file']['tmp_name'];
            $arrayFileObject['File__originalFilename'] = $files['File__file']['name'];
            $arrayFileObject['File__mimetype'] = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $sourceFilePath);
            $arrayFileObject['File__extension'] = strtolower(pathinfo($arrayFileObject['File__originalFilename'], PATHINFO_EXTENSION));
        }

        if (!$arrayFileObject['File__id']) {  // Insert
            $arrayFileObject['File__objectName'] = $objectName;
            $arrayFileObject['File__lastDateTime'] = date("Y-m-d H:i:s");
            $arrayFileObject['File__user'] = $_SESSION['User']['id'];
            $arrayFileObject['File__secretKey'] = bin2hex(openssl_random_pseudo_bytes(10));

            // INSERT TO GET ID
            $arrayFileObjectInsert = $this->DatabaseHelper->cleanFieldsArray('File', $arrayFileObject);
            $arrayFileObject['File__id'] = $this->DatabaseHelper->insert(TBPX . 'File', $arrayFileObjectInsert);
            //$arrayFileObject['File__id'] = $this->DatabaseHelper->pdo->lastInsertId();
            $this->insertLog('stmt insert ' . TBPX . 'File id=' . $arrayFileObject['File__id'], json_encode($arrayFileObject));
        } else {
            $OldFile = new File($this->DatabaseHelper);
            $OldFile->loadById($arrayFileObject['File__id']);
            $OldFileExtension = $OldFile->getExtension();
            $arrayFileObject['File__secretKey'] = $OldFile->getSecretKey();
        }

        if ($sourceFilePath) {
            if (!is_dir($uploaded_dir))
                @mkdir($uploaded_dir, 0777);
            $destinationOriginalDir = $uploaded_dir . '/original';
            if (!is_dir($destinationOriginalDir))
                @mkdir($destinationOriginalDir, 0777);
            if (filter_var(UPLOADED_IMAGE_RESAMPLED_ENABLED, FILTER_VALIDATE_BOOLEAN)) {
                $destinationResampledDir = $uploaded_dir . '/resampled';
                if (!is_dir($destinationResampledDir))
                    @mkdir($destinationResampledDir, 0777);
            }
            if (filter_var(UPLOADED_IMAGE_USE_IMAGEMAGICK, FILTER_VALIDATE_BOOLEAN)) {
                $destinationTmpDir = $uploaded_dir . '/tmp';
                if (!is_dir($destinationTmpDir))
                    @mkdir($destinationTmpDir, 0777);
            }
            $destinationThumbnailDir = $uploaded_dir . '/thumbnail';
            if (!is_dir($destinationThumbnailDir))
                @mkdir($destinationThumbnailDir, 0777);

            $originalFilePath = $destinationOriginalDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.' . $arrayFileObject['File__extension'];
            if (file_exists($originalFilePath))
                unlink($originalFilePath);
            if ($OldFileExtension) {
                $OldFileOriginalFilePath = $destinationOriginalDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.' . $OldFileExtension;
                unlink($OldFileOriginalFilePath);
            }

            if (filter_var(UPLOADED_IMAGE_RESAMPLED_ENABLED, FILTER_VALIDATE_BOOLEAN)) {
                $destinationResampleFilePath = $destinationResampledDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.' . $arrayFileObject['File__extension'];
                if (file_exists($destinationResampleFilePath))
                    unlink($destinationResampleFilePath);
            }
            if (filter_var(UPLOADED_IMAGE_USE_IMAGEMAGICK, FILTER_VALIDATE_BOOLEAN)) {
                $destinationTmpFilePath = $destinationTmpDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.jpg';
                if (file_exists($destinationTmpFilePath))
                    unlink($destinationTmpFilePath);
            }
            $destinationThumbnailFilePath = $destinationThumbnailDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.jpg';
            if (file_exists($destinationThumbnailFilePath))
                unlink($destinationThumbnailFilePath);

            $sourceImageInfo = @getimagesize($sourceFilePath);
            list($sourceImageWidth, $sourceImageHeight, $sourceImageType, $sourceImageAttr) = $sourceImageInfo;

            // ORIGINAL
            if (filter_var(UPLOADED_IMAGE_CONVERT_TIFF, FILTER_VALIDATE_BOOLEAN) &&
                filter_var(UPLOADED_IMAGE_USE_IMAGEMAGICK, FILTER_VALIDATE_BOOLEAN) &&
                ($sourceImageType == IMAGETYPE_TIFF_II || $sourceImageType == IMAGETYPE_TIFF_MM) // Convert original .tiff or .tif to .jpg
            ) {
                $arrayFileObject['File__mimetype'] = 'image/jpeg';
                $arrayFileObject['File__extension'] = 'jpg';
                $arrayFileObject['File__originalFilename'] = $arrayFileObject['File__originalFilename'] . '.' . $arrayFileObject['File__extension'];
                $originalFilePath = $destinationOriginalDir . '/' . $arrayFileObject['File__id'] . '_' . $arrayFileObject['File__secretKey'] . '.' . $arrayFileObject['File__extension'];
                if (file_exists($originalFilePath))
                    unlink($originalFilePath);
                exec('convert "' . $sourceFilePath . '" "' . $originalFilePath . '"');
                $sourceImageInfo = @getimagesize($originalFilePath);
                $arrayFileObject['File__sizeKb'] = round(filesize($originalFilePath) / 1024, 2);
            } else {
                # Da upload o da filesystem
                if (is_uploaded_file($sourceFilePath))
                    move_uploaded_file($sourceFilePath, $originalFilePath);
                else
                    rename($sourceFilePath, $originalFilePath);
                $arrayFileObject['File__sizeKb'] = round(filesize($originalFilePath) / 1024, 2);
                if ($arrayFileObject['File__mimetype'] == 'application/pdf') {
                    exec('convert "' . $originalFilePath . '"[0] "' . $destinationTmpFilePath . '"');
                    $sourceImageInfo = @getimagesize($destinationTmpFilePath);
                    list($sourceImageWidth, $sourceImageHeight, $sourceImageType, $sourceImageAttr) = $sourceImageInfo;
                    $originalFilePath = $destinationTmpFilePath;
                }
            }

            $hasThumbnail = false;
            $arrayFileObject['File__widthPx'] = null;
            $arrayFileObject['File__heightPx'] = null;
            $arrayFileObject['File__hasResampled'] = 0;
            $arrayFileObject['File__hasThumbnail'] = 0;

            if ($sourceImageInfo) {
                list($sourceImageWidth, $sourceImageHeight, $sourceImageType, $sourceImageAttr) = $sourceImageInfo;
                $arrayFileObject['File__widthPx'] = $sourceImageWidth;
                $arrayFileObject['File__heightPx'] = $sourceImageHeight;

                if ($sourceImageType != IMAGETYPE_GIF &&
                    $sourceImageType != IMAGETYPE_JPEG &&
                    $sourceImageType != IMAGETYPE_PNG &&
                    filter_var(UPLOADED_IMAGE_USE_IMAGEMAGICK, FILTER_VALIDATE_BOOLEAN)
                ) {
                    exec('convert "' . $originalFilePath . '" "' . $destinationTmpFilePath . '"');
                    if (file_exists($destinationTmpFilePath)) {
                        $im = @imagecreatefromjpeg($destinationTmpFilePath);
                        unlink($destinationTmpFilePath);
                    }
                } else if ($sourceImageType == IMAGETYPE_GIF ||
                    $sourceImageType == IMAGETYPE_JPEG ||
                    $sourceImageType == IMAGETYPE_PNG
                ) {
                    switch ($sourceImageType) {
                        case IMAGETYPE_GIF:
                            $im = @imagecreatefromgif($originalFilePath);
                            break;
                        case IMAGETYPE_JPEG:
                            $im = @imagecreatefromjpeg($originalFilePath);
                            break;
                        case IMAGETYPE_PNG:
                            $im = @imagecreatefrompng($originalFilePath);
                            break;
                    }
                }

                if ($im) {
                    if (filter_var(UPLOADED_IMAGE_RESAMPLED_ENABLED, FILTER_VALIDATE_BOOLEAN)) {
                        // RESAMPLED
                        if (max($sourceImageWidth, $sourceImageHeight) > UPLOADED_IMAGE_RESAMPLED_EDGE_MAX_SIZE) {
                            $k = max($sourceImageWidth, $sourceImageHeight) / UPLOADED_IMAGE_RESAMPLED_EDGE_MAX_SIZE;
                            $resampledImageWidth = $sourceImageWidth / $k;
                            $resampledImageHeight = $sourceImageHeight / $k;
                        } else {
                            $resampledImageWidth = $sourceImageWidth;
                            $resampledImageHeight = $sourceImageHeight;
                        }
                        $resampledImage = @imagecreatetruecolor($resampledImageWidth, $resampledImageHeight);
                        @imagecopyresampled($resampledImage, $im, 0, 0, 0, 0, $resampledImageWidth, $resampledImageHeight, $sourceImageWidth, $sourceImageHeight);
                        switch ($sourceImageType) {
                            case IMAGETYPE_GIF:
                                imagegif($resampledImage, $destinationResampleFilePath);
                                break;
                            case IMAGETYPE_JPEG:
                                imagejpeg($resampledImage, $destinationResampleFilePath, UPLOADED_IMAGE_RESAMPLED_JPG_COMPRESSION);
                                break;
                            case IMAGETYPE_PNG:
                                imagepng($resampledImage, $destinationResampleFilePath);
                                break;
                        }
                        $arrayFileObject['File__hasResampled'] = 1;
                    }

                    // THUMBNAIL
                    $k = max($sourceImageWidth, $sourceImageHeight) / UPLOADED_IMAGE_THUMB_EDGE_MAX_SIZE;
                    $thumbnailImageWidth = $sourceImageWidth / $k;
                    $thumbnailImageHeight = $sourceImageHeight / $k;
                    $thumbnailImage = @imagecreatetruecolor($thumbnailImageWidth, $thumbnailImageHeight);
                    @imagecopyresampled($thumbnailImage, $im, 0, 0, 0, 0, $thumbnailImageWidth, $thumbnailImageHeight, $sourceImageWidth, $sourceImageHeight);
                    imagejpeg($thumbnailImage, $destinationThumbnailFilePath, UPLOADED_IMAGE_THUMB_JPG_COMPRESSION);
                    $arrayFileObject['File__hasThumbnail'] = 1;
                    $hasThumbnail = true;

                    if ($arrayFileObject['File__mimetype'] == 'application/pdf') {
                        $arrayFileObject['File__widthPx'] = null;
                        $arrayFileObject['File__heightPx'] = null;
                        if (file_exists($destinationTmpFilePath))
                            unlink($destinationTmpFilePath);
                    }

                }

            }
        }

        $arrayFileObject['setObjectCoverFile'] = $arrayFileObject['setObjectCoverFile'] == 1 ? true : false;
        $arrayFileObject['isCoverFile'] = $arrayFileObject['isCoverFile'] == 1 ? true : false;

        // COVER FILE UPDATE
        if ($arrayFileObject['setObjectCoverFile'] ||
            (!$arrayFileObject['setObjectCoverFile'] && $arrayFileObject['isCoverFile'])
        ) {
            $arrayObject = array();
            $arrayObject['id'] = $arrayFileObject['File__objectId'];
            if (!$hasThumbnail || (!$arrayFileObject['setObjectCoverFile'] && $arrayFileObject['isCoverFile']))
                $arrayObject['coverFile'] = null;
            else
                $arrayObject['coverFile'] = $arrayFileObject['File__id'];
            $arrayObject['lastDateTime'] = date("Y-m-d H:i:s");
            $arrayObject['user'] = $_SESSION['User']['id'];
            $id = $arrayObject['id'];
            unset($arrayObject['id']);
            $result = $this->DatabaseHelper->update(TBPX . $objectName, $arrayObject, array('id' => $id));
            $this->insertLog('stmt update ' . TBPX . $objectName . ' id=' . $id, json_encode($arrayObject));
        }

        // FINAL FILE UPDATE
        $arrayFileObject['File__lastDateTime'] = date("Y-m-d H:i:s");
        $arrayFileObject['File__user'] = $_SESSION['User']['id'];
        $id = $arrayFileObject['File__id'];
        unset($arrayFileObject['File__id']);
        $arrayFileObject = $this->DatabaseHelper->cleanFieldsArray('File', $arrayFileObject);
        $result = $this->DatabaseHelper->update(TBPX . 'File', $arrayFileObject, array('id' => $id));
        $this->insertLog('stmt update ' . TBPX . 'File id=' . $id, json_encode($arrayObject));

        return $id;
    }

    function deleteFile($File__id, $isCoverFile)
    {
        $File = new File($this->DatabaseHelper);
        $File->loadById($File__id);
        if ($File->getId()) {
            $originalFilePath = UPLOADED_DIR . '/original/' . $File->getId() . '_' . $File->getSecretKey() . '.' . $File->getExtension();
            @unlink($originalFilePath);
            $resampledFilePath = UPLOADED_DIR . '/resampled/' . $File->getId() . '_' . $File->getSecretKey() . '.' . $File->getExtension();
            @unlink($resampledFilePath);
            $thumbnailFilePath = UPLOADED_DIR . '/thumbnail/' . $File->getId() . '_' . $File->getSecretKey() . '.' . 'jpg';
            @unlink($thumbnailFilePath);

            $result = $this->DatabaseHelper->delete(TBPX . 'File', array('id' => $File->getId()));
            $this->insertLog('stmt delete ' . TBPX . 'File id=' . $File->getId(), null);

            if ($isCoverFile) {
                $arrayObject = array();
                $arrayObject['coverFile'] = null;
                $arrayObject['lastDateTime'] = date("Y-m-d H:i:s");
                $arrayObject['user'] = $_SESSION['User']['id'];
                $result = $this->DatabaseHelper->update(TBPX . $File->getObjectName(), $arrayObject, array('id' => $File->getObjectId()));
                $this->insertLog('stmt update ' . TBPX . $File->getObjectName() . ' id=' . $File->getObjectId(), json_encode($arrayObject));
            }

            return true;
        }
        return false;
    }

    function getModuleLogoAndLabel($addedLabel = '')
    {
        return '<img class="vertical_middle" alt="' . $this->Module->getIcon() . '" src="../img/icons/nuvola/16x16/' . $this->Module->getIcon() . '"/><span class="vertical_middle colorblue"> ' . strtoupper($this->Module->getLabel()) . ($addedLabel != '' ? ' - ' . $addedLabel : '') . '</span>';
    }

    function echoFileBox($Object, $File__id)
    {
        $File = new File($this->DatabaseHelper);
        $isCoverFile = false;
        if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'] && $File__id) {
            $File->loadById($File__id);
            $isCoverFile = $File->getId() == $Object->getCoverFile();
        }
        echo '
<div class="display_none" id="FileBox">
    <div id="FileContainer">
        <div class="thickboxCaption">
            <div class="float_left no_wrap">';
        if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'] && $File->getId())
            echo 'Modifica file allegato';
        else if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'])
            echo 'Aggiungi file allegato';
        else
            echo 'File allegati';
        echo '
            </div>
            <div class="float_right padding2">';
        if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'] && $File->getId()) {
            echo '
                <a href="?operation=manageFiles&' . $this->Module->getName() . '__id=' . $Object->getId() . '" title="Torna in modalit&agrave; di aggiunta file." class="button">
                    <img src="../img/icons/nuvola/16x16/actions/filenew.png" alt="+" width="16" height="16" />
                    <span>Aggiungi</span>
                </a>';
        }
        echo '
                <a href="#" title="Chiudi la finestra." onclick="$(\'#FileBox\').dialog(\'close\');return false;" class="button">
                    <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
                    <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
                </a>
            </div>
            <div class="clearer">&nbsp;</div>
        </div>';
        if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE']) {
            echo '
        <form enctype="multipart/form-data" action="' . $this->Module->getName() . '_operations.php" method="post" id="fileForm" name="fileForm">
            <input type="hidden" name="operation" value="saveFile"/>    
            <input type="hidden" name="File__objectId" value="' . $Object->getId() . '"/>';
            if ($File->getId())
                echo '
            <input type="hidden" name="File__id" id="File__id" value="' . $File->getId() . '"/>';
            echo '
            <div class="mandatory">
                <label for="File__title">Titolo</label>
                <br/>
                <input data-type="file" data-field="title" class="autocomplete input_text" type="text" name="File__title" id="File__title" maxlength="255" style="width:50%;" value="' . htmlspecialchars($File->getTitle()) . '"/>
                
                <input type="hidden" name="hasThumbnail" id="hasThumbnail" value="' . $File->getHasThumbnail() . '"/>
                <input type="hidden" name="isCoverFile" id="isCoverFile" value="' . ($isCoverFile ? 1 : 0) . '"/>                
                <input class="input_checkbox" ' . (!$Object->getCoverFile() || $isCoverFile ? 'checked="checked"' : '') . ' type="checkbox" name="setObjectCoverFile" id="setObjectCoverFile" value="1"/>
                <label for="setObjectCoverFile" style="color:#333;">Usa come cover</label>                                                                
            </div>         
            <div>
                <label for="File__description">Descrizione</label><br/>
                <textarea name="File__description" id="File__description" rows="2" style="width:95%;">' . htmlspecialchars($File->getDescription()) . '</textarea>
            </div>     
            <div ' . ($File->getId() ? '' : 'class="mandatory"') . '>
                <label for="File__file">Seleziona file' . ($File->getId() ? ' per sostituire l\'attuale' : '') . ' (dimensione massima ' . (UPLOADED_LIMIT_FILESIZE / 1048576) . 'MB)</label>
                <br/>
                <input class="input_text" type="file" name="File__file" id="File__file"/>                               
            </div>             
            <div class="button_box">
              <a href="#" title="Salva." onclick="submitFileForm();" class="button">
                <img src="../img/icons/nuvola/16x16/actions/button_accept.png" alt="OK" width="16" height="16" />
                <span>Salva</span>
              </a>        
                <a href="#" title="Annulla modifiche." onclick="fileForm.reset();return false;" class="button">
                    <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="NO" width="16" height="16" />
                    <span>Annulla modifiche</span>
                </a>
            </div>
        </form>';
        }
        $fileList = $this->listFilesByObjectId($Object->getId());
        echo '
        <div class="modalFileFormOverflow">';
        if (sizeof($fileList) == 0) {
            echo '
            <div class="thickboxCaption">Non sono presenti file.</div>';
        } else {
            echo '
            <div class="align_left table_box" style="padding: 0px;">
                <table border="1">
                    <thead>
                        <tr>
                            <th>Titolo</th>
                            <th>Desc</th>
                            <th>Attributi</th>
                            <th>Data mod</th>
                            <th width="1%">&nbsp;</th>
                            <th width="1%">&nbsp;</th>
                            <th width="1%">&nbsp;</th>';
            if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE']) {
                echo '
                            <th width="1%">&nbsp;</th>
                            <th width="1%">&nbsp;</th>';
            }
            echo '
                        </tr>
                    </thead>
                    <tbody>';
            $count = 0;
            foreach ($fileList as $file) {
                $count++;
                if ($count % 2 == 1) $colorazione_riga = "lightgray"; else $colorazione_riga = "";
                if ($file["id"] == $File__id)
                    $colorazione_riga = "green";

                $isCoverFile = $file["id"] == $Object->getCoverFile();

                echo '
                        <tr ';
                if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'])
                    echo 'ondblclick="location.href=\'?operation=manageFiles&' . $this->objectName . '__id=' . $Object->getId() . '&File__id=' . $file['id'] . '\';return false;"';
                echo ' class="' . $colorazione_riga . '">';
                echo '
                            <td>';

                $file['title'] = htmlspecialchars(trim($file['title']));
                if ($file['title'] != '') {
                    $maxlen = 35;
                    if (strlen($file['title']) > $maxlen) {
                        echo '
                              <div class="tooltip-ajax help" data-type="file" data-field="title" data-id="' . $file['id'] . '" data-title="Titolo">
                                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                                <span>' . mb_strimwidth($file['title'], 0, $maxlen, "...") . '</span>                
                              </div>';
                    } else
                        echo $file['title'];
                } else
                    echo '&nbsp;';
                echo '
                            </td>';
                echo '
                            <td>';
                $file['description'] = htmlspecialchars(trim($file['description']));
                if ($file['description'] != '') {
                    $maxlen = 35;
                    if (strlen($file['description']) > $maxlen) {
                        echo '
                              <div class="tooltip-ajax help" data-type="file" data-field="description" data-id="' . $file['id'] . '" data-title="Descrizione">
                                <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                                <span>' . mb_strimwidth($file['description'], 0, $maxlen, "...") . '</span>                
                              </div>';
                    } else
                        echo $file['description'];
                } else
                    echo '&nbsp;';
                echo '
                            </td>';

                $attrs = ($isCoverFile ? ' <span class="bold">cover</span><br/>' : '') . ($file['mimetype'] ? '<i>Mimetype:</i> ' . $file['mimetype'] . '<br/>' : '') . ($file['sizeKb'] ? '<i>Dim kB:</i> ' . str_replace('.', ',', $file['sizeKb']) . '<br/>' : '') . ($file['widthPx'] ? '<i>Dim px:</i> ' . $file['widthPx'] . 'x' . $file['heightPx'] : '');
                echo '
                            <td><span class="font9">' . $attrs . '</span></td>';

                $file['lastDateTime'] = date('d/m/Y H:i:s', strtotime($file['lastDateTime']));
                echo '      
                            <td class="no_wrap"><span class="font8">' . $file['lastDateTime'] . '<br/>ID: ' . $file['id'] . ' User: ' . $file['user_userName'] . '</span></td>';
                $originalFilePath = UPLOADED_DIR . '/original/' . $file['id'] .'_'.$file['secretKey']. '.' . $file['extension'];
                echo '    
                            <td>
                                <a href="' . $originalFilePath . '" target="_blank" class="vertical_middle" title="Link al file originale">
                                    <img src="../img/icons/nuvola/16x16/custom/link.png" alt="link"/>
                                </a>    
                            </td>           
                            <td>
                                <a href="#" onclick="location.href=\'_Download.php?File__id=' . $file['id'] . '\'" class="vertical_middle" title="Download file originale">
                                    <img src="../img/icons/nuvola/16x16/actions/revert.png" alt="download"/>
                                </a>    
                            </td>                            
                            <td class="align_center vertical_middle">';
                if ($file['hasThumbnail']) {
                    echo '
                                <a href="#" onclick="location.href=\'_Download.php?File__id=' . $file['id'] . '\'" title="Download file originale" class="tooltip-ajax" data-type="file" data-field="thumbnail" data-id="' . $file['id'] . '" data-title="Thumbnail">
                                    <img src="../img/icons/nuvola/16x16/actions/thumbnail.png" alt="thumb"/>
                                </a>';
                } else
                    echo '&nbsp;';
                echo '
                            </td>';
                if ($_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE']) {
                    echo '
                            <td class="link">
                                <a href="#" title="modifica" onclick="location.href=\'?operation=manageFiles&' . $this->objectName . '__id=' . $Object->getId() . '&File__id=' . $file['id'] . '\';return false;">
                                    <img src="../img/icons/nuvola/16x16/actions/pencil.png" alt="mod" width="16" height="16"/>
                                </a>
                            </td>
                            <td class="link">                           
                                <a href="#" title="cancella" onclick="swal({title: \'Confermi la cancellazione del file?\', text: \'I file cancellati verranno rimossi fisicamente dal sistema e non saranno recuperabili.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false},
                                    function(){
                                        location.href=\'' . $this->objectName . '_operations.php?operation=deleteFile&File__objectId=' . $Object->getId() . '&File__id=' . $file['id'] . ($isCoverFile ? '&isCoverFile=1' : '') . '\';
                                    }); return false;">
                                  <img src="../img/icons/nuvola/16x16/actions/cancel.png" alt="can" width="16" height="16"/>
                                </a>
                            </td>';
                }
                echo '
                        </tr>';
            }
            echo '
                    </tbody>
                </table>
            </div>';
        }
        echo '
        </div>
    </div>
</div>';
    }

    function echoLogBox($objectType, $objectInstance)
    {
        $Log = new Log($this->DatabaseHelper);
        echo '
<div class="display_none" id="LogBox">
    <div id="LogContainer">
        <div class="thickboxCaption">
            <div class="float_left no_wrap">Log modifiche</div>
            <div class="float_right padding2">
                <a href="#" title="Chiudi la finestra." onclick="$(\'#LogBox\').dialog(\'close\');return false;" class="button">
                    <img src="../img/icons/nuvola/16x16/actions/fileclose.png" alt="NO" width="16" height="16" />
                    <span>Chiudi (<span class="shortkeys">Esc</span>)</span>
                </a>
            </div>
            <div class="clearer">&nbsp;</div>
        </div>';
        echo '
        <div class="modalFileFormOverflow">';
        echo '
            <div class="align_left table_box" style="padding: 0px;">
                <table border="1">                                              
                    <thead>
                        <tr>                        
                            <th class="no_wrap">Tipo</th>
                            <th class="no_wrap">Log</a></th>
                            <th class="no_wrap">IP</a></th>';
//        echo '
//                            <th class="no_wrap">Browser</th>';
        echo '
                            <th class="no_wrap">Data</th>
                            <th class="no_wrap">Utente</th>
                        </tr>
                    </thead>
                    <tbody>';


        $sql = $Log->getSqlFilteredByObject($objectType, $objectInstance->getId());
        $stmt = $this->DatabaseHelper->prepare($sql);
        $stmt->execute();
        $count = 0;
        while ($Log_array = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $count++;
            if ($count % 2 == 1) $colorazione_riga = "lightgray"; else $colorazione_riga = "";
            echo '
                        <tr  class="' . $colorazione_riga . '">
                            <td>' . (strpos($Log_array['type'], 'insert') !== false ? 'INSERIMENTO' : 'MODIFICA') . '</td>
                            <td>
                                <a href="#" title="Dettagli log." class="tooltip-ajax button button_help" data-field="_log"  data-id="' . $Log_array['id'] . '" data-title="Dettagli Log"  onclick="return false;">
                                  <img class="vertical_middle" src="../img/icons/nuvola/16x16/actions/messagebox_info.png" alt="info"/>
                                  <span>Dettagli</span>
                                </a>
                            </td>
                            <td>' . $Log_array['ip'] . '</td>';
//            echo '
//                            <td>' . $Log_array['browser'] . '</td>';
            $Log_array['lastDateTime'] = date('d/m/Y H:i:s', strtotime($Log_array['lastDateTime']));
            echo '
                            <td>' . $Log_array['lastDateTime'] . '</td>
                            <td>' . $Log_array['user_userName'] . '</td>
                        </tr>';
        }
        echo '
                    </tbody>
                </table>
            </div>';

        echo '
        </div>
    </div>
</div>';
    }


    function echoDefaultJsFunctions()
    {
        //@todo: move js in a file js...
        echo '       
         
function submitFileForm(){
    if ($(\'#File__title\').val()==\'\')
        swal(\'Attenzione! Compilare il campo: Titolo.\');
    else if (!$(\'#File__id\').length && $(\'#File__file\').get(0).files.length === 0)
        swal(\'Attenzione! Compilare il campo: File.\');     
    else
        $(\'#fileForm\').submit();
    return false;
}

function submitFilterForm(){
    $(\'#filterForm\').submit();
    return false;
}

function submitFilterFormByEnter(e) {
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;
    else return true;

    if (keycode == 13)
      submitFilterForm();
    else
      return true;
}

function precompileFields(moduleName, discriminant){
    $.post(moduleName + "_operations.php",
        {\'operation\': \'getFieldsPrecompilation\', \'discriminant\': discriminant},            
        function (data) {
            $.each(data, function(field, value) {
                $(\'#\' + moduleName + \'__\' + field).val(value);
            });
        }, 
        \'json\' 
    );                
}

function validEmail(elementValue){
    var emailPattern = /^[a-zA-Z0-9+._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(elementValue);
}

';
    }

    function echoDefaultJsInit()
    {
        //@todo: move js in a file js...

        echo '  
    $.fn.scrollViewAndFocus = function (scrollSlow=1000, color="red") {
        return this.each(function () {
            $("html, body").animate({
                scrollTop: ($(this).offset().top-200)
            }, scrollSlow);
            $(this).focus();
            $(this).effect("highlight", {color: color}, 2000);        
        });
    }    

    $.fn.focusAndHighlight = function (color="red") {
        return this.each(function () {
            $(this).focus();
            $(this).effect("highlight", {color: color}, 2000);        
        });
    }    
    
    $(\'#FilterBox\').dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        height: \'auto\',
        closeOnEscape: true,
        draggable: true,
        resizable: false,
        dialogClass: \'no-title-dialog\'
    });
    
    $(\'#FileBox\').dialog({
        autoOpen: false,
        modal: true,
        width: 700,
        height: \'auto\',
        closeOnEscape: true,
        draggable: true,
        resizable: false,
        dialogClass: \'no-title-dialog\'
    });    
    
    $(\'.tooltip\').tooltipster({
        theme: \'tooltipster-light\',
        interactive: true,
        trackTooltip: true,
        updateAnimation: null,
        functionInit: function(instance, helper){
            var $origin = $(helper.origin);
            var title = $origin.data(\'title\');
            var body = $origin.data(\'body\');
            var content = $(\'<div class="tooltipster-custom-title">\' + title + \'</div><div class="tooltipster-custom-body">\' + body + \'</div>\');
            instance.content(content);
        },                
    });
    
    $(\'.tooltip-ajax\').tooltipster({
        theme: \'tooltipster-light\',
        interactive: true,
        trackTooltip: true,   
        content: \'Loading...\',
        updateAnimation: null,
        functionBefore: function(instance, helper) {
            var $origin = $(helper.origin);
            var id = $origin.data(\'id\') ? $origin.data(\'id\') : $(\'#\'+$origin.data(\'elementid\')).val();
            var field = $origin.data(\'field\');
            var type = $origin.data(\'type\');
            var object = $origin.data(\'object\') ? $origin.data(\'object\') : \'' . $this->objectName . '\' ;
            var operation = \'echoField\';
            if(field==\'thumbnail\')
                operation=\'echoImage\';
            if(field==\'_details\')
                operation=\'echoDetails\';      
            if(field==\'_log\')
                operation=\'echoLog\';                             
            $.get(object + \'_operations.php?operation=\' + operation + \'&type=\' + type + \'&field=\' + field + \'&id=\' + id , function(data) {                
                var title = $origin.data(\'title\');
                var body = data;
                var content = $(\'<div class="tooltipster-custom-title">\' + title + \'</div><div class="tooltipster-custom-body">\' + body + \'</div>\');
                instance.content(content); 
            });            
        }          
    });
    
    $.ui.autocomplete.prototype._renderItem = function (ul, item) {        
        var t = String(item.value).replace(
                new RegExp(this.term, "gi"),
                "<strong>$&</strong>");
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<div>" + t + "</div>")
            .appendTo(ul);
    };
    
    $(\'.autocomplete\').each(function(i, el) {    
        var el = $(el);        
        el.autocomplete({
            source: \'' . $this->objectName . '_operations.php?operation=autocomplete&type=\'+el.data(\'type\')+\'&field=\' + el.data(\'field\'),
            minLength: 3,
            autoFocus: false        
        });
    });    
    
    $(\'.autocomplete-foreign\').each(function(i, el) {    
        var el = $(el);        
        el.autocomplete({
            source: el.data(\'foreignobject\') + \'_operations.php?operation=autocomplete&field=\' + el.data(\'foreignfield\'),
            minLength: 3,
            autoFocus: false,
            select: function( event, ui ) { 
                $(\'#Foreign' . $this->objectName . '__\' + el.data(\'field\')).val(ui.item.value);
                $(\'#' . $this->objectName . '__\' + el.data(\'field\')).val(ui.item.id);
                return false; 
            }              
        });
    });                  
       
    $(\'.autocomplete-select\').each(function(i, el) {    
        var el = $(el);        
        el.autocomplete({
            source: \'' . $this->objectName . '_operations.php?operation=autocomplete&type=\'+el.data(\'type\')+\'&field=\' + el.data(\'field\'),
            minLength: 3,
            autoFocus: false        
        }).focus(function () {        
            if ($(this).val()==\'\')
                $(this).autocomplete("search", "@suggest@");
        });
        
    });         
    
    function cleanDatepicker() {
       var old_fn = $.datepicker._updateDatepicker;
    
       $.datepicker._updateDatepicker = function(inst) {
          old_fn.call(this, inst);
    
          var buttonPane = $(this).datepicker("widget").find(".ui-datepicker-buttonpane");
    
          $("<button type=\'button\' class=\'ui-datepicker-clean ui-state-default ui-priority-primary ui-corner-all\'>Svuota</button>").appendTo(buttonPane).click(function(ev) {
              $.datepicker._clearDate(inst.input);
          }) ;
       }
    }    
    cleanDatepicker();
        
    $(\'.date-pick\').datepicker({
        showOn: \'both\', 
        showButtonPanel: true,
        buttonImageOnly: true, 
        buttonImage: \'../img/icons/nuvola/16x16/apps/date.png\', 
        buttonText: \'cal\',
        changeMonth: true,
        yearRange: "-150:+50",
        changeYear: true
    });        
        
    $(\'.dateRange\').datepicker({
        showOn: \'both\', 
        showButtonPanel: true,
        changeMonth: true,
        yearRange: "-150:+50",
        changeYear: true,
        buttonImageOnly: true, 
        buttonImage: \'../img/icons/nuvola/16x16/apps/date.png\', 
        buttonText: \'cal\',
        beforeShow: function(input, inst) {
            return {minDate: (input.id.indexOf(\'_ADT\') >= 0 ? $(\'#\' + input.id.replace(\'_ADT\',\'_ADF\')).datepicker(\'getDate\') : null),
                    maxDate: (input.id.indexOf(\'_ADF\') >= 0 ? $(\'#\' + input.id.replace(\'_ADF\',\'_ADT\')).datepicker(\'getDate\') : null)};
        }
    });        
    
    $.datepicker._gotoToday = function(id) { 
        $(id).datepicker(\'setDate\', new Date()).datepicker(\'hide\').blur(); 
    };
        
    $(\'select\').change(function () {
        if (!$(this).val()){
            $(this).css(\'font-weight\', \'normal\');
            $(this).css(\'color\', \'black\');
        }
        else{
            $(this).css(\'font-weight\', \'bold\');
            $(this).css(\'color\', \'blue\');
        }
    });
    
    $(\'select\').each(function(i, el) {    
        if (!$(el).val()){
            $(el).css(\'font-weight\', \'normal\');
            $(el).css(\'color\', \'black\');
        }
        else{
            $(el).css(\'font-weight\', \'bold\');
            $(el).css(\'color\', \'blue\');
        }
    });';

        if ($_REQUEST['operation'] == 'insert' || $_REQUEST['operation'] == 'modify' || $_REQUEST['operation'] == 'duplicateManual') {
            echo '    
    $(top).bind(\'beforeunload\', function(){
      return \'Are you sure you want to leave?\';
    }); 
    $(\'_WorkFrame\').bind(\'beforeunload\', function(){
      return \'Are you sure you want to leave?\';
    });                                           
    $(window).bind(\'beforeunload\', function(){
      return \'Are you sure you want to leave?\';
    });';
        }
        echo '
                                               
    $(function() {
        $.contextMenu({
            selector: \'.contextMenuTableRecord\', 
            callback: function(operation, options) {               
                var id = $(this).attr(\'id\').replace(\'record\', \'\');
                if (operation == \'filter\')                
                    $(\'#FilterBox\').dialog(\'open\');                  
                else if (operation == \'insert\')                
                    location.href=\'?operation=\' + operation;                
                else if (operation == \'modify\' || operation == \'manageFiles\' || operation == \'duplicateManual\')                
                    location.href=\'?operation=\' + operation + \'&' . $this->objectName . '__id=\' + id;                                                              
                else if (operation == \'activate\')                
                    location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=\' + id + \'&' . $this->objectName . '__active=1\';
                else if (operation == \'deactivate\')                
                    location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=\' + id + \'&' . $this->objectName . '__active=0\';  
                else if (operation == \'trash\'){
                    swal({title: \'Confermi di voler cestinare il record?\', text: \'Il record restera\\\' disponibile nel cestino.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                        function(){
                            location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=\' + id + \'&' . $this->objectName . '__trashed=1\';
                    });
                }
                else if (operation == \'restore\'){     
                    swal({title: \'Confermi il ripristino del record?\', text: \'Il record tornera\\\' disponibile nel desktop.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                        function(){
                            location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=\' + id + \'&' . $this->objectName . '__trashed=0\';
                    });
                }
            },
            items: {';
        echo '
                "filter": {name: "Filtra", icon: "filtertable"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['INSERT'] || $_SESSION['Mask'][$this->Module->getId()]['DUPLICATE'])
            echo '
                "insert": {name: "Aggiungi", icon: "addrecord"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['UPDATE'])
            echo '
                "modify": {name: "Modifica", icon: "modifyrecord"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['ACTIVATE'])
            echo '
                "activate": {name: "Attiva", icon: "activate"},
                "deactivate": {name: "Disattiva", icon: "deactivate"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['FILESVIEW'] || $_SESSION['Mask'][$this->Module->getId()]['FILESMANAGE'])
            echo '
                "manageFiles": {name: "File", icon: "managefiles"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['DUPLICATE'])
            echo '
                "duplicateManual": {name: "Duplica", icon: "duplicate"},';
        if ($_SESSION['Mask'][$this->Module->getId()]['TRASH']) {
            if ($_SESSION['ViewTrashed' . $this->objectName] != 1)
                echo '
                "trash": {name: "Cestina", icon: "trashrecord"},';
            else
                echo '
                "restore": {name: "Ripristina", icon: "restorerecord"},';
        }
        echo '                
            }
        });
    });';
    }

    function renderTrashAction($id)
    {
        return '
                <a href="#" title="cestina" onclick="swal({title: \'Confermi di voler cestinare il record?\', text: \'Il record restera\\\' disponibile nel cestino.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                    function(){
                        location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=' . $id . '&' . $this->objectName . '__trashed=1\';
                    }); return false;">
                  <img src="../img/icons/nuvola/16x16/filesystems/trashcan_empty.png" alt="cestino" width="16" height="16"/>
                </a>';
    }

    function renderRestoreAction($id)
    {
        return '        
                <a href="#" title="ripristina" onclick="swal({title: \'Confermi il ripristino del record?\', text: \'Il record tornera\\\' disponibile nel desktop.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                    function(){
                        location.href=\'' . $this->objectName . '_operations.php?operation=changeField&' . $this->objectName . '__id=' . $id . '&' . $this->objectName . '__trashed=0\';
                    }); return false;">
                  <img src="../img/icons/nuvola/16x16/actions/wizard.png" alt="ripristina" width="16" height="16"/>
                </a>';
    }

    function renderDeleteAction($id)
    {
        return '
                <a href="#" title="elimina" onclick="swal({title: \'Confermi di voler eliminare il record?\', text: \'Il record non sara\\\' piu\\\' recuperabile.\', type: \'warning\', showCancelButton: true, confirmButtonColor: \'#DD6B55\', confirmButtonText: \'Confermo\', closeOnConfirm: false}, 
                    function(){
                        location.href=\'' . $this->objectName . '_operations.php?operation=delete&' . $this->objectName . '__id=' . $id . '\';
                    }); return false;">
                  <img src="../img/icons/nuvola/16x16/actions/messagebox_critical.png" alt="elimina" width="16" height="16"/>
                </a>';
    }

    function renderPdfAction($id)
    {
        return '
                <a href="#" title="PDF" onclick="location.href=\'' . $this->objectName . '_operations.php?operation=objectPdf&' . $this->objectName . '__id=' . $id . '\';return false;">
                  <img src="../img/icons/nuvola/16x16/mimetypes/pdf.png" alt="PDF" width="16" height="16"/>
                </a>';
    }

    function echoSelectOggetti()
    {
        $oggetti = $this->listFieldValues('', 'objectName');
        if ($oggetti) {
            echo '
        <div class="align_center">
            <span>Oggetti:</span>
            <select onchange="
            if ($(this).val()==\'\')
                location.href=\'?operation=filterOrder&Filter' . $this->objectName . '__objectName_ALO=&Filter' . $this->objectName . '__objectName=\';
            else
                location.href=\'?operation=filterOrder&Filter' . $this->objectName . '__objectName_ALO=LIKE&Filter' . $this->objectName . '__objectName=\'+$(this).val();
            return false;">
                <option value="">Seleziona...</option>';
            foreach ($oggetti as $oggetto) {
                $selected = ($oggetto == $_SESSION['Filter' . $this->objectName]['ant_' . $this->objectName . '.objectName'] && $_SESSION['Filter' . $this->objectName]['ant_' . $this->objectName . '.objectName_ALO'] == 'LIKE') ? 'selected="selected"' : '';
                echo '
                <option ' . $selected . ' value="' . htmlspecialchars($oggetto) . '">' . htmlspecialchars($oggetto) . "</option>\n";
            }
            echo '
            </select>    
        </div>';
        }
    }


    function echoBoxOperationButtonsBackgroud($istituzioneCulturale)
    {
        echo '
<style>
        #box_operation_buttons {
            background-image:url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\' version=\'1.1\'><text x=\'30\' font-family=\'Arial\' y=\'30\' fill=\'rgba(0,55,77,1)\' font-weight=\'bold\'  font-size=\'20\'>' . $istituzioneCulturale . '</text></svg>");
            background-repeat:no-repeat;
        }
</style>';
    }


}

?>
