<?php

class Design
{
    public $customer;
    public $bodyTag;
    public $css;
    public $js;
    public $DatabaseHelper;

    function __construct()
    {
        $this->customer['name'] = CUSTOMER_NAME;
        $this->customer['site'] = CUSTOMER_SITE;
        $this->bodyTag = '
  <body>';
        $this->css = array();
        $this->addCss('../lib/jquery/jquery-ui.min.css');
        $this->addCss('../lib/jquery/jquery.contextMenu.min.css');
        $this->addCss('../lib/jquery/tooltipster.bundle.min.css');
        $this->addCss('../lib/jquery/tooltipster-sideTip-light.min.css');
        $this->addCss('../lib/jquery/sweetalert.css');
        $this->addCss('../lib/jquery/sweetalert.theme.css');
        $this->addCss('../css/base.css');
        $this->addCss('../css/custom.css');
        $this->addCss('../lib/IcoMoon-Free-master/style.css');

        $this->js = array();
        $this->addJs('../lib/jquery/jquery.min.js');
        $this->addJs('../lib/jquery/jquery-ui.min.js');
        $this->addJs('../lib/jquery/datepicker-it.js');
        $this->addJs('../lib/jquery/jquery.contextMenu.min.js');
        $this->addJs('../lib/jquery/tooltipster.bundle.min.js');
        $this->addJs('../lib/jquery/sweetalert.min.js');
        $this->addJs('../js/jquery.gui-functions.js?5');
    }

    function setDatabaseHelper($DatabaseHelper)
    {
        $this->DatabaseHelper = $DatabaseHelper;
    }

    function setBodyTag($bodyTag)
    {
        $this->bodyTag = $bodyTag;
    }

    function __destruct()
    {
    }

    function addCss($css)
    {
        array_push($this->css, $css);
    }

    function addJs($js)
    {
        array_push($this->js, $js);
    }

    function privateHtmlOpen($frameset = 0)
    {
        $display = '<!DOCTYPE html>';
        $display .= '
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it-IT">
  <head>
    <title>' . APPLICATION_NAME . ' - powered by Antilophpe</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <meta name="author" content="www.ncfsistemi.com"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link rel="shortcut icon" href="../favicon.ico"/>';
        if (!$frameset) {
            foreach ($this->css as $cssUrl)
                $display .= '
    <link rel="stylesheet" media="screen" href="' . $cssUrl . '" type="text/css"/>';
            foreach ($this->js as $jsUrl)
                $display .= '
    <script src="' . $jsUrl . '" language="javascript" type="text/javascript"></script>';
        }
        $display .= '
  </head>';
        if (!$frameset) {
            $display .= $this->bodyTag;
            if (ENVIRONMENT == 'test') {
                $color = 'limegreen';
                $display .= '
    <style>
    div#login_div {
        border: 10px solid ' . $color . ';
        background-image:url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\' version=\'1.1\'><text x=\'300\' font-family=\'Arial\' y=\'250\' fill=\'rgba(0,55,77,1)\' font-weight=\'bold\'  font-size=\'40\'>TEST</text></svg>");
        background-repeat:no-repeat;             
    }
    .orange {
        background-color: ' . $color . ';
    }    
    .border1orange {
        border: 1px solid ' . $color . ';
    }
    div#header {
        background-color: ' . $color . ';
        background-image:url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\' version=\'1.1\'><text x=\'300\' font-family=\'Arial\' y=\'30\' fill=\'rgba(0,55,77,1)\' font-weight=\'bold\'  font-size=\'30\'>TEST</text></svg>");
        background-repeat:no-repeat;        
    }    
    .mandatory label, .mandatory legend{
        color: ' . $color . ';
    }
    label.mandatory, legend.mandatory {
        color: ' . $color . ';
    }        
    .FormBoxHeaderInside {
        color: ' . $color . ';
    }    
    div.workframe {
        border: 10px solid ' . $color . ';
    }  
    </style>';
            }
        }

        echo $display;
    }

    function privateHtmlClose($frameset = 0, $noswitch = 0, $sublevels = '')
    {
        $display = '';
        if (!$frameset && !$noswitch)
            $display = '
      <div id="LeftFrameSwitcherContainer">
        <div class="LeftFrameSwitcher" id="divLeftFrameSwitcher">
          <img name="close" class="link" style="vertical-align:middle;" alt="bar_close" width="12" height="60" id="imgLeftFrameSwitcher" onClick="menuSwitch();" title="Nascondi/mostra il pannello di navigazione" src="../img/bar_close.png">
        </div>
      </div>
      <script>
        if (window.opener){
          $(\'#imgLeftFrameSwitcher\').attr({ style: "display:none;"});
        }
        
            if(' . $sublevels . 'parent.document.getElementById("_MainFrameSet").cols==\'210,*\'){
              $(\'#imgLeftFrameSwitcher\').attr({ src: "../img/bar_close.png"});
              $(\'#imgLeftFrameSwitcher\').attr({ name: "close"});
            }
            else {
              $(\'#imgLeftFrameSwitcher\').attr({ src: "../img/bar_open.png"});
              $(\'#imgLeftFrameSwitcher\').attr({ name: "open"});
            }        
        
        
        function menuSwitch() {

            if(' . $sublevels . 'parent.document.getElementById("_MainFrameSet").cols==\'210,*\'){
              $(\'#menu\').attr({ style: "display:none;"});
              $(\'#imgLeftFrameSwitcher\').attr({ src: "../img/bar_open.png"});
              $(\'#imgLeftFrameSwitcher\').attr({ name: "open"});
              ' . $sublevels . 'parent.document.getElementById("_MainFrameSet").cols=\'0,*\';
            }
            else {
              $(\'#menu\').attr({ style: "display:block;"});
              $(\'#imgLeftFrameSwitcher\').attr({ src: "../img/bar_close.png"});
              $(\'#imgLeftFrameSwitcher\').attr({ name: "close"});
              ' . $sublevels . 'parent.document.getElementById("_MainFrameSet").cols=\'210,*\';
            }

        }
        
        function menuClose(){
              $(\'#menu\').attr({ style: "display:none;"});
              $(\'#imgLeftFrameSwitcher\').attr({ src: "../img/bar_open.png"});
              $(\'#imgLeftFrameSwitcher\').attr({ name: "open"});
              ' . $sublevels . 'parent.document.getElementById("_MainFrameSet").cols=\'0,*\';          
          
        }
      </script>';
        if (!$frameset)
            $display .= '
  </body>';
        $display .= '
</html>';
        echo $display;
    }

    function menu($mask)
    {
        echo '
<div id="menu" class="dtree">
  <br/>
	<script type="text/javascript">';
        //@todo: fix in Antilophpe: idToPosition, tree
        echo "
        idToPosition = new Array();
	    tree = new dTree('tree');
		tree.config.useStatusText = true;";

        $sql = "SELECT *
             FROM " . TBPX . "Module
             WHERE `active`='1'
             AND `trashed` = '0'
             ORDER BY `parentModule`, `siblingOrder`, `label`";
        $stmt = $this->DatabaseHelper->pdo->prepare($sql);
        $stmt->execute();
        $count = 0;
        while ($menuItem = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Fix per evitare che vengano estratti i moduli ad istanza multipla (gli originali)
            $sql = "SELECT COUNT(*) AS tot
             FROM " . TBPX . "Action
             WHERE module = " . $menuItem['id']."
             AND trashed=0";
            $stmt2 = $this->DatabaseHelper->pdo->prepare($sql);
            $stmt2->execute();
            $menuItemActionsCount = $stmt2->fetch(PDO::FETCH_ASSOC);
            if ($menuItem['jsonDescription'] && $menuItemActionsCount['tot'] == 0)
                continue;

            if ($menuItem['id'] == 1) {
                $menuName = str_replace("'", "\'", APPLICATION_NAME);

                echo "
        idToPosition['" . $menuItem['id'] . "'] = " . $count . ";
        tree.add(".$menuItem['id'].", -1, '" . strtoupper($menuName) . "', '".$menuItem['url']."', '" . APPLICATION_NAME . "', '".$menuItem['urlTarget']."', '', '');";
                $count++;
            } else if (isset($mask[$menuItem['id']]) && $menuItem['url'] != '') {
                echo "
        idToPosition['" . $menuItem['id'] . "'] = " . $count . ";        
        tree.add(".$menuItem['id'].", ".$menuItem['parentModule'].", '" . str_replace("'", "\'", $menuItem['label']) . "', '" . str_replace("'", "\'", $menuItem['url']) . "', '" . str_replace("'", "\'", $menuItem['label']) . "', '".$menuItem['urlTarget']."', '../img/icons/nuvola/16x16/".$menuItem['icon']."', '../img/icons/nuvola/16x16/".$menuItem['icon']."');";
                $count++;
            } else if ($menuItem['url'] == '') {
                $sql = "SELECT *
                        FROM " . TBPX . "Module
                        WHERE active='1'
                        AND trashed = '0'
                        AND parentModule='" . $menuItem['id'] . "'";
                $stmt2 = $this->DatabaseHelper->pdo->prepare($sql);
                $stmt2->execute();
                while ($menuChild = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    if (isset($mask[$menuChild['id']])) {
                        echo "
        idToPosition['" . $menuItem['id'] . "'] = " . $count . ";                        
        tree.add(".$menuItem['id'].", ".$menuItem['parentModule'].", '" . str_replace("'", "\'", $menuItem['label']) . "', '" . str_replace("'", "\'", $menuItem['url']) . "', '" . str_replace("'", "\'", $menuItem['label']) . "', '".$menuItem['urlTarget']."', '../img/icons/nuvola/16x16/".$menuItem['icon']."', '../img/icons/nuvola/16x16/".$menuItem['icon']."');";
                        $count++;
                        break;
                    }
                }
            }
        }

        echo "        
		document.write(tree);";
        if ($_SESSION['User']['id']!=1) // if not admin
            echo "
		tree.openAll();";
        echo '
	</script>
	<br/>
    <div class="align_center padding2">
        <a href="#" title="Apri tutti i sottomenu." onclick="tree.openAll();" class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag+.png" alt="+" width="16" height="16" />
          <span>Apri</span>
        </a>
        <a href="#" title="Chiudi tutti i sottomenu." onclick="tree.closeAll();" class="button">
          <img src="../img/icons/nuvola/16x16/actions/viewmag-.png" alt="-" width="16" height="16" />
          <span>Chiudi</span>
        </a>
        <a href="#" title="Esci dal programma." onclick="top.location.href =\'_LoginOperations.php?operation=logout\';return false;" class="button">
          <img src="../img/icons/nuvola/16x16/actions/exit.png" alt="X" width="16" height="16" />
          <span>Esci</span>
        </a>
        <br/>
    </div>	
</div>';
    }


}
