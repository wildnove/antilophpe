<?php
define('K_PATH_IMAGES', '../lib/Pdf/TCPDF/');
require_once("../lib/Pdf/TCPDF/tcpdf.php");

class CustomPdf extends TCPDF
{
    public function Footer()
    {
        $this->SetY(-20);
        $this->SetFont('helvetica', 'N', 8);
        $this->Cell(0, 5, '', 'T', true, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 5, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
